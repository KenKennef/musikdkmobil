({
	baseUrl: ".",
	name: "main",
	out: "main-built.js",
	paths: {
        jquery: "../lib/jquery-2.1.1.min",
		underscore   	: '../lib/lodash-2.4.1',
		backbone     		: '../lib/backbone',
		unveil				: '../lib/jquery.unveil',	
		layoutmanager	: '../lib/backbone.layoutmanager',
		handlebars	 	: '../lib/handlebars/handlebars-v1.3.0',
		blur					: '../lib/blur',
		pace					: '../lib/pace.min',
		magnificPopup	: '../lib/jquery.magnific-popup',
		hello					: '../lib/hello.min',
		authentication 	: '../app/tools/authentication',
		API					 : '../app/tools/api',
		utils					: '../app/tools/utils',
		datetimepicker	: '../lib/datetimepicker',
		autocomplete		: '../lib/jquery-ui-1.10.4.custom',
		moment 			: '../lib/moment.min',
		countdown		: '../lib/countdown'
    },
})