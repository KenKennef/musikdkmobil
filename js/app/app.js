
define([
	'jquery',
	'underscore',
	'backbone',
	'layoutmanager',
	'handlebars',
	'hello',
	'datetimepicker',
	'countdown',
	'unveil',
	'owlCarousel',
	'swipeScript',
	'modal',
	'lazySizes'
],

	function ($, _, Backbone, hello, datetimepicker, countdown, unveil, owlCarousel, swipeScript, modal, lazySizes) {

		// If the expired flag is set helper check
		Handlebars.registerHelper('ifExpired', function(status, options) {
		    if(status === 'expired') return options.fn(this); 
		    return options.inverse(this);
	  	});

		Handlebars.registerHelper("dateFromNow", function(datetime) {
			if (moment) {
				moment.lang('da');
				return moment(datetime, 'X').fromNow();
			}
			else {
				moment.lang('da');	
				return datetime;
		  }
		});
		
		Handlebars.registerHelper('nextConcerts', function(arr) {
			var size = arr.filter(function(value) { return value !== undefined }).length;
			if(size === 1){
				return new Handlebars.SafeString("<h2>Kommende koncert</h2>")
			}			
			if(size === 2){
				return new Handlebars.SafeString("<h2>Næste 2 koncerter</h2>")
			}
			if(size === 3){
				return new Handlebars.SafeString("<h2>Næste 3 koncerter</h2>")
			}
		});
		
		Handlebars.registerHelper('specificServices', function(links, service, serviceName, options) {
			var i, result = '';
			for(i = 0; i < links.length; ++i)
				if(links[i][service] == serviceName)
					result = result + options.fn(links[i]);
				console.log(serviceName);
			return result;
		});		

		Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
			switch (operator) {
				case '==':
					return (v1 == v2) ? options.fn(this) : options.inverse(this);
				case '===':
					return (v1 === v2) ? options.fn(this) : options.inverse(this);
				case '<':
					return (v1 < v2) ? options.fn(this) : options.inverse(this);
				case '<=':
					return (v1 <= v2) ? options.fn(this) : options.inverse(this);
				case '>':
					return (v1 > v2) ? options.fn(this) : options.inverse(this);
				case '>=':
					return (v1 >= v2) ? options.fn(this) : options.inverse(this);
				case '&&':
					return (v1 && v2) ? options.fn(this) : options.inverse(this);
				case '||':
					return (v1 || v2) ? options.fn(this) : options.inverse(this);
				default:
					return options.inverse(this);
			}
		});		
		
		Handlebars.registerHelper('ifCondMultiple', function(v1,operator, v2, operator, v3, operator, v4, options) {
			switch (operator) {
				case '&&':
					return (v1 && v2 && v3 && v4) ? options.fn(this) : options.inverse(this);
				case '||':
					return (v1 || v2 || v3 || v4) ? options.fn(this) : options.inverse(this);
			}
		});	
		
		Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
			if (arguments.length < 3)
				throw new Error("Handlebars Helper equal needs 2 parameters");
			if( lvalue!=rvalue ) {
				return options.inverse(this);
			} else {
				return options.fn(this);
			}
		});	
		
		Handlebars.registerHelper('titleSplit', function(title) {
		  var titles;     
		  if(title.indexOf(' - ') >= 0){
			titles = title.split(' - ');
			return  new Handlebars.SafeString("<h2>" + titles[1] + "</h2>"  + "<h3>" + titles[0] + "</h3>");			
		  } else if(title.indexOf(' -- ') >= 0){
			titles = title.split(' -- ');
			return  new Handlebars.SafeString("<h2>" + titles[1] + "</h2>" +  "<h3>" + titles[0] + "</h3>");
		  } else {
			titles = title.split();
			titles[1] = ''; 
			return  new Handlebars.SafeString("<h2>" + titles[0] + "</h2>" +  "<h3>&nbsp;</h3>");
		  }
		});
		
		Handlebars.registerHelper('trimTime', function(time) {
			var trimmedTime = time.substring(0,5);
			return new Handlebars.SafeString(trimmedTime)
		});
		
		Handlebars.registerHelper('trimYear', function(time) {
			var trimmedYear = time.substring(0,4);
			return new Handlebars.SafeString(trimmedYear)
		});				
		
		Handlebars.registerHelper('trimTrack', function(time) {
			var trimmedTrack = time.substring(3);
			return new Handlebars.SafeString(trimmedTrack)
		});			

		Handlebars.registerHelper('dateFormatMonth', function(context, block) {
		  if (moment) {
			var f = block.hash.format || "MMM";
			return moment(context).format(f);
		  } else {
			return context;
		  }
		});	

		Handlebars.registerHelper('dateFormatDay', function(context, block) {
		  if (moment) {
			var f = block.hash.format || "DD";
			return moment(context).format(f);
		  } else {
			return context;
		  }
		});	
		
		Handlebars.registerHelper('dateFormatFull', function(context, block) {
		  if (moment) {
			var f = block.hash.format || "DD.MM.YYYY";
			return moment(context).format(f);
		  } else {
			return context;
		  }
		});	

		
		// Provide a global location to place configuration settings and module
		// creation.
		// Check the host URL to set one config or other depending
		var BASE_HOST = location.hostname;
				
		if(BASE_HOST == 'localhost'){
			console.log('RUNING IN MOBILE ENVIRONMENT!!!!');
			//alert('RUNING IN MOBILE ENVIRONMENT!!!!');
			// DEVELOPMENT SETTINGS
			// THIS MUST BE REMOVED ONCE DEPLOYED
			var App = {
				URI: 'http://localhost/musikdkmobil',
				ROOT: '/musikdkmobil/',
				APIO: 'https://devapi.musik.dk',
				clients: {
					google: '96709818823-kvsmbdld8hah2q9fhda5q93brbdnld8j.apps.googleusercontent.com',
					twitter: 'UnUDsmttnEAWl192DQ8e9xa71',
					facebook: '257604381092996'
				}
			};
		} else 
		{
			console.log('RUNING IN PC ENVIRONMENT!!!!');
			//alert('RUNING IN PC ENVIRONMENT!!!!');
			// PRODUCTION SETTINGS
			// THIS MUST BE REMOVED ONCE DEPLOYED
			
			var App = {
				ROOT: '',
				URL: '',
				APIO: 'https://api.musik.dk',
				clients: {
					google: '96709818823-kvsmbdld8hah2q9fhda5q93brbdnld8j.apps.googleusercontent.com',
					twitter: 'UnUDsmttnEAWl192DQ8e9xa71',
					facebook: '257604381092996'
				}
			};
		}
		
		// Localize or create a new JavaScript Template object.
		var JST = window.JST = window.JST || {};
		
		// Configure LayoutManager with Backbone Boilerplate defaults.
		Backbone.Layout.configure({
			// Allow LayoutManager to augment Backbone.View.prototype.
			manage: true,

			prefix: '/js/app/templates/',

			fetchTemplate: function (path) {
				// Concatenate the file extension.
				path = path + '.html';

				// If cached, use the compiled template.
				if (JST[path]) {
					return JST[path];
				}

				// Put fetch into `async-mode`.
				var done = this.async();

				// Seek out the template asynchronously.
				$.get(App.ROOT + path, function (contents) {
					done(JST[path] = Handlebars.compile(contents));
				});

				return JST[path];
			}
		});
		
		// Mix Backbone.Events, modules, and layout management into the app object.
		return _.extend(App, {
			// Create a custom object with a nested Views object.
			module   : function (additionalProps) {
				return _.extend({View: {}}, additionalProps);
			},
			

			// Helper for using layouts.
			useLayout: function (name, template) {
				// If already using this Layout, then don't re-inject into the DOM.
				if (this.layout && this.layout.options.template === name) {
					return this.layout;
				}

				// If a layout already exists, remove it from the DOM.
				if (this.layout) {
					this.layout.remove();
				}

				// Create a new Layout.
				var layout = null;

				if (template) {
					layout = new Backbone.Layout({
						template : name,
						className: name,
						id       : 'layout'
					});
				} else {
					layout = new Backbone.Layout({
						className: name,
						id       : 'layout'
					});
				}

				// Insert into the DOM.
				$('#mdkMainWrap').empty().append(layout.el);

				// Render the layout.
				//layout.render();

				// Cache the reference.
				this.layout = layout;

				// Return the reference, for chainability.
				return layout;
			}			
		},Backbone.Events);
			
	});
