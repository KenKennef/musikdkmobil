define(function (require, exports, module) {
	var App = require('app');
	var ArtistModule = require('modules/artist');
	var ArtistDiscographyModule = require('modules/artistDiscography');
	var ArtistVideosModule = require('modules/artistVideos');
	var ArtistConcertsModule = require('modules/artistConcerts');
	var ArtistMerchandiseModule = require('modules/artistMerchandise');
	var DerpConModule = require('modules/derpCon');
	var utils = require('utils');	

	var ArtistController = function(navigation, userNavigation) {
		this.navigation = navigation;
		this.userNavigation = userNavigation;
		//this.acuserNav = acuserNav;
	};
		
	ArtistController.prototype.initArtist = function(name) {
		this.artistModel = new ArtistModule.Model({slug: name});
		this.artistModel.fetch();
		this.artistModel.on('sync', function(){
			this.artistView = new ArtistModule.View({model: this.artistModel});
			App.useLayout('artistchannel', 'artistchannel').setViews({
				'.navMenu': this.navigation,
				'.loginMenuPlaceholder' : this.userNavigation,
				//'.userMenu': this.acuserNav,
				'.artistChannelDiv': this.artistView
			}).render();
		}, this);
		window.scrollTo(0,0);
	};
	
	ArtistController.prototype.initVideos = function(name, videoId) {	
		this.artistvideoModel = new ArtistVideosModule.Model({slug: name});
		this.artistvideoModel.fetch();
		this.artistvideoModel.on('sync', function(){
			this.artistvideoView = new ArtistVideosModule.View({model: this.artistvideoModel, video: videoId});
			App.useLayout('artistchannel', 'artistVideoPage').setViews({
				'.navMenu': this.navigation,
				'.loginMenuPlaceholder' : this.userNavigation,
				'.artistChannelDiv': this.artistvideoView
			}).render();
		}, this);
		window.scrollTo(0,0);
	};

	ArtistController.prototype.initDiscography = function(name) {
		this.artistdiscographyModel = new ArtistDiscographyModule.ArtistDiscographyModel({slug: name});
		this.artistdiscographyModel.fetch();
		this.artistdiscographyModel.on('sync', function() {
			this.artistDiscographyView = new ArtistDiscographyModule.View({model: this.artistdiscographyModel});
			App.useLayout('artistchannel', 'artistDiscography').setViews({
				'.navMenu': this.navigation,
				'.loginMenuPlaceholder' : this.userNavigation,
				'.artistChannelDiv' : this.artistDiscographyView
			}).render();
		}, this);
		window.scrollTo(0,0);
	};
	
	ArtistController.prototype.initConcerts = function(name) {
		this.artistconcertsModel = new ArtistConcertsModule.ArtistConcertsModel({slug: name});
		this.artistconcertsModel.fetch();
		this.artistconcertsModel.on('sync', function() {	
			this.artistConcertsView = new ArtistConcertsModule.View({model: this.artistconcertsModel});
			App.useLayout('artistchannel', 'artistConcertPage').setViews({
				'.navMenu': this.navigation,
				'.loginMenuPlaceholder' : this.userNavigation,
				'.artistChannelDiv' : this.artistConcertsView
			}).render();
		}, this);			
		window.scrollTo(0,0);
	};
	
	ArtistController.prototype.initMerchandise = function(name) {
		this.artistMerchandiseModel = new ArtistMerchandiseModule.ArtistMerchandiseModel({slug: name});
		this.artistMerchandiseModel.fetch();
		this.artistMerchandiseModel.on('sync', function() {	
			this.artistMerchandiseView = new ArtistMerchandiseModule.View({model:this.artistMerchandiseModel});
			App.useLayout('artistchannel', 'artistMerchPage').setViews({
				'.navMenu': this.navigation,
				'.loginMenuPlaceholder' : this.userNavigation,
				'.artistChannelDiv' : this.artistMerchandiseView
			}).render();
		}, this);
		window.scrollTo(0,0);
	};		
	ArtistController.prototype.initDerpCon = function(name) {
			var derpconImage = 'https://static.musik.dk/upload/derpcon/header.jpg';
			utils.changeBackground(derpconImage);		
			this.derpconModel = new DerpConModule.DerpConModel({slug: name});
			this.derpconModel.fetch();
			this.derpconModel.on('sync', function() {			
				this.derpConView = new DerpConModule.View({model: this.derpconModel});
				App.useLayout('artistchannel', 'derpConPage').setViews({
					'.navMenu': this.navigation,
					'.loginMenuPlaceholder' : this.userNavigation,
					'.artistChannelDiv' : this.derpConView
				}).render();
			}, this);			
			window.scrollTo(0,0);
		};
	// Return the module for AMD compliance.
	return ArtistController;
});