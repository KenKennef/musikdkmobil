define(function (require, exports, module) {
	var App = require('app');
	var AdminPanelModule = require('modules/adminPanel');	
	var CompetitionListModule = require('modules/competitionList');
	var CompetitionDetailsModule = require('modules/competitionQuestion');	
	var auth = require('authentication');
	var api = require('API');

	var CompController = function(navigation, userNavigation) {
		this.navigation = navigation;
		this.userNavigation = userNavigation;
		this.adminPanelView =  new AdminPanelModule.View();		
	};

	CompController.prototype.initPage = function() {
		this.competitionListCollection = new CompetitionListModule.Collection();
		this.competitionListCollection.fetch();
		this.competitionListCollection.on('sync', function(){
			App.useLayout('competitions', 'competitions').setViews({
				'.navMenu': this.navigation,
				'.loginMenuPlaceholder' : this.userNavigation,
				'.allComp': new CompetitionListModule.View({collection: this.competitionListCollection}),
				//'.adminPanel' : this.adminPanelView
			}).render();
		}, this);
		this.navigation.triggerRoute('konkurrencer');
	};
	
	CompController.prototype.initCompetition = function(contest_id) {
		this.competitionDetailsModel = new CompetitionDetailsModule.Model({contest_id: contest_id});
		this.competitionDetailsModel.fetch();
		this.competitionDetailsModel.on('sync', function(){
			this.competitionDetailsView = new CompetitionDetailsModule.View({model:this.competitionDetailsModel});
			App.useLayout('competitionDetails', 'competitionDetails').setViews({
				'.navMenu': this.navigation,
				'.loginMenuPlaceholder' : this.userNavigation,
				'.questionDetails' : this.competitionDetailsView,
				//'.adminPanel' : this.adminPanelView
			}).render();
		}, this);
	};

	// Return the module for AMD compliance.
	return CompController;
	
});