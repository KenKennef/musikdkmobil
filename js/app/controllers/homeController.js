define(function (require, exports, module) {
	var App = require('app');
	var BlurUtil = require('blur');
	var FrontPageModule = require('modules/frontPage')

	var HomeController = function(navigation, userNavigation) {
		this.navigation = navigation;
		this.userNavigation = userNavigation;
	};

	HomeController.prototype.initPage = function(letter){
		this.frontpageCollection = new FrontPageModule.Collection();
		this.frontpageCollection.fetch();
		this.frontpageCollection.on('sync', function(){
			App.useLayout('home', 'home').setViews({
				'.navMenu': this.navigation,
				'.loginMenuPlaceholder' : this.userNavigation,
				'.frontpageDiv':new FrontPageModule.View({collection: this.frontpageCollection})
			}).render();
		}, this);		
		this.navigation.triggerRoute('home'); 
	};

	// Return the module for AMD compliance.
	return HomeController;
});