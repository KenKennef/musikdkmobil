define(function (require, exports, module) {
	var App = require('app');
	var BlurUtil = require('blur');
	var VideoModule = require('modules/videos');
	var AdminPanelModule = require('modules/adminPanel');	

	var VideoController = function(navigation, userNavigation) {
		this.navigation = navigation;
		this.userNavigation = userNavigation;
		this.adminPanelView =  new AdminPanelModule.View();	
	};

	VideoController.prototype.initPage = function() {
		this.videoCollection = new VideoModule.VideosCollection();
		this.videoCollection.fetch();
		this.videoCollection.on('sync', function(){
			App.useLayout('videos', 'videos').setViews({
				'.navMenu': this.navigation,
				'.userMenuPlaceholder' : this.userNavigation,
				'.singleVideos' : new VideoModule.View({collection: this.videoCollection}),
				'.adminPanel' : this.adminPanelView
			}).render();
			//this.addBlur();
		}, this)
		this.navigation.triggerRoute('videoer');
	};

	// Return the module for AMD compliance.
	return VideoController;
});