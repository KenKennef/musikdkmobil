define(function (require, exports, module) {
	var App = require('app');
	var BlurUtil = require('blur');
	var TermModule = require('modules/terms');
	var ErrorModule = require('modules/error');
	var AdminPanelModule = require('modules/adminPanel');
	var utils = require('utils');	

	var GlobalController = function(navigation, userNavigation) {
		this.navigation = navigation;
		this.userNavigation = userNavigation;
		this.adminPanelView =  new AdminPanelModule.View();
	};

	GlobalController.prototype.initPage = function() {
		App.useLayout('terms', 'terms').setViews({
			'.navMenu': this.navigation,
			'.loginMenuPlaceholder' : this.userNavigation,
			'.termsDetails' : new TermModule.View(),
			'.adminPanel' : this.adminPanelView
		}).render();
		utils.changeBackground();		
	};
	
	GlobalController.prototype.frontPageData = function() {	
		App.useLayout('frontPageData', 'frontPageData').setViews({
			'.fields': new FrontPageDataModule.View()
		}).render();
	};
	
	GlobalController.prototype.notFound = function() {
		
		App.useLayout('404', '404').setViews({
			'.navMenu': this.navigation,
			'.loginMenuPlaceholder' : this.userNavigation,
			'.errorDiv': new ErrorModule.View(),
			'.adminPanel' : this.adminPanelView
		}).render();
		utils.changeBackground();
	};

	// Return the module for AMD compliance.
	return GlobalController;
});