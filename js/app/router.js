define(function (require, exports, module) {
	var App = require('app');
	var utils = require('utils');
	var querystring = require('queryparams');
	// Modules
	//var AudioPlayerModule = require('modules/audioplayer');
	var NavigationModule = require('modules/navigation');
	var UserTopNavigationModule = require('modules/userTopNavigation');
	var ArtistChannelUserNavigationModule = require('modules/artistChannelUserNav');
	var AdminPanelModule = require('modules/adminPanel');	
	var TermsModule = require('modules/terms');
	var navigation = new NavigationModule.View();
	var userNavigation = new UserTopNavigationModule();
	var acuserNav = new ArtistChannelUserNavigationModule();

	//Controllers
	//For each page there should be created a controller, because otherwise the router gets too big.
	var HomeController = require('controllers/homeController');  
	var homeController = new HomeController(navigation, userNavigation);	
	
	var ArtistController = require('controllers/artistController'); 
	var artistController = new ArtistController(navigation, acuserNav);
	
	var VideoController = require('controllers/videoController'); 
	var videoController = new VideoController(navigation, userNavigation);

	var CompController = require('controllers/compController'); 
	var compController = new CompController(navigation, userNavigation);
	
	var GlobalController = require('controllers/globalController'); 
	var globalController = new GlobalController(navigation, userNavigation);

	// Show cookie alert.
	utils.showCookiesAlert();
	
	utils.defaultMetaTags();

	// Pass query strings as object
	Backbone.Router.namedParameters = true;
	
	// Defining the application router, you can attach sub routers here.
	return Backbone.Router.extend({
		routes: {
			'': 'index',
			'home': 'home',		
			'konkurrencer' : 'konkurrencer',
			'konkurrencer/' : 'konkurrencer',
			'konkurrencer/:contest_id' : 'conId',
			'konkurrencer/:contest_id/' : 'conId',
			'videoer' : 'videoer',
			'videoer/' : 'videoer',
			'terms': 'terms',
			'404': '404',
			':name' : 'artistkanal',
			':name/' : 'artistkanal',
			':name/videoer': 'artist_video',
			':name/videoer/': 'artist_video',
			':name/videoer?w=:videoid' : 'artist_video',
			':name/udgivelser': 'artist_discography',
			':name/udgivelser/': 'artist_discography',
			':name/merchandise' : 'artist_merchandise',
			':name/koncerter': 'artist_concerts',
			// THIS IS ONLY TEMPORARY FOR DERPCON-CAMPAIGN PURPOSE
			// FOR 5SECONDSOFSUMMER		
			':name/derpcon': 'derpcon',
			':name/derpcon/terms': 'derpcon_terms'
			// END DERPCON-CAMPAIGN
		},
		
		initialize: function() {
			//var audioplayerView = new AudioPlayerModule.View({el: '#player'});
			this.adminPanelView =  new AdminPanelModule.View({el: '#fnav'});	
		},

		index: function (letter) {
			homeController.initPage(letter);
			$(document).prop('title', 'MusikDK');
			utils.removeMetaTags();
			utils.defaultTwitterCards();	
			utils.defaultFbMetaTags();		
		},
		home: function (letter) {
			homeController.initPage(letter);
			$(document).prop('title', 'MusikDK');
			utils.removeMetaTags();
			utils.defaultTwitterCards();	
			utils.defaultFbMetaTags();		
		},	
		artistkanal: function (params) {
			artistController.initArtist(params.name);
		},
		artist_video: function(params){
			var watchVideoId = params.w || null;
			artistController.initVideos(params.name, watchVideoId);
		},
		artist_discography: function(params){
			artistController.initDiscography(params.name);
		},
		 artist_merchandise: function(params){
			artistController.initMerchandise(params.name)
		},
		 artist_concerts: function(params){
			 artistController.initConcerts(params.name)
		},
		// THIS IS ONLY TEMPORARY FOR DERPCON-CAMPAIGN PURPOSE
		// FOR 5SECONDSOFSUMMER
		derpcon: function(params){
			artistController.initDerpCon(params.name)
		},
		// END DERPCON-CAMPAIGN
		videoer: function () {
			videoController.initPage();
			utils.removeMetaTags();
			utils.defaultMetaTags();			
		},
		konkurrencer: function () {
			compController.initPage();
			utils.removeMetaTags();
			utils.defaultMetaTags();
		},
		conId: function(params) {
			compController.initCompetition(params.contest_id);
		},
		terms: function() {
			globalController.initPage();
			utils.removeMetaTags();
			utils.defaultMetaTags();			
		},
		404: function(){
			globalController.notFound();
			utils.removeMetaTags();
			utils.defaultMetaTags();			
		},
		// THIS IS ONLY TEMPORARY FOR DERPCON-CAMPAIGN PURPOSE
		// FOR 5SECONDSOFSUMMER		
		derpcon_terms: function(){
			globalController.derpConTerms();
			$('footer').remove();
			utils.removeMetaTags();
			utils.defaultTwitterCards();	
			utils.defaultFbMetaTags();		
		}
		// END DERPCON-CAMPAIGN		
	});

	});
