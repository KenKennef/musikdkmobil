define(['app', 'blur', 'API',
	], function(App, blur, API){

	var Utils = function(){

		return {

			serializeToJSON: function(form){

				var obj = {};
				var formArray = form.serializeArray();

				$.each(formArray, function(k, v){
					// check if the object has the prop to evit rewrite of value
					if(!obj.hasOwnProperty(v.name))
					{
						obj[v.name] = v.value;
					}
					
				});

				return obj;

			},

			changeBackground: function(image){

				// Delete last element once inserted new image
				
				var canvasList = $('#bg').find('canvas');
				if(canvasList.length === 2){
					canvasList.eq(1).remove();
				}

				var imageObj = new Image();
				imageObj.crossOrigin = '';

				if(!image){
					imageObj.src = App.ROOT + '/demobilleder/blurbg.jpg';
				} else 
				{
					imageObj.src = image;
				}
				/*
				//when image is finished loading
				imageObj.onload = function() {

					var w = imageObj.naturalWidth;
					var h = imageObj.naturalHeight;

					var canvas = document.createElement("canvas");
					canvas.width = w;
					canvas.height = h;
					canvas.id = 'supercanvas_'+Math.floor((Math.random() * 100) + 1);
					
					

					var ctx = canvas.getContext('2d');
					ctx.drawImage(imageObj, 0, 0, canvas.width, canvas.height);

					var $canvas = $(canvas) 
					$("#bg").append(canvas);
					$canvas.show();

					stackBlurCanvasRGBA(canvas.id, 0, 0, w, h, 25);

					var canvasRatio = $canvas.width()/$canvas.height();
					var windowRatio = $(window).width()/$(window).height();
					if (canvasRatio > windowRatio){
						$canvas.css({
							"height": "100%",
							"width" : "auto"
						});
					} else {
						$canvas.css({
							"width": "100%",
							"height": "auto"
						});
					}
					$canvas.css({
						"marginLeft" : -$canvas.width()/2+"px",
						"marginTop" : -$canvas.height()/2+"px"
					});

					window.onresize = function(){
						var canvasRatio = $canvas.width()/$canvas.height();
						var windowRatio = $(window).width()/$(window).height();
						if (canvasRatio > windowRatio){
							$canvas.css({
								"height": "100%",
								"width" : "auto"
							});
						} else {
							$canvas.css({
								"width": "100%",
								"height": "auto"
							});
						}
						$canvas.css({
							"marginLeft" : -$canvas.width()/2+"px",
							"marginTop" : -$canvas.height()/2+"px"
						 });
					}				

				};
				*/

			},
			
			defaultMetaTags: function(){				
				var defMeta = ['<meta property="og:title" content="Musik.dk">', '<meta property="og:locale" content="da_DK">','<meta property="og:url" content="https://musik.dk">', '<meta property="og:description" content="MusikDK -  Danmarks største samling af danske og internationale artister">', '<meta property="og:type" content="Website">'];
				var arrayLength = defMeta.length;
				for (var i = 0; i < arrayLength; i++) {
					$(defMeta[i]).insertAfter('title');
				}				
			},
			
			defaultFbMetaTags: function(){				
				var defFbMeta = ['<meta property="og:title" content="Musik.dk" />', '<meta property="og:locale" content="da_DK" />','<meta property="og:url" content="https://musik.dk" />', '<meta property="og:description" content="MusikDK - Danmarks største samling af danske og internationale artister" />', '<meta property="og:type" content="Website" />'];
				var arrayLength = defFbMeta.length;
				for (var i = 0; i < arrayLength; i++) {
					$(defFbMeta[i]).insertAfter('title');
				}				
			},			
			
			defaultTwitterCards: function(){
				var defTwCards = ['<meta name="twitter:card" content="https://static.musik.dk/assets/images/musikdklogo.png">','<meta name="twitter:site" content="@MusikDK">','<meta name="twitter:creator" content="@MusikDK">','<meta name="twitter:title" content="MusikDK">', '<meta name="twitter:description" content="MusikDK - Danmarks største samling af danske og internationale artister">', '<meta name="twitter:url" content="https://musik.dk">', '<meta name="twitter:image" content="https://static.musik.dk/assets/images/musikdklogo.png">'];
				var arrayLength = defTwCards.length;
				for (var i = 0; i < arrayLength; i++) {
					$(defTwCards[i]).insertAfter('title');
				}					
			},	
			
			removeMetaTags:function(){
				$("meta[property=og\\:title]").remove();
				$("meta[property=og\\:url]").remove();
				$("meta[property=og\\:locale]").remove();
				$("meta[property=og\\:site_name]").remove();
				$("meta[property=og\\:description]").remove();
				$("meta[property=og\\:image]").remove();
				$("meta[property=og\\:type]").remove();
				$('meta[name=description]').remove();
			},
		
			setMetaTag: function(metaName, name, value) {
				var meta = '<meta '+metaName+'="'+name+'" content="'+value+'"/>';
				$(meta).insertAfter('title');
			},			

			showCookiesAlert: function(){
				if(!API.__getCookieByName('app_sett')){
					var alertHtml;
					alertHtml = '<section id="cookie">';	
					alertHtml += '<div class="tc">';
					alertHtml += '<h3>På Musik.dk bruger vi cookies</h3>';
					alertHtml += '<p>For at kunne give en tilfredsstillende oplevelse på Musik.dk, gør vi brug af cookies til at gemme simple informationer om brugeradfærd og aktivitet, så vi kan gøre sitet bedre. Ved at fortsætte din brug af Musik.dk accepterer du derved dette. Hvis du vil vide mere om cookies og om hvordan man sletter dem, kan du læse mere <a href="http://minecookies.org" class="acookie" target="_blank">her</a>.</p>';
					alertHtml += '</div>';
					alertHtml += '<div class="ip">';
					alertHtml += '<button class="btngreen">Luk</button>';
					alertHtml += '</div>';
					alertHtml += '</section>';

					// Append to the body
					$('body').append(alertHtml);

					$('#cookie').click(function(e){
						$(this).remove();
						API.setCookie('app_sett', true, 90);
					});
				}
			},
				

		}

	};

	return new Utils;

});