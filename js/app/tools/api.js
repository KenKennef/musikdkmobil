define(['jquery',
	'underscore',
	'backbone',
	'app',
	], function($, _, Backbone, App){


	// Ajax error handling
	
	$.ajaxSetup({
	    //cache: false,
	    statusCode: {
	    	400: function () { return Backbone.history.navigate('404', {trigger:true}); },
	    	401: function () { return App.trigger('app:logout:full'); },
	        404: function () { return Backbone.history.navigate('404', {trigger:true}); }
	    }
	});



	var api = function(){

		var uri = App.APIO,
			bearerToken = null;

		var serialize = function(obj){

			var str = [];
			for(var p in obj)
			if (obj.hasOwnProperty(p)) {
			  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
			return str.join("&");

		};

		var makeRequest = function(path, method, data, callback){

			var settings = {
				url: uri + path,
				contentType:"application/json",
				dataType: 'json',
				beforeSend: function(xhr){
					if (bearerToken) xhr.setRequestHeader('Authorization', 'Bearer '+bearerToken);
				},
				success: function(res){
					callback(res);
				}
			};

			// set data if is not null.
			if(data !== null && method !== 'GET') settings.data = JSON.stringify(data);
			if(method !== 'GET') settings.type = method;
			if(method === 'GET' && data) settings.url += '?'+serialize(data);

			$.ajax(settings);
		};

		return {

			get : function(url, data, cb){
				makeRequest(url, 'GET', data, cb)
			},

			post : function(url, data, cb){
				makeRequest(url, 'POST', data, cb)

			},

			put : function(url, data, cb){
				makeRequest(url, 'PUT', data, cb)
			},

			del : function(url, data, cb){
				makeRequest(url, 'DELETE', data, cb)
			},

			setToken : function(token){
				if(!this.__getCookieByName('app_ss')){
					this.setCookie('app_ss', token, 5);
				}
				bearerToken = token;
			},

			setCookie: function(name, value, expireHours){
				var hours = expireHours || 5;
				var expiration = new Date(new Date().getTime() + ((3600 * hours) * 1000));
				document.cookie = name + "=" + value + '; expires=' + expiration + "; path=/";
			},

			removeToken: function(){
				var expiration = new Date(new Date().getTime() - (10 * (360000 * 60)));
				document.cookie = 'app_ss' + "=0" + '; expires=' + expiration + "; path=/";
				bearerToken = null;
			},

			__getCookieByName: function(name){
				var value = "; " + document.cookie;
				var parts = value.split("; " + name + "=");
				if (parts.length == 2) return parts.pop().split(";").shift();
			},

			__getTokenFromCookie: function(){
				return this.__getCookieByName('app_ss');
			},

			getToken: function(token){
				return bearerToken || this.__getTokenFromCookie() || null;
			}

		}
	};		


	return new api;
});