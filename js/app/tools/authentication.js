define(['jquery',
	'underscore',
	'backbone',
	'app',
	'API'
	], function($, _, Backbone, App, API){

		hello.init(App.clients, {
		   redirect_uri: location.protocol + '//' + location.hostname + App.ROOT,
		   oauth_proxy: App.APIO + '/oauthproxy'
		});

		// we need to return a function that will be accessed to the APP
		var auth = Backbone.Model.extend({

			defaults: {
				logged: false,
				user: null,
				loginIntentLooked: false
			},

			initialize: function(){
				_.bindAll(this);
				this.__initState();
				App.on('app:logout:full', this.logout, this);

				var self = this;
				hello.on('auth.login', function(acct){
					// on auth.login some provider has given access
					// so we send the data to the API to get a musikdk token
					
					if(self.get('logged') === false && self.get('loginIntentLooked') === false){

						
						self.__postAUth({
							provider: acct.network, 
							access_token: acct.authResponse.access_token,
							client_id: acct.authResponse.client_id
						});
						self.set('loginIntentLooked', true);
					}
				});
			},

			isLogged: function(){
				return this.get('logged');
			},

			getUser: function(){
				return this.get('user');
			},

			login: function(){
				alert('opening loggin');
			},

			logout: function(){
				this.set('logged', false);
				this.set('user', null);
				this.set('loginIntentLooked', false);
				App.trigger('user:login:status:changed', null);
				API.removeToken();
			},
			
			__getToken: function(){
				return API.getToken();
			},

			__getUser: function(){
				var self = this;
				API.get('/i/user/me', null, function(res){
					self.set('logged', true);
					self.set('user', res.data);
					App.trigger('user:login:status:changed', res.data);
					$('#complogin').trigger('closeModal');
				});
			},

			__initState: function(){
				var token = API.getToken();
				if(token){
					API.setToken(token);
					this.set('loginIntentLooked', true);
					this.__getUser();
				}
			},

			// priv methods
			// This method will send the response of hello.js with the provider and client id // token
			__postAUth: function(data){

				self = this;
				API.post('/i/auth/login', data, function(res){
					// Set the token in API
					API.setToken(res.token);
					self.__getUser();
					
				});
			}

		});

		// Attach bearer token
		var backboneSync = Backbone.sync;
		Backbone.sync = function(method, model, options) {

			var token = API.getToken();
			if(token){
				options.headers = {
	            	'Authorization': 'Bearer ' + token
       			};
			}
		   	backboneSync(method, model, options);
		};


		return new auth;

});