define([

	'app',
	'backbone',
	'modules/competitionQuestion',
],

	function (App, Backbone, CompetitionQuestion) {

		// Create a new module.
		var CompetitionDetails = App.module();

		// Create view
		CompetitionDetails.View = Backbone.View.extend({
			template: 'competitionDetails_sub',
			beforeRender: function() {
				this.model.contest_id = this.contest_id;
				this.insertView('.questionDetails_sub', new CompetitionQuestion.View({model: this.model}));
				this.model.fetch();		
			},
		});

		return CompetitionDetails;
	});