define([

	'app',
	'backbone',
	'utils'

],

	function (App, Backbone, utils) {

		// Create a new module.
		var ArtistConcerts = App.module();

		// Create view
		ArtistConcerts.View = Backbone.View.extend({
			tagname: 'div',
			className : 'standardLayoutDiv',
			template: 'artistConcerts',
			initialize: function() {
				this.listenTo(this.model, 'sync', this.render);		
			},
			serialize: function() {
				return this.model.toJSON();
			},
			events: {
				'click .selected': 'selected'
			},

			selected: function(e){
				e.preventDefault();
			},			
			printCover: function(){
				var firstConcert= this.model.get('concerts')[0];
					itel = $('div#burnsbox');
				itel.empty();
				var coverImage = this.model.get('images').standard;
				var artist_name = this.model.get('name');
				if(coverImage){
					itel.append("<img data-src="+coverImage+" src='demobilleder/placeholders/square.png' alt="+artist_name+"/> ");
				} else {
					itel.append("<img data-src='/assets/images/default_cover.gif' src='demobilleder/placeholders/square.png' alt="+artist_name+" />");
				}
				$('img').unveil(600);
				//utils.changeBackground(coverImage);
			},			
			afterRender:function(){
				utils.removeMetaTags();
				
				$(document).prop('title', this.model.get('name') + ' | ' + 'Koncerter' + ' | ' + 'MusikDK');
				utils.setMetaTag('name', 'description', this.model.get('name') + ' på MusikDK. ' + 'Se, hør og læs mere om ' + this.model.get('name') + ' her');
				
				// Facebook meta
				utils.setMetaTag('property', 'og:title', 'Se hvornår ' + this.model.get('name') + ' giver koncert');
				utils.setMetaTag('property', 'og:site_name', 'MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('property', 'og:image', this.model.get('images').cover);
				}
				utils.setMetaTag('property', 'og:locale', 'da_DK');	
				utils.setMetaTag('property', 'og:description', 'Koncerter fra ' + this.model.get('name'));
				utils.setMetaTag('property', 'og:url', 'https://musik.dk/' + this.model.get('slug') + '/koncerter');
				
				// Twitter cards 
				utils.setMetaTag('name', 'twitter:title', 'Koncerter fra ' + this.model.get('name') + ' | MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('name', 'twitter:image', this.model.get('images').cover);
				}
				utils.setMetaTag('name', 'twitter:url', 'https://musik.dk/' + this.model.get('slug') + '/koncerter');
				utils.setMetaTag('name', 'twitter:description', 'Se hvornår ' + this.model.get('name') + ' giver koncert');						
				
				if(this.model.get('images')){
					var cover = this.model.get('images').cover || null;
				}
				utils.changeBackground(cover);
				
				var concertCheck = this.model.get('concerts')[0];
				if(concertCheck){
					this.printCover();
				}
				
				$('.headerwhite').find('h1').html(this.model.get('name'));
			}
		});
		
		ArtistConcerts.ArtistConcertsModel = Backbone.Model.extend({
			url: function() {
				return App.APIO + '/i/artist/' + this.get('slug') + '/concerts';
			},		
			parse: function(response){
				return response.data;
			},		
		});		

		return ArtistConcerts;
	}
);