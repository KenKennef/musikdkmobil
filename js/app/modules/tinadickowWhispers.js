define([

	'app',
	'backbone'
],

	function (App, Backbone) { 

		// Create a new module.
		var TinaDickowWhispers = App.module();
		
		TinaDickowWhispers.View= Backbone.View.extend({
			template: 'tinadickow_whispers',
			initialize: function() {
				this.render();
			}
		});

		return TinaDickowWhispers;
	});