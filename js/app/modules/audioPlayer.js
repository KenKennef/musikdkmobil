define([

	'app',
	'backbone',
	'audioplayer'

],

	function (App, Backbone, audioplayer) {

		// Create a new module.
		var Audioplayer = App.module();

		// Create view
		Audioplayer.View = Backbone.View.extend({
			template: 'audioplayer',
			initialize: function() {
				var self = this;
				this.render();
	 
			},
			events: {
				'click .icon-numbered-list' : "showPlayList",
				'click .icon-circled-cross' : "closePlayer"
			},
			
			showPlayList: function() {
				alert('showplaylist');
			},
			closePlayer:function(){
				$('#player').remove();
			},
			player: function(){
				$("#jquery_jplayer_1").jPlayer({
					ready: function(event) {
						$(this).jPlayer("setMedia", {
							title: "The Stark Palace - Cro Magnon Man",
							mp3: "http://jplayer.org/audio/mp3/TSP-01-Cro_magnon_man.mp3",
							oga: "http://jplayer.org/audio/ogg/TSP-01-Cro_magnon_man.ogg",
							poster: "https://static.musik.dk/upload/artist_images/f/fergie_5383/1300x540_fergie-4de57853a8b2b.jpg"
						});
						
					},
					swfPath: "http://jplayer.org/latest/js",
					supplied: "mp3, oga",
					wmode: "window",
					loop: true,
					cssSelectorAncestor: ""
				});	
			},
			afterRender:function(){
				this.player();
				var image = $("#jquery_jplayer_1").find('img');
				$('.jp-poster').append(image);
			}
		});

		return Audioplayer;
	}
);