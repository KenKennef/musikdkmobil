define([

	'app',
	'backbone',
	'utils',
	'moment',
	'API',

],

	function (App, Backbone, utils, moment, api) {

		// Create a new module..
		var AdminPanel = App.module();

		// Create view
		AdminPanel.View = Backbone.View.extend({
			tagName: 'div',
			id: 'fnav',
			template: 'adminPanel',
			initialize: function() {
				this.render();
				var self = this;
				$('.admin-panel').hide();
				App.on('user:login:status:changed', this.loginStatusChanged, this);
				App.on('popup:contest:close', this.closeContestPopup);
			},
			
			events: {
				'click .createContest': 'createContest',
			},
			
			loginStatusChanged: function(user){
				if(user && user.role === 'admin'){
					$('.admin-panel').show();
				} else {
					$('.admin-panel').hide();
				}
			},

			closeContestPopup: function(){
				$.magnificPopup.close();
			},
			
			createContest: function() {
				
				var self = this;
				var popup = $('.createContest').magnificPopup({
					items: {
						src: '#simplecontestwiz',
						disableOn: 700,
						type: 'inline',
						mainClass: 'mfp-fade',
						removalDelay: 160,
						preloader: false, 
						fixedContentPos: false
					},
					closeBtnInside: false,
					callbacks: {
						open: self.popupMethods.call(self)
					}
				}, 0).magnificPopup('open');
			},

			popupMethods: function(){


				var InputsWrapper   = $("#answerDiv");
				var x = InputsWrapper.length; 
				var FieldCount=1; 
				
				// Add new Field answer
				$('.adaddnext').click(function(e){
					FieldCount++; 
					$(InputsWrapper).append('<div class="adinputfield83"><span class="font-entypo icon-circled-cross adaddnextremove" aria-hidden="true"></span><div class="checkaccept cacc1"><label class="option"><input type="radio" id="contestAnswersChk_'+ FieldCount +'" name="correct" class="validate[required]"><span class="checkbox"></span></label> </div><input type="text" id="contestAnswer_'+ FieldCount +'" placeholder="Write possible answer" class="validate[required]"/></div></div>');
					x++;
					return false;
				});

				$(document).on("click",".adaddnextremove", function(e){ 
					 if( x > 1 ) {
					  $(this).parent('div').remove(); 
					  x--; 
					 }
					 return false;
				});

				$('#setdate,#setopendate').datetimepicker({
					timepicker:false,
					format:'d/m/Y',
					formatDate:'Y/m/d' ,
					minDate:0
				});	
				
				$('#settime,#setopentime').datetimepicker({
					datepicker:false,
					format:'H:i'
				});				
				
				$('#showpubtime').click(function() {
					$('#pubtime').toggle();
					$('#apd').remove();
				});

				$('#showemailtext').click(function() {
					$('#emailtext').toggle();
					$('#etxt').remove();
				});	

				$("#contestArtistId").autocomplete({
					source: function(req, res){
						$.get(App.APIO+'/i/search/artist?name='+req.term, {delay: 1}, function(xhr){
							res(
								$.map(xhr.data, function (artist) {
					            	return {value: artist.name, label: artist.name, artist_id: artist.artist_id};
					        	})
					        );
						});
					}, 
					select: function(event, ui){
						// set the artist_id to the hidden field
						$('[name="artist_id"]').val(ui.item.artist_id);
					}
				});	

				var self = this;

				$('form#contestForm').submit(function(e){
					e.preventDefault();
					
					var form = $(this);
					var jsonFormData = utils.serializeToJSON(form);
					var xhrData = _.pick(jsonFormData, 'artist_id', 'title', 'description', 'prize', 'winners');
					// set the end contest date
					xhrData.endDate = moment(jsonFormData.endDate +' '+jsonFormData.endTime, 'DD-MM-YYYY hh:mm');
					// check if publication date is set and store
					if(jsonFormData.publicationDate && jsonFormData.publicationTime) xhrData.startDate = moment(jsonFormData.publicationDate+' '+jsonFormData.publicationTime, 'DD-MM-YYYY hh:mm');
					// Set question array
					xhrData.questions = [{title:jsonFormData.question, answers:[]}];
					
					// Loop on each answers an push to the array questions
					$('.adinputfield83').each(function(i, v){
						var field = $(v);
						var answer = field.find('input[type="text"]').val();
						var correct = (field.find('input[type="radio"]').is(':checked') === true) ? true : false;
						xhrData.questions[0].answers.push({answer: answer, correct: correct});
					});

					api.post('/i/contest', xhrData, function(response){
						alert('Konkurrencen er blevet oprettet!');
						App.trigger('app:contests:push', response.data);
						App.trigger('popup:contest:close');
					});

				});			

			},
			
		});

		return AdminPanel;
	});