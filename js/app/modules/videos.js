define([

	'app',
	'backbone',
	'unveil',
	'owlCarousel'

],

	function (App, Backbone, unveil, owlCarousel) {

		// Create a new module.
		var Videos = App.module();

		// Create view
		Videos.View = Backbone.View.extend({
			template: 'singleVideo',
			initialize: function() {
				//this.listenTo(this.collection, 'sync', this.render, this);
				//this.collection.fetch();
			},
			serialize: function() {
				return this.collection ? this.collection.toJSON() : [];
			},
			events: {
				'click .goplayvideoLink': 'seeVid'
			},
			
			seeVid: function(e) {
				e.preventDefault();
				
				// SCRIPT TO PUT CLICKED LINK IN IFRAME
				/*
				var videoItem = $(e.currentTarget);
				var tmp = $($(videoItem).html());
				var videoUrl = $($($(videoItem).parent())).find('a').attr('href');
				var iFrame = '<iframe style="border:none" height="720" src="' + videoUrl.replace('https:', '').replace('watch', 'embed/').replace('?v=', '') + '?autohide=1&autoplay=0&iv_load_policy=3&showinfo=0&modestbranding=1&vq=hd480""></iframe>'
				
				$('.video-container').html(iFrame);
				
				$($('#video-container').find('iframe')).attr('src' , videoUrl);
				*/
				// MANIPULATING PAGE CONTENT AND ADDING VIDEOPLAYER
				
				var videoItem = $(e.currentTarget);
				var tmp = $($(videoItem).html());
				var videoUrl = $($($(videoItem).parent())).find('a').attr('href');
				var videoId = videoUrl.replace('https://www.youtube.com/', '').replace('watch', '').replace('?v=', '');
				var iFrame = '<iframe style="border:none" height="720" src="//www.youtube.com/embed/' + videoId + '?autohide=1&autoplay=0&iv_load_policy=3&showinfo=0&modestbranding=1&vq=hd480"></iframe>'
				
				//alert(videoId);
				
				$('.clist').remove();
				$('.h').remove();
				$('.header').removeClass('headerwhite').addClass('header hvid');
				$('.video-container').html(iFrame);
				$('#menubtn-right').remove();
				$('#hlogo').before('<a href="./videoer" id="menubtn-back"></a>');
				//$('#hlogo').attr('src', './assets/css/logo.svg');
			},
			
			afterRender: function() {
				//$("clist.video:gt(11)").hide();
				var videoList =  $(".clist li");
				var videoItem = $(videoList[0]);
				var tmp = $($(videoItem).html());
				var videoUrl = $($($(videoItem).parent())).find('a').attr('href');
				var videoId = videoUrl.replace('https://www.youtube.com/', '').replace('watch', '').replace('?v=', '');
				var iFrame = '<iframe style="border:none" height="720" src="//www.youtube.com/embed/' + videoId + '?autohide=1&autoplay=0&iv_load_policy=3&showinfo=0&modestbranding=1&vq=hd480"></iframe>'
				$('.video-container').html(iFrame);
				$(videoList[0]).remove();
				//alert(.length);
				/*var i = 8;
				$(".btnmore").on("click" , function() {
					i = i + 8;    
					$("article.video:lt(" + i + ")").show();
					if(i > count){
						$(".btnmore").hide();
					}
				});		
				*/
				
				$('#cvid1 img').unveil(600);
			}
		});
		
		Videos.VideosCollection = Backbone.Collection.extend({
			sync: function(method, collection, options){
				var met = 'GET';
				Backbone.ajax({
					method: met,
					url: this.url(),
				}).success(options.success).error(options.error);
			},
			url: function() {
				return 'https://gdata.youtube.com/feeds/api/users/beta/uploads?alt=jsonc&v=2&max-results=48&orderby=published';
			},
			parse: function(res) {
				return res.data.items;    
			}			
		});

		return Videos;
	});