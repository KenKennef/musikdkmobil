define([

	'app',
	'backbone',
	'utils'

],

	function (App, Backbone) {

		// Create a new module.
		var ArtistRelated = App.module();

		// Create view
		ArtistRelated.View = Backbone.View.extend({
			tagName: 'section',
			className: 'ipfs',
			template: 'artistRelated',
			initialize: function() {
				this.listenTo(this.collection, 'sync', this.render);
			},
			serialize: function() {
				return this.collection.toJSON();
			}
		});
		
		ArtistRelated.Collection = Backbone.Collection.extend({
			url: function() {
				return 'https://api.spotify.com/v1/artists/1HY2Jd0NmPuamShAr6KMms/related-artists';
			},		
			parse: function(response){
				return response.artists;
			}			
		});

		return ArtistRelated;
	});