define([

	'app',
	'backbone'

],

	function (App, Backbone) {

		// Create a new module.
		var Error = App.module();

		// Create view
		Error.View = Backbone.View.extend({
			template: 'notfound',
			initialize: function() {
			 var self = this;	
			},
			events: {
				'click .goback' : 'goback',
				'click .gostart' : 'gostart'
			},
			
			goback: function(e) {
				e.preventDefault();
				window.history.go(-2);
			},
			gostart: function() {
				window.location.href=App.URL;
			}
			
		});

		return Error;
	});