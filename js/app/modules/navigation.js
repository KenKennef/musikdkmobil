define([

	'app',
	'backbone'

],

	function (App, Backbone) {

		// Create a new module.
		var Navigation = App.module();
		var ItemView = Backbone.View.extend({
			tagName: 'li',
			attributes : function () {
			return {
			  id : this.model.get( 'id' )
			}},
			template: 'navigationItem',
			serialize: function() {
				return this.model.toJSON();
			},
			initialize: function () {
				if(this.model.has('triggers') && this.model.get('triggers') == this.model.get('route')) {
					this.$el.addClass('active');
				} else {
					this.$el.removeClass('active');
				}	
			}
		});

		// Create view
		Navigation.View = Backbone.View.extend({
			tagName  : 'ul',
			initialize: function() {
				this.listenTo(App, 'navigate', this.triggerRoute);
			},
			triggerRoute: function (pageName) {
				this.route = pageName;
			},
			beforeRender: function () {
				this.insertView(new ItemView({
					model: new Navigation.ItemModel({
						id : 'mhome',
						href: '/',
						class: 'home',
						text: 'Forside',
						triggers: 'home',
						route: this.route
					})
				}));
				this.insertView(new ItemView({
					model: new Navigation.ItemModel({
						id : 'mcomp',
						href: '#konkurrencer',
						class: 'currentinteraction',
						text: 'Konkurrencer',
						triggers: 'konkurrencer',
						route: this.route
					})
				}));				
				this.insertView(new ItemView({
					model: new Navigation.ItemModel({
						id : 'mvid',
						href: '#videoer',
						class: 'currentvideo',
						text: 'Videoer',
						triggers: 'videoer',
						route: this.route
					})
				}));
				/*this.insertView(new ItemView({
					model: new Navigation.ItemModel({
						id : 'mart',
						href: '#bernhoft',
						class: 'currentArtist',
						text: 'Artister',
						triggers: 'artistkanal',
						route: this.route
					})
				}));*/
			}
		});
		Navigation.ItemModel = Backbone.Model.extend({
			defaults: {
				id: '',
				href: '',
				text: '',
				triggers: [],
				route: ''
			}
		});

		return Navigation;
	});