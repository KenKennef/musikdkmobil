define([

	'app',
	'backbone',
	'authentication',
	'swipeScript'
],

	function (App, Backbone, auth, swipeScript) {

		// Create a new module.
		var userTopNavigation = App.module();
		
		// Create View
		var userTopNavigation = Backbone.View.extend({
			//tagName: 'ul',
			//className : 'nm',
			template: 'userNavigationItem',
			model: auth,
			events: {
				'click .mlogin' : 'showHideLogin',
				'click #mluser' : 'logout'
			},

			initialize: function(e){
				var self = this;
				this.model.on('change', function(){
					self.render();
				});

				$('#complogin').easyModal({
					//autoOpen: true,
					closeButtonClass : '.modal-close',
					updateZIndexOnOpen : true,

				});
			},	
			showHideLogin: function(e){
				$('#menubtn-left').click();	
				$('#complogin').trigger('openModal');
				e.preventDefault();
			},
			
			login: function(e){
				$('#menubtn-left').click();	
				$('#complogin').trigger('closeModal');
			},
			
			logout: function(logout) {
				hello.logout();
				this.model.logout();
				$('#menubtn-left').click();
				//window.location.href = window.location.href;
			},
				
		});


		return userTopNavigation;
	});