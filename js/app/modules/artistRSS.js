define([

	'app',
	'backbone',
	'utils',
	'imagesLoaded',
	'moment',
	//'locale',
	'unveil',
	'owlCarousel'
],

	function (App, Backbone, imagesLoaded, moment, /* locale, */unveil, owlCarousel) {

		// Create a new module.
		var ArtistRSS = App.module();

		// Create view
		ArtistRSS.View = Backbone.View.extend({
			tagName : 'div',
			className: 'owl-carousel',
			id : 'cnews',
			template:'artistRSS',
			initialize: function() {
				this.listenTo(this.collection, 'sync', this.hideLoader);
				this.listenTo(this.collection, 'reset', this.afterRender);
			},
			serialize: function() {
				return this.collection.toJSON();
			},
			showLoader: function(){
				$('.artistNews').prepend('<div class="ip tc" id="tmp-ldr"><div class="loading"></div></div>')
			},
			hideLoader: function(){
				$('.artistNews').find('#tmp-ldr').remove();
				this.render();
			},
			events: {
				'click .btnrss': 'showMore'
			},
			showMore: function(){
				//this.collection.fetch({remove: false});
				//this.showLoader();
			},			
			afterRender: function(){
				
			$('#cnews').owlCarousel({
					responsiveClass:true,
					responsive:{
						0:{
							items:1,
							nav:true,
							//autoHeight:true,
							//loop:true /* maybe we should remove this? */
						}
						
					}
				});
			$('#cnews img').unveil(600);	
			}
				
		});
		
		ArtistRSS.Collection = Backbone.Collection.extend({
			comparator: function(data){
				return data.pubDate;
			},
			parse: function(response){
				return response.data.news
			}			
		});

		return ArtistRSS;
	});