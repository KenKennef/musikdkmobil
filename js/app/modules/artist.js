define([
	'app',
	'backbone',
	'modules/artistSocialMedia',
	'modules/artistRSS',
	'owlCarousel',
	'utils',
	'authentication',
	'socketio'
],

	function (App, Backbone, ArtistSocialMedia, ArtistRSS, owlCarousel, utils, auth, io) {

		// Create a new module.
		var Artist = App.module();

		// Create view
		Artist.View = Backbone.View.extend({
			template: 'artistInfo',
			initialize: function() {
				this.listenTo(this.model, 'sync', this);
				
			},
			serialize: function() {
				return this.model.toJSON();
			},
			events: {
				'click .vidPopup': 'seeVid',
				'click .selected': 'selected',
				'click .btnservice' : 'openMusicServices'
			},

			selected: function(e){
				e.preventDefault();
			},
			
			openMusicServices: function(e){
			var musicServices = $('#musicservice');
			musicServices.remove();			
			$('#compmax').after(musicServices);
			$('#musicservice').attr('style', 'width:100%;');
			$('#musicservice').easyModal({
					closeButtonClass : '.modal-close',
					updateZIndexOnOpen : true,
					width: '100%'
				}).trigger('openModal');
				e.preventDefault();			
			},
			
			
			afterRender: function(){
				if (navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/MSIE/)) {
						$('.iebgfix').each(function(){
							var url = $(this).css('background-image');
							$(this).css('background-image', url.replace('.svgz', '.png' ));
						});
					};  
				// LARS LILHOLT BAND SPECIFIC BUTTON
				var downloadBtn = '<a href="https://static.musik.dk/upload/files/various_temp/Ulven_Kommer.mp3" download="Ulven_Kommer.mp3" target="_blank"><div class="ip"><button class="acbtn">Klik her for at downloade "Ulven Kommer" gratis</button></div></a>'

				if(this.model.get('id') == '9559') $('.feat').append(downloadBtn);
				//console.log($($(this.$el).html()).find($('#modal-slide').html()).find('.close-modal').text());
				$(document).prop('title', this.model.get('name') + ' | ' + 'MusikDK');
				utils.removeMetaTags();
				// Facebook meta tags
				utils.setMetaTag('name', 'description', this.model.get('name') + ' på MusikDK. ' + 'Se, hør og læs mere om ' + this.model.get('name') + ' her');
				utils.setMetaTag('property', 'og:title', this.model.get('name'));
				utils.setMetaTag('property', 'og:site_name', 'MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('property', 'og:image', this.model.get('images').cover);
				}
				utils.setMetaTag('property', 'og:locale', 'da_DK');
				utils.setMetaTag('property', 'og:type', 'Website');	
				utils.setMetaTag('property', 'og:url', 'https://musik.dk/' + this.model.get('slug'));
				utils.setMetaTag('property', 'og:description', 'Se, hør og læs mere om ' + this.model.get('name') + ' her');
				
				// Twitter cards 
				utils.setMetaTag('name', 'twitter:title', this.model.get('name') + ' | MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('name', 'twitter:image', this.model.get('images').cover);
				}
				utils.setMetaTag('name', 'twitter:url', 'https://musik.dk/' + this.model.get('slug'));
				utils.setMetaTag('name', 'twitter:description', 'Se, hør og læs mere om ' + this.model.get('name') + ' her');				
				

				var artistsocialmediaCollection = new ArtistSocialMedia.Collection();
				artistsocialmediaCollection.url = this.model.url() + '/feed';
				artistsocialmediaCollection.fetch({
					error: function() {
							$('#tmp-ldr').remove();
							$('#socialMediaDiv').remove();
					},				
					success: function(collection) {
						if (collection.length === 0) {
							$('#socialMediaDiv').remove();
						}
					}
				});
				this.insertView('.social', new ArtistSocialMedia.View({collection: artistsocialmediaCollection}));
				
				// INSERT DEFAULT IMAGE IN SOCIAL FEED IF  NO IMAGE
				$('body').append('<input type="hidden" id="socialStandardImage" value="'+ this.model.get('images').cover +'" />');
				
				var artistrssCollection = new ArtistRSS.Collection();
				artistrssCollection.url =  this.model.url() + '/news';
				artistrssCollection.fetch({
					error: function() {
							$('#tmp-ldr').remove();
							$('#rssDiv').remove();
					},
					success: function(collection) {
						if (collection.length === 0) {
							$('#rssDiv').remove();
						}
					}
				});
				this.insertView('.artistNews', new ArtistRSS.View({collection: artistrssCollection}));
				
				// Videos mobile			
				$('#cvid').owlCarousel({
					lazyLoad : true,
					responsiveClass:true,
					responsive:{
						0:{
							items:1,
							nav:true,
							loop:true /* maybe we should remove this? */
						}
						
					}
				});
				
				// Mobile- unveil images				
				$('#cvid img').unveil(600);
				
				// Concerts mobile
				$('#ccon').owlCarousel({
					lazyLoad : true,
					responsiveClass:true,
					responsive:{
						0:{
							items:1,
							nav:true,
							//autoHeight:true,
							loop:true /* maybe we should remove this? */
						}
						
					}
				});
				
				// Mobile- unveil images				
				$('#ccon img').unveil(600);
				
				
				// Mobile biography script if 'txt' length is smaller than 'length' toggle class acbio-full
				var txt = $('#acbio p').text();
				var length = 200;
				(txt.length < length) ? $('#acbio').toggleClass('acbio-full') : $('#acbio').on('click',function(){$('#acbio').toggleClass('acbio-full');});
				
				// Check if polls exists to initialize realtime and user is authenticated
				if(this.model.get('polls').length){	

					// Initialize all polls to default
					var itm, initialized = false;
					$('div[data-poll-item-id]').each(function(){
						itm = $(this).find('#progresspoll');
						itm.css('width', '2.5%');
						itm.attr('data-procentage', 0);
					});


					function changePollBarAttrs(pollItemId, percent){
						itm = $('div[data-poll-item-id="'+pollItemId+'"]').find('#progresspoll');
						itm.attr('data-procentage', percent);
						if(percent < 2.5 )
							return itm.css('width', '2.5%');
						if(initialized) 
							itm.css('width',  percent+'%');
						else 
							itm.delay(1500).css('width',  percent+'%');
					};

					// Set the current polls votes
					var pollsQuestions = this.model.get('polls')[0].pollItems;

					$.each(pollsQuestions, function(i, v){
						changePollBarAttrs(v.pollItemId, v.percentage);
					});

					function activateItem(pollItemId){
						$('.poll[data-poll-item-id="'+pollItemId+'"]').attr('class', 'pollactive');
					}

					function deactivateItem(pollItemId){
						$('.pollactive[data-poll-item-id="'+pollItemId+'"]').removeClass('pollactive').attr('class', 'poll');
					}

					var socket;
					var poll_id = this.model.get('polls')[0].poll_id;
					// Emmit our current poll to join as room
					function connectSocket(){
						if(auth.isLogged()){
							socket = io(App.APIO, {
								query: 'token='+auth.__getToken(),
								forceNew: 'true'
							});
							socket.on('artist:poll:vote:set:active', activateItem);
						}
						else 
						{
							socket = io(App.APIO, {forceNew: 'true'});
							// deselect all items as non voted
							$('.pollactive').attr('class', 'poll');
						}
						socket.emit('artist:poll:join', poll_id);
					}

					App.on('user:login:status:changed', connectSocket);

					connectSocket();

					// listen when changes on poll
					socket.on('artist:poll:votes:new', function(pollsItems){
						$.each(pollsItems, function(i,v){
							changePollBarAttrs(v.pollItemId, v.percentage);
						});
					});

					socket.on('artist:poll:vote:set:deactive', deactivateItem);

					// listen to activate poll Items when all the code was executed
					socket.on('artist:poll:vote:set:active', activateItem);
					
					socket.on('artist:poll:vote:max:reached', function(){
					  $('#compmax').easyModal({
								//autoOpen: true,
								closeButtonClass : '.modal-close',
								updateZIndexOnOpen : true
							}).trigger('openModal');
					 });


					var triggerVote = function(e){

						// Check login for non-authorized people to vote
						if(!auth.isLogged()) {
							$('#complogin').easyModal({
								//autoOpen: true,
								closeButtonClass : '.modal-close',
								updateZIndexOnOpen : true
							}).trigger('openModal');
							return;					
						}
						var votes = $('.pollactive');
						if(votes.length == 4){
						$('#compmax').easyModal({
								//autoOpen: true,
								closeButtonClass : '.modal-close',
								updateZIndexOnOpen : true
							}).trigger('openModal');
						}
						else{
						var pollItem = $(this);
						// Get the id of this poll
						pollItemId = pollItem.data('poll-item-id');
						socket.emit('artist:poll:vote:me', pollItemId);
						}
					};


					$('.pollactive').click(function(e){
						$('#comperror').easyModal({
								//autoOpen: true,
								closeButtonClass : '.modal-close',
								updateZIndexOnOpen : true
							}).trigger('openModal');
						$('.compbtnwrap').remove();
					});
					$('.poll').click(triggerVote);

				}
				

			},
		
						
			seeVid: function(e) {
				e.preventDefault();
				var videoItem = $(e.currentTarget);
				this.openVidPopup(videoItem, videoItem.data('ytid'));
			},
			
			openVidPopup: function(jqueryRef, videoId){

				var slug = this.model.get('slug');
				$.magnificPopup.open({
					disableOn: 700,
					items: {
						src: 'https://www.youtube.com/watch?v='+videoId
					},
					type: 'iframe',
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: false,
					fixedContentPos: false,
					callbacks: {
						open: function(){
							Backbone.history.navigate(slug+'/videoer?w='+videoId, {replace: true}); 
						},
						close: function(){
							Backbone.history.navigate(slug, {replace: true}); 
						}
					}
				});

			},				
			
			openVideoPopupByYoutubeId: function(videoId){
				var slug = this.model.get('slug');
				// We need to create a item that will hold the video document element by hidden in the DOM
				$.magnificPopup.open({
					items: {
						src: 'https://www.youtube.com/watch?v='+videoId
					},
					type: 'iframe',
					disableOn: 700,
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: false,
					fixedContentPos: false,
					callbacks: {
						close: function(){
							Backbone.history.navigate(slug, {replace: true}); 
						}
					}
				});
			}		
		});
		
		Artist.Model = Backbone.Model.extend({
			url: function() {
				return App.APIO + '/i/artist/' + this.get('slug');
			},
			parse: function(response){
				return response.data;
			},
		});		

		return Artist;
	});