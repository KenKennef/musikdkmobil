define([

	'app',
	'backbone'

],

	function (App, Backbone) {

		// Create a new module.
		var Terms = App.module();

		// Create view
		Terms.View = Backbone.View.extend({
			template: 'termsDetails',
			initialize: function() {
			 var self = this;	
			},
		});

		return Terms;
	});