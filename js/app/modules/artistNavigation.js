define([

	'app',
	'backbone'

],

	function (App, Backbone) {

		// Create a new module.
		var artistNavigation = App.module();

		// Create view
		var artistNavigation.View = Backbone.View.extend({
			tagName: 'ul',
			className :'nm',
			template: 'userNavigationItem',
			initialize: function() {
			 var self = this;	
			}
		});
		
		ArtistNavigation.Model = Backbone.Model.extend({
			url: function() {
				return 'http://private-c439c-musik.apiary-mock.com/i/artist/' + this.get('slug');
			},
			parse: function(response){
				return response.data;
			},
		});		

		return ArtistNavigation;
	});