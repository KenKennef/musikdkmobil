define([

	'app',
	'backbone',
	'imagesLoaded',
	'utils',
	'owlCarousel',
	'unveil'

],

	function (App, Backbone, imagesLoaded, utils, owlCarousel, unveil) {

		// Create a new module.
		var ArtistMerchandise = App.module();

		// Create view
		ArtistMerchandise.View = Backbone.View.extend({
			tagname: 'div',
			className : 'standardLayoutDiv',
			template: 'artistMerchandise',
			initialize: function() {
				this.listenTo(this.model, 'sync', this.render);	
			},
			serialize: function() {
				return this.model.toJSON();
			},
			
			events: {
				'click .selected': 'selected'
			},

			selected: function(e){
				e.preventDefault();
			},
			
			afterRender:function(){
				

				utils.removeMetaTags();
				
				$(document).prop('title', this.model.get('name') + ' | ' + 'Merchandise' + ' | ' + 'MusikDK');
				utils.setMetaTag('name', 'description', this.model.get('name') + ' på MusikDK. ' + 'Se, hør og læs mere om ' + this.model.get('name') + ' her');
				
				// Facebook meta tags
				utils.setMetaTag('property', 'og:title', 'Merchandise fra ' + this.model.get('name'));
				utils.setMetaTag('property', 'og:site_name', 'MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('property', 'og:image', this.model.get('images').cover);
				}
				utils.setMetaTag('property', 'og:locale', 'da_DK');	
				utils.setMetaTag('property', 'og:description', 'Se og køb merchandise fra ' + this.model.get('name'));
				utils.setMetaTag('property', 'og:url', 'https://musik.dk/' + this.model.get('slug') + '/merchandise');
				
				// Twitter cards 
				utils.setMetaTag('name', 'twitter:title', ' Merchandise fra ' + this.model.get('name') + ' | MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('name', 'twitter:image', this.model.get('images').cover);
				}
				utils.setMetaTag('name', 'twitter:url', 'https://musik.dk/' + this.model.get('slug') + '/merchandise');
				utils.setMetaTag('name', 'twitter:description', 'Se og køb merchandise fra ' + this.model.get('name'));						
				
				if(this.model.get('images')){
					var cover = this.model.get('images').cover || null;
				}
				utils.changeBackground(cover);	
				
				
				$('.headerwhite').find('h1').html(this.model.get('name'));
				
				$('#1').owlCarousel({
					loop:true,
					responsiveClass:true,
					responsive:{
						0:{
							items:1,
							nav:true,
							autoHeight:true,
							loop:true 
						}
					}
				});
				$('.owl-stage-outer').attr('style', 'height:100%');
				$('img').unveil(600);
				this.$el.imagesLoaded(function(){
				
					/*$('#suptiles li').wookmark({
						autoResize: true,
						container: $('#merch'),
						offset: 10,
						outerOffset: 10,
						fillEmptySpace: true,
						itemWidth: 280,
						flexibleWidth: 350
					});*/
					
				});

				
			}
		});
		
		ArtistMerchandise.ArtistMerchandiseModel = Backbone.Model.extend({
			url: function() {
				return App.APIO + '/i/artist/' + this.get('slug') + '/merchandise';
			},		
			parse: function(response){
				return response.data;
			},		
		});	

		return ArtistMerchandise;
	}
);