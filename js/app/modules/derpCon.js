define([

	'app',
	'backbone',
	'imagesLoaded',
	'owlCarousel'
],

	function (App, Backbone, owlCarousel) {

		// Create a new module.
		var DerpCon = App.module();

		// Create view
		DerpCon.View = Backbone.View.extend({
			template: 'derpcon',
			initialize: function() {
				this.listenTo(this.model, 'sync', this.render);	
			},
			serialize: function() {
				return this.model.toJSON();
			},
			afterRender: function(){
				$('#derpvid').owlCarousel({
					lazyLoad : true,
					responsiveClass:true,
					responsive:{
						0:{
							items:1,
							nav:true,
							//autoHeight:true,
							loop:true /* maybe we should remove this? */
						}
						
					}
				});
				$('img').unveil(100);
			}			
		});
		
		DerpCon.DerpConModel = Backbone.Model.extend({
			url: function() {
				return App.APIO + '/i/artist/' + this.get('slug') + '/derpcon';
			},		
			parse: function(response){
				return response.data;
			},		
		});			

		return DerpCon;
	});