define([

	'app',
	'backbone',
	'authentication'
],

	function (App, Backbone, auth) {

		// Create a new module.
		var artistChannelUserNav = App.module();
		
		// Create View
		var artistChannelUserNav = Backbone.View.extend({
			tagName: 'ul',
			className : 'nm',
			template: 'userNavigationItem',
			model: auth,
			events: {
				'click .mlogin' : 'showHideLogin',
				'click #mluser' : 'logout'
			},

			initialize: function(e){
				var self = this;
				this.model.on('change', function(){
					self.render();
				});
			},
			showHideLogin: function(e){
				$('#menubtn-left').click();	
				$('#complogin').trigger('openModal');
				e.preventDefault();
			},
			
			login: function(e){
				$('#menubtn-left').click();	
				$('#complogin').trigger('closeModal');
			},
			
			logout: function(logout) {
				hello.logout();
				this.model.logout();
				$('#menubtn-left').click();
				//window.location = window.location.href;
			}
				
		});


		return artistChannelUserNav;
	});