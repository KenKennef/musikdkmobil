define([

	'app',
	'backbone'

],

	function (App, Backbone) {

		// Create a new module.
		var DerpConTerms = App.module();

		// Create view
		DerpConTerms.View = Backbone.View.extend({
			template: 'derpcon_terms',
			initialize: function() {
			 var self = this;	
			}
		});

		return DerpConTerms;
	});