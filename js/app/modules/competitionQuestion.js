define([

	'app',
	'backbone',
	'API',
	'authentication',
	'utils'
],

	function (App, Backbone, API, auth, utils) {

		// Create a new module.
		var CompetitionQuestion = App.module();

		// Create view
		CompetitionQuestion.View = Backbone.View.extend({
			//tagName: 'div',
			//className: 'questionDetails',
			template: 'competitionQuestion',
			serialize: function() {
				return this.model.toJSON();
			},
			
			initialize : function(e){
				
				$('#compyes').easyModal({
					//autoOpen: true,
					closeButtonClass : '.modal-close',
					updateZIndexOnOpen : true

				});
				$('#comperror').easyModal({
					//autoOpen: true,
					closeButtonClass : '.modal-close',
					updateZIndexOnOpen : true

				});
			},
			
			events: {
				'click .btncomp': 'answer'
			},
			updateBackground: function(){
				//utils.changeBackground(this.model.get('artist').artist_standard);
			},
			
			/*
			 *	This function marks the answer that the user marked before as selected.
			 */
			markSelectedAnswer: function(){
				var question = this.model.get('question');
				if(!question.answered) return;
				this.$el.find('button[data-answer-id="'+question.answered.answer_id+'"]').addClass('btngreen');
			},
			
			answer: function(e) {
				e.stopPropagation();
				

				// Check if the user have answered before this question
				if(this.model.get('question').answered){
					function error_popup() {
							$('#comperror').trigger('openModal');
							e.preventDefault();
						}
					return error_popup();
						
				}
				
				var btn = $(e.target);
				var reqObj = {
					answer_id: btn.data("answer-id"),
					question_id: this.model.get('question').question_id
				};
				if(auth.isLogged()) {
					var self = this;
					btn.addClass('btncomp');
					API.post('/i/contest/' + this.model.get('contest_id') + '/answer', reqObj, function(response){  
					
						// Added a method to register that the user already answered this contest
						var question = self.model.get('question');
						question.answered = {answer_id: reqObj.answer_id};
						self.model.set('question', question);
											
						function answered_popup() {
							$('#compyes').trigger('openModal');
							e.preventDefault();
						}
						return answered_popup();
						
					});
				} else {

				$('#complogin').easyModal({
					//autoOpen: true,
					closeButtonClass : '.modal-close',
					updateZIndexOnOpen : true,
					onClose: function(){
						window.location = window.location.href;
					}
				}).trigger('openModal');
					
				}
			},
			afterRender: function() {
				this.markSelectedAnswer();
				//this.updateBackground();

				$(".countdown").countdown({
				  date: this.model.get('endDate'), 
				  format: "on"
				},

				function() {
				  // the code here will run when the countdown ends
				  // Add popup here
				 //$.magnificPopup.open();

				});	
			$('img').unveil(600);
			$('.btnblue').html('Se mere med ' + this.model.get('artist').artist_name);
			$('.artistchannelLink').attr('href', '../' + this.model.get('artist').artist_slug);
			}
			
		});
		CompetitionQuestion.Model = Backbone.Model.extend({
			url: function() {
				return App.APIO + '/i/contest/' + this.get('contest_id');
			},

			parse: function(response){
				return response.data;
			},
				
		});

		return CompetitionQuestion;
	});