define([

	'app',
	'backbone',
	'utils',
	'imagesLoaded',
	'moment',
	'owlCarousel',
	'unveil'
	//'locale'
],

	function (App, Backbone, imagesLoaded, moment, owlCarousel, unveil) {

		// Create a new module.
		var ArtistSocialMedia = App.module();

		// Create view
		ArtistSocialMedia.View = Backbone.View.extend({
			tagName: 'div',
			className :'owl-carousel',
			id: 'csocial',			
			template:'artistSocialMedia',
			initialize: function() {
				//this.currentPage = 0;
				this.showLoader();
				this.listenTo(this.collection, 'sync', this.hideLoader);
			},
			serialize: function() {
				return this.collection.toJSON();
			},
			showLoader: function(){
				$('.social').prepend('<div class="tc" id="tmp-ldr"><div class="loading"></div></div>')
			},
			hideLoader: function(){
				$('.social').find('#tmp-ldr').remove();
				this.render();
			},
			events: {
				'click .btnsocial': 'showMore'
			},
			showMore: function(){
				//this.currentPage++;
				//this.collection.fetch({url: this.collection.url + '?page='+ this.currentPage, remove: false});
				//this.collection.fetch({remove: false});
			},
			afterRender: function(){
			
				$('#csocial').owlCarousel({
					lazyLoad : true,
					responsiveClass:true,
					responsive:{
						0:{
							items:1,
							nav:true,
							autoHeight:true,
						}
					}
				});				
				// FETCHING AND REPLACING IMAGE URL FROM HTML HIDDEN FIELD "socialStandardImage"
					$('.social article a img').each(function(){
						if($(this).attr('alt') == 'empty'){
							$(this).attr('data-src', $('#socialStandardImage').attr('value'));
							$(this).attr('src', '');
						}
					});	
									
					$("img").unveil(100);
					$('#socialStandardImage').remove();
					// Set height of social owl-carousel to height of first element
					$('.social .owl-stage-outer').ready(function(){						
						$('.social .owl-stage-outer').css('height', $('.social .owl-item.active').css('height'));
					});
	
				
			}			
		});
		
		ArtistSocialMedia.Collection = Backbone.Collection.extend({
			comparator: function(data){
				return data.pubDate;
			},
			step: 0,
			parse: function(response){
				var slice = response.data.posts.slice(this.step*5,(this.step+1)*5)
				this.step++;
				return response.data.posts;//slice;
			}
		});

		return ArtistSocialMedia;
	});