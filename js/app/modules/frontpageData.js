define([

	'app',
	'backbone',
	'API',
	'authentication',
	'utils',
	'select2'
],

	function (App, Backbone, API, auth, utils, select2) {

		// Create a new module.
		var FrontPageData = App.module();

		// Create view
		FrontPageData.View = Backbone.View.extend({
			tagName: 'div',
			id: 'frontData',
			template: 'frontPageDataInput',
			initialize: function() {
			 var self = this;	
			},
			beforeRender: function(){
			},
			afterRender: function(){
				
				$(".select-multiple-artists").select2({
					minimumInputLength: 1,
					multiple: true,
					query: function(query){

						$.getJSON( App.APIO+'/i/search/artist', {
						  name:query.term, 
						  featured:true
						 }, function(results){

						 	var data = {results: []};
						 	$.each(results.data, function(index, item){
						 		data.results.push({id: item.artist_id, text: item.name});
						 	});
						 	query.callback(data);	

						 } );
					}
				});	

				// function that takes the artist selected and sends it to backend
				$(".updater-btn").click(function(e){

					// get section
					var itm = $(this), 
						section = itm.parent(),
						list = section.find('.select-multiple-artists');

					// create object
					var objReq = {
						section: section.data('section'),
						key: section.data('key'),
						artists: []
					};

					// push artists to objReq
					var artistSelectedArray = list.select2('val');
					$.each(artistSelectedArray, function(i, v){
						objReq.artists.push({artist_id: v});
					});

					API.post('/s/frontpage', objReq, function(response){
						alert('Section: '+section.find('h2').html()+' updated' );
						//section.find('button').prop('disabled', 'disabled');
					});

				});
			
			}
		});
		

		return FrontPageData;
	});