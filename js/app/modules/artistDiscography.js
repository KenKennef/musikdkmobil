define([

	'app',
	'backbone',
	//'magnificPopup',
	'utils',
	'modules/tinadickowWhispers'

],

	function (App, Backbone, /* magnificPopup, */ utils, TinaDickowWhispers) {

		// Create a new module.
		var ArtistDiscography = App.module(); 

		// Create view
		ArtistDiscography.View = Backbone.View.extend({
			tagname : 'div',
			className : 'standardLayoutDiv',
			template: 'artistReleases',
			initialize: function() {
				this.listenTo(this.model, 'sync', this.render);
				if (navigator.userAgent.match(/Trident/) && !navigator.userAgent.match(/MSIE/)) {
					$('.iebgfix').each(function(){
						var url = $(this).css('background-image');
						$(this).css('background-image', url.replace('.svgz', '.png' ));
					});
				}				
			},
			serialize: function() {
				return this.model.toJSON();
			},		
			events: {
				'click .albumolfull': 'listenAlbum',
				'click .selected': 'selected'
			},

			selected: function(e){
				e.preventDefault();
			},
			
			listenAlbum: function(e) {
				e.preventDefault();
				// get the div that contains all the services
				var linksHTML = $(e.currentTarget).find('.album-services').html();
				this.openMusicService(linksHTML);
			},

			openMusicService: function(linksHTML){
				$('#pickservice').html(linksHTML);
				/*$.magnificPopup.open({
					items: {
						src: '#pickservice'
					},
					closeBtnInside: false,
					type: 'inline',
					disableOn: 700,
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: false,
					fixedContentPos: false,
				});*/
			},			
			
			afterRender: function(){
				utils.removeMetaTags();
				
				$(document).prop('title', this.model.get('name') + ' | ' + 'Udgivelser' + ' | ' + 'MusikDK');
				utils.setMetaTag('name', 'description', this.model.get('name') + ' på MusikDK. ' + 'Se, hør og læs mere om ' + this.model.get('name') + ' her');
				
				if(this.model.get('id') === 17617){
					$('.tinaDickowNewestAlbum').append('<div id="tdw"></div>');
					this.tinadickowView = new TinaDickowWhispers.View({el: '#tdw'});
				}	
				
				
				// Facebook meta tags
				utils.setMetaTag('property', 'og:title', 'Udgivelser fra ' + this.model.get('name'));
				utils.setMetaTag('property', 'og:site_name', 'MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('property', 'og:image', this.model.get('images').thumb);
				}
				utils.setMetaTag('property', 'og:locale', 'da_DK');	
				utils.setMetaTag('property', 'og:description', 'Hør og se alle udgivelser fra ' + this.model.get('name'));
				utils.setMetaTag('property', 'og:url', 'https://musik.dk/' + this.model.get('slug') + '/udgivelser');
				
				// Twitter cards 
				utils.setMetaTag('name', 'twitter:title', 'Udgivelser fra ' + this.model.get('name') + ' | MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('name', 'twitter:image', this.model.get('images').cover);
				}
				utils.setMetaTag('name', 'twitter:url', 'https://musik.dk/' + this.model.get('slug') + '/udgivelser');
				utils.setMetaTag('name', 'twitter:description', 'Hør og se alle udgivelser fra ' + this.model.get('name'));						
				
				if(this.model.get('images')){
					var cover = this.model.get('images').cover || null;
				}
				//utils.changeBackground(cover);		
				$('.headerwhite').find('h1').html(this.model.get('name'));
				$('img').unveil('600');
			}
		});
		
		ArtistDiscography.ArtistDiscographyModel = Backbone.Model.extend({
			url: function() {
				return App.APIO + '/i/artist/' + this.get('slug') + '/releases';
			},		
			parse: function(response){
				return response.data;
			},

		});
		return ArtistDiscography;
	});