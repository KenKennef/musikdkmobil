define([
	'jquery',
	'app',
	'backbone',
	//'magnificPopup',
	'utils',
	'owlCarousel',
	'unveil'
],

	function ($, App, Backbone, /*magnificPopup, */ utils, owlCarousel, unveil) {

		// Create a new module.
		var ArtistVideos = App.module();

		// Create view
		ArtistVideos.View = Backbone.View.extend({
			tagname : 'div',
			className : 'standardLayoutDiv',
			template: 'artistVideos',
			initialize: function() {
				this.listenTo(this.model, 'sync', this.render);
			},
			events: {
				'click .vidPopup': 'seeVid',
				'click .selected': 'selected'
			},

			selected: function(e){
				e.preventDefault();
			},
			
			serialize: function() {
				return this.model.toJSON();
			},		
			afterRender: function(){
				utils.removeMetaTags();
				$(document).prop('title', this.model.get('name') + ' | ' + 'Videoer' + ' | ' + 'MusikDK');
				utils.setMetaTag('name', 'description',  this.model.get('name') + ' på MusikDK. ' + 'Se, hør og læs mere om ' + this.model.get('name') + ' her');
				utils.setMetaTag('property', 'og:title', 'Videoer fra ' + this.model.get('name'));
				utils.setMetaTag('property', 'og:site_name', 'MusikDK');
				if(this.model.get('images')){
					utils.setMetaTag('property', 'og:image', this.model.get('images').thumb);
				}
				utils.setMetaTag('property', 'og:locale', 'da_DK');	
				utils.setMetaTag('property', 'og:description',  'Se alle videoer og interviews med ' + this.model.get('name'));
				utils.setMetaTag('property', 'og:url', 'https://musik.dk/' + this.model.get('slug') + '/videoer');
				
				// Detect that the view is getting a parameter of video id that must be open when the video is initialized.
				if(this.video) this.openVideoPopupByYoutubeId(this.video);
				if(this.model.get('images')){
					var cover = this.model.get('images').cover || null;
				}
				//utils.changeBackground(cover);
				
				$('img').unveil(600);
			},
			openVideoPopupByYoutubeId: function(videoId){
				var slug = this.model.get('slug');
				
				
				// MANIPULATING PAGE CONTENT AND ADDING VIDEOPLAYER
				var iFrame = '<iframe style="border:none" height="720" src="//www.youtube.com/embed/' + videoId + '?autohide=1&autoplay=0&iv_load_policy=3&showinfo=0&modestbranding=1&vq=hd480"></iframe>'
				var videoContainer = '<div class="video-container"></div>';
				$('#swipe').append(videoContainer);
				$('.clist').remove();
				$('.h').remove();
				$('.header').removeClass('headerwhite').addClass('header hvid');
				$('.video-container').html(iFrame);
				$('#menubtn-right').remove();
				$('#hlogo').before('<a href="javascript: history.go(-1)" id="menubtn-back"></a>');
				$('#hlogo').attr('src', '//static.musik.dk/upload/mobile/logo.svgz');
				
				
				// We need to create a item that will hold the video document element by hidden in the DOM
				/*$.magnificPopup.open({
					items: {
						src: 'https://www.youtube.com/watch?v='+videoId
					},
					type: 'iframe',
					disableOn: 700,
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: false,
					fixedContentPos: false,
					callbacks: {
						close: function(){
							Backbone.history.navigate(slug+'/videoer', {replace: true}); 
						}
					}
				});*/
			},
			seeVid: function(e){
				e.preventDefault();
				var videoItem = $(e.currentTarget);
				this.openVidPopup(videoItem, videoItem.data('ytid'));
			},
			openVidPopup: function(jqueryRef, videoId){

				var slug = this.model.get('slug');
				/*$.magnificPopup.open({
					disableOn: 700,
					items: {
						src: 'https://www.youtube.com/watch?v='+videoId
					},
					type: 'iframe',
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: false,
					fixedContentPos: false,
					callbacks: {
						open: function(){
							Backbone.history.navigate(slug+'/videoer?w='+videoId, {replace: true}); 
						},
						close: function(){
							Backbone.history.navigate(slug+'/videoer', {replace: true}); 
						}
					}
				});
			*/
			}
		});
		
		ArtistVideos.Model = Backbone.Model.extend({
			url: function() {
				return App.APIO + '/i/artist/' + this.get('slug') + '/videos';
			},
			parse: function(response){
				return response.data;
			}
		});
		return ArtistVideos;
	});