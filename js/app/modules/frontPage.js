define([

	'app',
	'backbone',
	'utils',
	//'modules/alphabet',
	'lazySizes'

],

	function (App, Backbone, utils, Alphabet, lazySizes) {

		// Create a new module.
		var FrontPage = App.module();
		var genre = 'world';
		var genres;
		var decades;
		var jSonObj;
		var videos;

		// Create view
		FrontPage.View = Backbone.View.extend({

			template: 'frontPage',
			initialize: function() {
				this.listenTo(this.collection, 'add', this.render, this);
			},
			
			serialize: function() {
				return this.collection ? this.collection.toJSON() : [];
			},
			
			selectFromGenreList: function(genre){
			items = [];
			(genre != 'Alle genrer') ? items = this.getObjects('genre', jSonObj[0], genre.toLowerCase()) : items = this.getObjects('genre', jSonObj[0], 'poprock');
			this.renderGenreToPage(items);
			},
			
			selectFromYearList: function(decade){
			items = [];	
			(decade != 'Alle år') ? items = this.getObjects('decades', jSonObj[0], decade):items = this.getObjects('decades', jSonObj[0], '2010');
			this.renderYearToPage(items);
			},
			
			renderGenreToPage: function(items){				
				var count = 0;
				var alist = '';
				$('#art3').html('<ul class="owl-carousel"></ul>');
				$(items).each(function(){	
					if(count % 2 == 0)alist += '<ul class="alist">';
					alist += '<li><a href="/' + this.slug + '"><img class="lazyload" data-src="' + this.images['thumb'] + '" alt="' + this.name + '" /><span><h2>' + this.name + '</h2></span></a></li>';
					if(count % 2 == 1)alist += '</ul>';
					count++;
				});	
				if(count > 1 && count % 2 == 1)alist += '</ul>';				
				$("#art3 .owl-carousel").append(alist);
				$("#art3 .owl-carousel").owlCarousel({ 
					lazyLoad:true,
					loop:true,
					responsiveClass:true,
					responsive:{
						0:{
							items:1,
							autoHeight:true,
							nav:true,
							loop:true /* maybe we should remove this? */
						}
					}
				});
				//$('#art3 .owl-carousel img').unveil(600);
			},
			
			renderYearToPage: function(items){				
				var count = 0;
				var alist = '';
				$('#art4').html('<ul class="owl-carousel"></ul>');
				$(items).each(function(){	
					if(count % 2 == 0)alist += '<ul class="alist">';
					alist += '<li><a href="/' + this.slug + '"><img class="lazyload" data-src="' + this.images['thumb'] + '" alt="' + this.name + '" /><span><h2>' + this.name + '</h2></span></a></li>';
					if(count % 2 == 1)alist += '</ul>';
					count++;
				});	
				if(count > 1 && count % 2 == 1)alist += '</ul>';				
				$("#art4 .owl-carousel").append(alist);
				$("#art4 .owl-carousel").owlCarousel({ 
					lazyLoad:true,
					loop:true,
					responsiveClass:true,
					responsive:{
						0:{
							items:1,
							nav:true,
							loop:true /* maybe we should remove this? */
						}
					}
				});
				//$('#art4 .owl-carousel img').unveil(600);
			},
			
			renderVideoToPage: function(items){		
					var count = 0;
					var clist = '';
					$('#cvid').html('<ul class="owl-carousel"></ul>');
					$(items).each(function(){	
						if(count % 2 == 0)clist += '<ul class="clist">';
						clist += '<li><a href="/' + this.slug + '/videoer?w=' + this.youtube_id + '"><img class="lazyload" data-src="https://i1.ytimg.com/vi/' + this.youtube_id + '/hqdefault.jpg" alt="' + this.name + '" /><span><h2>' + this.title + '</h2><h3>' + this.name + '</h3></span></a></li>';
						if(count % 2 == 1)clist += '</ul>';
						count++;
					});	
					if(count > 1 && count % 2 == 1)clist += '</ul>';				
					$("#cvid .owl-carousel").append(clist);
					$("#cvid .owl-carousel").owlCarousel({ 
						lazyLoad:true,
						loop:true,
						responsiveClass:true,
						responsive:{
							0:{
								items:1,
								nav:true,
								loop:true /* maybe we should remove this? */
							}
						}
					});
					//$('img').unveil(600);
			},
			
			populateGenreList: function(){	
				$('#selectGenre').append('<option>Alle genrer</option>');
				for(var i = 0; i < Object.keys( genres ).length; i++){
					$('#selectGenre').append('<option>' + Object.keys( genres )[i] + '</option>');
				}
				this.selectFromGenreList('poprock');
			},
			
			populateYearList: function(){	
				$('#selectYear').append('<option>Alle år</option>');
				for(var i = 0; i < Object.keys( decades ).length; i++){
					$('#selectYear').append('<option>' + Object.keys( decades )[i] + '</option>');
				}
				this.selectFromYearList('2010');
			},
			
			/* Get objects from json based on listname,  key */
			getObjects: function(list, obj, key) {
				var objects = [];
				if(list == 'genre'){
					for (var i = 0; i < Object.keys( genres ).length; i++) {
							if (Object.keys( genres )[i] == key) {
								$(eval(obj.genres[key])).each(function(){
									objects.push(this);
								});						
								
							}
					}	
				}
				if(list == 'decades'){
					for (var i = 0; i < Object.keys( decades ).length; i++) {
							if (Object.keys( decades )[i] == key) {
								$(eval(obj.decades[key])).each(function(){
									objects.push(this);
								});						
								
							}
					}	
				}
				if(list == 'videos'){
					for (var i = 0; i < Object.keys( videos ).length; i++) {
							if (Object.keys( videos )[i]) {
								$(eval(obj.videos[i])).each(function(){
									objects.push(this);
								});						
								
							}
					}	
				}
				return objects;
			},
			
			/* Call api json for json obj and set variables */
			fetchJson:function(){	
					jSonObj = this.collection ? this.collection.toJSON() : [];
					genres = eval(jSonObj[0].genres);
					decades = eval(jSonObj[0].decades);
					videos = eval(jSonObj[0].videos);
					this.populateGenreList();
					this.populateYearList();
					var items = this.getObjects('videos', jSonObj[0], new RegExp('/[a-z]+/'));
					this.renderVideoToPage(items);
			},
			
			afterRender: function(){
			this.fetchJson();			
			var self = this;
				// Listeners for artist genre/ year
				$('#selectGenre').change(function(e){
					self.selectFromGenreList($(e.target).val());
				});
				$('#selectYear').change(function(e){
					self.selectFromYearList($(e.target).val());
				});
				
				// Set cnews carousel
				$('#cnews').owlCarousel({
					lazyLoad : true,
					responsiveClass:true,
					responsive:{
						0:{							
							items:1,
							nav:true,
						}
						
					}
				});
				
				// Check competitions lenght and show more btn if > 1
				//$("#ccon ul li:gt(0)").hide();
				var countComp =  $("#ccomp li").length;
				var competitionsList =  $(".nlist li");
				var i = 0;
				$(competitionsList).each(function(){
					if(i >= 1){
					$(this).remove();					
					}
					i++;
				});
				
				// If more that 1 show btn
				if(countComp >= 2){
					$(".compBtnDiv").show();
				}

				// Check concerts lenght and set to 4
				var concertsList =  $("#ccon .llist");
				var i = 0;
				$(concertsList).each(function(){
					if(i >= 4)$(this).remove();
					i++;
				});
							
				
				// Set height of competition owl-carousel to height of first element
				$('#ccomp .owl-stage-outer').ready(function(){						
					$('#ccomp .owl-stage-outer').css('height', $('#ccomp .active').css('height'));
				});
				
				// Script for click/ Hold
				var element = $('.contestImageLink');
				$(element).on('click', function () {
					if(longpress) return false;
				});

				$(element).on('mousedown', function () {
					longpress = false; //longpress is false initially
					pressTimer = window.setTimeout(function(){
					// your code here

					longpress = true; //if run hold function, longpress is true
					return;
					},1000)
				});

				$(element).on('mouseup', function () {
					clearTimeout(pressTimer); //clear time on mouseup
				});
				
				
				// Show images on page 
				$('img').unveil(200);

			}
		});

		FrontPage.Collection = Backbone.Collection.extend({
			url: function() {
				return App.APIO + '/i/general';
			},
			parse: function(response){
				return response.data;
			},
		});		
		return FrontPage;
	});