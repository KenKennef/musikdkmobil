define([

	'app',
	'backbone',
	'utils'
],

	function (App, Backbone, utils) {

		// Create a new module.
		var CompetitionList = App.module();

		// Create view
		CompetitionList.View = Backbone.View.extend({
			tagName: 'div',
			className: 'singleComp',
			template: 'competitionsList',
			initialize: function() {
				this.listenTo(this.collection, 'add', this.render, this);
				App.on('app:contests:push', this.pushContest, this);			
			},
			serialize: function() {
				return this.collection ? this.collection.toJSON() : [];
			},
			pushContest: function(obj){ 
				this.collection.push(obj); 
			},
			printCover: function(){
				if(this.collection.length) {
					var firstContest = this.collection.toJSON()[0],
						header = $('header.feat');
						itel = $('div#burnsbox');
					itel.empty();
					var latestImage = firstContest.artist.artist_standard;
					var artist_name = firstContest.artist.artist_name;
					if(latestImage){
						itel.append("<img data-src="+latestImage+" src='/musikdkmobil/assets/images/placeholders/square.png' alt="+artist_name+"/> ");
					} else {
						itel.append("<img data-src='/musikdkmobil/assets/images/default_cover.gif' src='/musikdkmobil/assets/images/placeholders/square.png' />");
					}
					//itel.append('<script src="../musikdkmobil/assets/js/unveil.js"></script>');
					//itel.append('<script>$(function(){$("img").unveil(600);});</script>');
					//utils.changeBackground(latestImage);
					// we add the title and the "go to" link to that contest
					header.append('<a href="konkurrencer/'+firstContest.contest_id+'"><nav class="goto"></nav></a><figcaption class="metacomp2"><h1>'+firstContest.title+'</h1></figcaption>');
				} else {
					$('.ti').append("<img src='/assets/images/fallbackheader.jpg' />");
					utils.changeBackground();
				}
			},
			afterRender: function() {
				var contest = this.collection.toJSON()[0];
				var contestImage = contest.artist.artist_cover;
				
				$(document).prop('title', 'Konkurrencer | MusikDK');
				utils.removeMetaTags();
				utils.setMetaTag('name', 'description', 'MusikDK | Vind fede præmier og se aktuelle konkurrencer');
				utils.setMetaTag('property', 'og:title', 'Konkurrencer på MusikDK');
				utils.setMetaTag('property', 'og:site_name', 'MusikDK');
				utils.setMetaTag('property', 'og:locale', 'da_DK');
				utils.setMetaTag('property', 'og:type', 'Website');
				utils.setMetaTag('property', 'og:image', contestImage);	
				utils.setMetaTag('property', 'og:url', 'https://musik.dk/konkurrencer/');
				utils.setMetaTag('property', 'og:description', 'Vind fede præmier og se aktuelle konkurrencer');
				
				this.printCover();

				var now = moment();
				moment.lang('da');

				$('time').each(function(i, e) {
					var time = moment($(e).attr('datetime'));
					if(now.diff(time, 'days') <= 1) {
						$(e).html(time.format('LLL'));
					}
				});
				
				$('img').unveil(600);
				
			}
		});

		CompetitionList.Collection = Backbone.Collection.extend({
			url: function() {
				return App.APIO + '/i/contests';
			},
			comparator: function(item) {
				return item.get('endDate');
			},
			parse: function(res){
				return res.data;
			},
		});

		return CompetitionList;
	});