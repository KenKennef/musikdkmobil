// Set the require.js configuration for your application.
require.config({	
noGlobal: true,
  // Initialize the application with the main application file.
  deps: ['main'],
  waitSeconds: 200,
  paths: {

    // Libraries.
    jquery       		: '//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery',
    underscore   	: '../lib/lodash-2.4.1.min',
    backbone     		: '../lib/backbone',
    layoutmanager	: '../lib/backbone.layoutmanager',
  	handlebars	 	: '../lib/handlebars/handlebars-v1.3.0',
  	blur					: '../lib/blur',
  	hello					: '../lib/hello.min',
  	authentication 	: '../app/tools/authentication',
    API					: '../app/tools/api',
    utils					: '../app/tools/utils',
  	datetimepicker	: '../lib/datetimepicker',
    moment 			: '../lib/danishMoment',
	countdown		: '../lib/countdown',
    imagesLoaded   : '../lib/imagesloaded.pkgd.min',
    queryparams     : '../lib/backbone.queryparams.min',
	unveil : '../lib/jquery.unveil',
	owlCarousel : '../lib/owl.carousel',
	swipeScript : '../lib/swipeScript',
	modal : '../lib/jquery.easyModal',
	socketio 			: 'https://devapi.musik.dk/socket.io/socket.io',
	lazySizes : '../lib/lazysizes'
  },

  shim: {
    // Backbone library depends on lodash and jQuery.
    backbone  : {
      deps   : ['jquery', 'underscore'],
      exports: 'Backbone'
    },
    handlebars: {
      exports: 'Handlebars'
    },
    async     : {
      exports: 'async'
    },
  	moment: ['jquery'],
  	locale: ['jquery'],
    datetimepicker: ['jquery'],
    countdown: ['jquery'],
    layoutmanager: ['backbone'],
    queryparams: ['backbone'],
    blur: {
      exports: 'blur'
    },
	unveil: {
        deps: ['jquery'],
        exports: 'unveil'
        },
    owlCarousel: {
        deps: ['jquery'],
        exports: 'owlCarousel'
    },
	swipeScript: {
        deps: ['jquery'],
        exports: 'swipeScript'
    },
	modal:['jquery'],
	lazySizes:['jquery']
}

});