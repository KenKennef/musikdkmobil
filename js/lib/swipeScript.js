window.swipeScript = (function() {
    var swipeScript = this;
  
  function opensLeft() {
    closesRight()
    $("#sl").addClass('visible')
    $("#swipe").addClass('isOpenLeft');
  }
  
  function closesLeft() {
    $("#sl").removeClass('visible')
    $("#swipe").removeClass('isOpenLeft');
  }

  function opensRight() {
    closesLeft()
    $("#sr").addClass('visible')
    $("#swipe").addClass('isOpenRight');
  }
  
  function closesRight() {
    $("#sr").removeClass('visible')
    $("#swipe").removeClass('isOpenRight');
  }

  
  $('#menubtn-left').on('click', function(e) {
    e.stopPropagation();
    e.preventDefault();
    
    if($('#swipe').hasClass('isOpenLeft')) {
      closesLeft();
    } else {
      opensLeft();
    }
    
  });

  $('#menubtn-right').on('click', function(e) {
    e.stopPropagation();
    e.preventDefault();
    
    if($('#swipe').hasClass('isOpenRight')) {
      closesRight();
    } else {
      opensRight();
    }
    
  });
  return swipeScript;
});