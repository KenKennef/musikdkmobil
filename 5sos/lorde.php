<!DOCTYPE html>
<html lang="da">
    <head>
        <title>Lorde | Musik.dk</title> 
        <meta charset="utf-8"/>
 <?php include 'includesheader.php'; ?>
  <?php include 'includesfooter.php'; ?>




<!-- RICH SNIPPETS  -->

<html itemscope itemtype="http://schema.org/Person">

<!-- Add the following three tags inside head. -->
<meta itemprop="name" content="bastille">
<meta itemprop="description" content="This is the description">
<meta itemprop="image" content="http://www.cphrecmedia.dk/musikdk/stage/demobilleder/detartistchannel.jpg">


<!-- FB OPEN GRAPH TAGS -->

<!-- Country. The content here, is the same all around the site, however set it up so we get it internationalized  -->
  <meta property="og:locale" content="da_DK" />

<!-- Full URL to the artist-header. This is also used for artistchannel subpages -->
  <meta property="og:image" content="http://cphrecmedia.dk/musikdk/stage/demobilleder/tv2featured.jpg"/>
  
  <meta property="og:title" content="bastille | Musik.dk"/>
 
<!--  Correct URL for this page. Inserted text is just dummy text -->
  <meta property="og:url" content="http://www.cphrecmedia.dk/musikdk/stage/artistchannel.php"/>
 
<!--  Same title as the page -->
  <meta property="og:site_name" content="bastille | Musik.dk"/>
 
<!--  Same description as the pagedescription -->
  <meta property="og:description" content="This is the artists description"/>
  
<!--   ARTISTS FB ID (not url, but we should have it anyway!). Inserted ID is just a dummy! -->
  <meta property="fb:profile_id" content="116311585057392"/>




<script>$(function(){$("img").unveil(900);});</script> 
       



</head>
    <body>

		
		
 	  <!-- Baggrund - Billedet er samme som bruges som header. Hvis der vises en video, bruges videothumbnailen, og vises der et cover, bruges dette som BG. -->
 	  <div class="bg" data-src="demobilleder/lorde.jpg" id="bgc"></div> 



<!--  Ydre og indre content-områder -->
 	  <section class="accw">
	 	<section class="cm">
		
		
		 	  <!-- HEADER - Artistens eget billede	 -->
		 	  <figure class="ti pr">
		 	  
     <div class="mhlogo"></div>

		 	  
		 	  <img data-src="demobilleder/lorde.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="ARTISTNAME">


<!-- 		  Only if logged in and no other image shown than the artistheader -->
			
			
		
<!-- 		 	  Hvis brugeren ikke er logget ind -->
				<!--
<div class="signupolwrap">
				
						<img src="demobilleder/logo.svg">
							<div class="signupol">
								<span>
									<h2>Følg dine yndlingsartister</h2>
									<p>På MusikDK kan du følge <span>dine yndlingsartister</span> og modtage <span> nyt</span> direkte i din mailbox. <span>Gå ikke glip</span> af de nyeste udgivelser og videoer.</p>
		
										<a href="#loginbox"> <button class="btnsup">Opret en profil <span>- eller log ind</span></button></a>
		
										</span>	
						
							</div>
				</div>

-->


		 	  <!-- Hvis artisten ikke længere opdateres (seneste album er ikke fra Universal), skal nedenstående kode/grafik aktiveres -->
<!--
		 	  			 	  <div class="anoupdates">
			 	  <span class="font-entypo icon-warning iconwarning" aria-hidden="true"><p>Denne artist opdateres</p><p> ikke længere<p></span>
			 	  </div>
			 	 
-->
			  </figure>	


<!-- 			  Hvis ny video indenfor 14 dage, vises den nyeste video, som header -->
<!--
<figure class="video-container">   
	  <iframe style="border:none" height="720" src="http://www.youtube.com/embed/b_SS3ZNmM6U?autohide=1&autoplay=0&iv_load_policy=3&modestbranding=1&vq=hd720" allowfullscreen></iframe>
</figure>  
-->





			 <!-- "pr" er position relative. Bruges til at holde højre-sidebar på plads under headerbilledets -->
			 <div class="pr">

		<!-- Venstre kolonne		 -->
		<section class="mcl mr350">
			
			
			<!-- SOCIAL UPDATES			 -->
			<header class="h"><span class="font-entypo icon-layout hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>Sociale opdateringer</h2></header>

  
   <div id="supmain">
                    <ul id="suptiles">
                        <!-- These are our grid blocks -->

                        <li>
                            <img src="https://scontent-a.xx.fbcdn.net/hphotos-prn2/t1.0-9/1508007_10152099557616799_218800160_n.png" alt="ARTISTNAME">
                            <section class="ip">
                                <h2>bastille</h2>

                                <p>DET har en version af "Jeg Har Ikke Lyst Til At Dø" med på udgivelsen "Beta Session Vol. 1", som I kan finde her:<br><br>

Spotify: http://goo.gl/8PbLJ0 <br>
TDC Play: http://goo.gl/mI1Kyd<br>
iTunes: http://goo.gl/ZUVokV <br>
WiMP: http://goo.gl/7PQkst <br>
Rdio: http://goo.gl/qVwF90 <br>
Deezer: http://goo.gl/qVwF90<br>
<br>
Husk at hele DET spiller hele Beta Sessionen for dig, når du vil, 24/7: http://www.youtube.com/watch?v=8RuqGx7HWgU</p>

                                <h3><time class="timeago" datetime="2013-12-03T09:24:17Z">December 03, 2013</time> | Facebook</h3>
                            </section>
                        </li>

                        <li>
                            <img src="https://scontent-b.xx.fbcdn.net/hphotos-frc3/t1.0-9/1959350_10152092578716799_1604870624_n.png" alt="ARTISTNAME">

                            <section class="ip">
                                <h2>bastille</h2>

                                <p>I morgen sender bastille en julehilsen, eksklusivt i Det Eneste Telegram. Er du endnu ikke tilmeldt, så skynd dig at gøre det nu!<a href=
                                "#">link</a></p>

                                <h3><time class="timeago" datetime="2013-12-02T09:24:17Z">December 02, 2013</time> | Facebook</h3>
                            </section>
                        </li>

                        <li>
                            <img src="https://fbexternal-a.akamaihd.net/safe_image.php?d=AQD1F5cQKUafT9Fw&w=377&h=197&url=http%3A%2F%2Fgeo-media.beatport.com%2Fimage_size%2F500x500%2F9172038.jpg&cfs=1" alt="ARTISTNAME">

                            <section class="ip">
                                <h2>bastille</h2>

                                <p>Instrumenterne er tørre igen, og DET er klar til Smukfest fredag...bestialsk klar!</p>

                                <h3><time class="timeago" datetime="2013-12-01T09:24:17Z">December 01, 2013</time> | Twitter</h3>
                            </section>
                        </li>

                        <li>
                            <img src="https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCYeC7OIyp3GH2N&url=https%3A%2F%2Ffbcdn-vthumb-a.akamaihd.net%2Fhvthumb-ak-ash3%2Ft15.0-10%2Fp280x280%2F1976378_10152085749021799_10152085738801799_34542_496_b.jpg&jq=100" alt="ARTISTNAME">

                            <section class="ip">
                                <h2>bastille</h2>

                                <p>bastille spiller på Smukfest på Bøgscenen næste fredag. Skal I se dem? Her kan I se en super flot stemningsvideo med bastille fra sidste års Nibe Festival (tidligere udsendt i Det Eneste Telegram)!</p>

                                <h3><time class="timeago" datetime="2013-11-11T09:24:17Z">November 11, 2013</time> | Facebook</h3>
                            </section>
                        </li>

                       
                                               
                        <li>
                        <img src="https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-prn1/t1.0-9/p403x403/1503938_10152074694721799_1198520703_n.png" alt="ARTISTNAME">

                            <section class="ip">
                                <h2>bastille</h2>

                                <p>DET Kører Rundt !</p>

                                <h3><time class="timeago" datetime="2013-11-01T09:24:17Z">November 01, 2013</time> | Instagram</h3>
                            </section>
                        </li>

                
      

<!-- End of grid blocks -->
                    </ul>
                
                    
                </div>
		

		
		
<div class="clear"></div>		

	
		
		


		<header class="h"><span class="font-entypo icon-video hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>Videoer</h2></header>

			<div class="ip2">

				<!-- SINGLE VIDEO -->	        
				<article data-mfp-src="https://www.youtube.com/watch?v=f2JuxM-snGc" class="c video pc33" id="1">
				<img data-src="https://i1.ytimg.com/vi/f2JuxM-snGc/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
				<div class="vidol"><span>AFSPIL</span></div>
				<div class="ellipsis"><h2 class="videoname">Team)</h2><span class="metamusikvideo"></span><br>
				<h2 class="van">Lorde</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 
				
				
				 <!-- SINGLE VIDEO -->               
				<article data-mfp-src="https://www.youtube.com/watch?v=ENP9PsMvQVs" class="c video pc33" id="2">
				<img data-src="https://i1.ytimg.com/vi/ENP9PsMvQVs/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
				<div class="vidol"><span>AFSPIL</span></div>
				<div class="ellipsis"><h2 class="videoname">Buzzcut Season (Letterman</h2><span class="metalive"></span><br>
				<h2 class="van">Lorde</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    
				
				<!-- SINGLE VIDEO -->	        
				<article data-mfp-src="https://www.youtube.com/watch?v=Y0dEuC_8YYQ" class="c video pc33" id="3">
				<img data-src="https://i1.ytimg.com/vi/Y0dEuC_8YYQ/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
				<div class="vidol"><span>AFSPIL</span></div>
				<div class="ellipsis"><h2 class="videoname">Royals (Letterman)</h2><span class="metalive"></span><br>
				<h2 class="van">Lorde</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 
				
				
				 <!-- SINGLE VIDEO -->               
				<article data-mfp-src="https://www.youtube.com/watch?v=lvudcekw2Z8" class="c video pc33" id="4">
				<img data-src="https://i1.ytimg.com/vi_webp/lvudcekw2Z8/mqdefault.webp" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
				<div class="vidol"><span>AFSPIL</span></div>
				<div class="ellipsis"><h2 class="videoname">ASK:REPLY 6</h2><span class="metabehind"></span><br>
				<h2 class="van">Lorde</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    
				
				
				 <!-- SINGLE VIDEO -->               
				<article data-mfp-src="https://www.youtube.com/watch?v=pstVCGyaUBM" class="c video pc33" id="5">
				<img data-src="https://i1.ytimg.com/vi/pstVCGyaUBM/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
				<div class="vidol"><span>AFSPIL</span></div>
				<div class="ellipsis"><h2 class="videoname">Buzzcut Season</h2><span class="metamusikvideo"></span><br>
				<h2 class="van">Lorde</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>   
				
				
				 <!-- SINGLE VIDEO -->               
				<article data-mfp-src="https://www.youtube.com/watch?v=nlcIKh6sBtc" class="c video pc33" id="6">
				<img data-src="https://i1.ytimg.com/vi/nlcIKh6sBtc/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
				<div class="vidol"><span>AFSPIL</span></div>
				<div class="ellipsis"><h2 class="videoname">Royals (US Version)</h2><span class="metamusikvideo"></span><br>
				<h2 class="van">Lorde</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 	   

</div>




<div class="clear"></div>




<header class="h"><span class="font-entypo icon-twitter hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>#lorde</h2></header>
<div class="ip">

<a class="twitter-timeline" href="https://twitter.com/search?q=%23lorde" data-widget-id="453153701398466562">Tweets om "#lorde"</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>





<style>


</style>

</div>






<div class="clear"></div>



		</section>
         
         
         
        
                
    
        
<section class="cr width350px">
  











<header class="h"><span class="font-entypo icon-play hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>Afspil på:</h2></header>

<!-- MUSICSERVICE LINKS -->

<div class="ip2">



<a href="http://open.spotify.com/artist/1vCWHaC5f2uS3yhpwWbIA6" target="_blank">
<article class="c ls">
<span class="font-entypo icon-c-spotify slogo spotifylogo"></span><h4>Spotify</h4>
</article></a>


<a href="https://itunes.apple.com/dk/artist/lorde/id602767352?l=da">
<article class="c ls">
<img class="slogo" data-src="css/icons/itunes.svg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="iTunes logo">
<h4>iTunes</h4>
</article></a>


<a href="http://play.tdc.dk/#!/play/artist/22714178">
<article class="c ls">
<object class="slogo" data="css/icons/tdcplay.png" type="image/png"></object>
<h4>TDC Play</h4>
</article></a>

<a href="http://wimp.dk/artist/4931831">
<article class="c ls">
<object class="slogo" data="css/icons/WiMP.svg" type="image/svg+xml"></object>
<h4>WiMP</h4>
</article></a>

<a href="http://www.deezer.com/artist/4448485">
<article class="c ls">
<object class="slogo" data="css/icons/deezer.png" type="image/png"></object>
<h4>Deezer</h4>
</article></a>

<a href="http://www.rdio.com/artist/Lorde/">
<article class="c ls">
<span class="font-entypo icon-rdio slogo rdiologo"></span><h4>Rdio</h4>
</article></a>


</div>




<header class="h"><span class="font-entypo icon-c-spotify hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>Lyt på Spotify</h2></header>

<iframe style="margin-left:-4px" src="https://embed.spotify.com/?uri=spotify:album:5NrFMOprmnMEf4gMnLaHcq&theme=white" width="358px" height="432" frameborder="0" allowtransparency="true"></iframe>





<header style="margin-top:-5px" class="h"><span class="font-entypo icon-star-empty hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>Tilmeld på andre platforme</h2></header>
<div class="ip">

<iframe src="https://embed.spotify.com/follow/1/?uri=spotify:artist:163tK9Wjr9P9DmM0AVK7lm&size=basic&theme=light" width="200" height="25" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>

<br><br>
<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Flordemusic&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21&amp;appId=160025487521480" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>

<br><br>
<a href="https://twitter.com/lordemusic" class="twitter-follow-button" data-show-count="false" data-lang="da" data-size="large">Følg @lordemusic</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>


</div>






<header class="h"><span class="font-entypo icon-book hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>Biografi</h2>
<span data-mfp-src="#addbio.php" id="addbio2" class="font-entypo icon-cog hiconright" aria-hidden="true"></span></header>
<section class="ip">
<p>Ella Maria Lani Yelich-O'Connor er kendt under kunstnernavnet Lorde, er en New Zealandsk sangerinde og sangskriver, født og opvokset i Devonport, Auckland.</p>
<br><p class="infotext floatright">Tekst fra Wikipedia</p>






<!-- IF soloartist -->
<br>
<h2 id="acmembers">Fødselsdag</h2>
<time datetime="1996-07-11"><p>7. november 1996 (17 år)<p></time>
<!-- Date above and years, should be calculated -->


</section>







<header class="h"><span class="font-entypo icon-export hicon" aria-hidden="true"></span>&nbsp;&nbsp;Links</header>

<section class="ip4">

<table style="width: 100%">
    <tr class="alink">
       <td class="fblink roundedcorners"></td>
		<td class="twlink roundedcorners"></td>
       <td class="iglink roundedcorners"></td>
       <td class="sclink roundedcorners"></td>
    </tr>	
</table>
</section>

	
	
			




</section>  

<!-- ADDITIONAL CONTENT -->

	</section>    


</div>    
<!--   SECTION FOR DARK BACKGROUND   -->
	</section> 
<!--   SECTION FOR CMEXTRA -->
     </section>
     
     <div class="clear"></div>		

    
    
          <script>
	 $(".fblink").click(function(){
    window.open('https://www.facebook.com/lordemusic');
});
 </script>  
 
 
 
  <script>
	 $(".twlink").click(function(){
    window.open('http://www.twitter.com/lordemusic');
});
 </script> 
 
 
  <script>
	 $(".iglink").click(function(){
    window.open('http://instagram.com/lordemusic');
});
 </script> 
 
  <script>
	 $(".sclink").click(function(){
    window.open('http://www.soundcloud.com/lordemusic');
});
 </script>  
    
    <script>
	    $(document).ready(function() {
  $('#1').magnificPopup({type:'iframe',mainClass: 'mfp-fade'});
});
    </script>
        <script>
	    $(document).ready(function() {
  $('#2').magnificPopup({type:'iframe',mainClass: 'mfp-fade'});
});
    </script>
        <script>
	    $(document).ready(function() {
  $('#3').magnificPopup({type:'iframe',mainClass: 'mfp-fade'});
});
    </script>
    
        <script>
	    $(document).ready(function() {
  $('#4').magnificPopup({type:'iframe',mainClass: 'mfp-fade'});
});
    </script>
    
        <script>
	    $(document).ready(function() {
  $('#5').magnificPopup({type:'iframe',mainClass: 'mfp-fade'});
});
    </script>
    
        <script>
	    $(document).ready(function() {
  $('#6').magnificPopup({type:'iframe',mainClass: 'mfp-fade'});
});
    </script>
    
    
    
    
<!-- EXTRA ls BUTTON   -->
<script>$(document).ready(function(){$("#xservice80").click(function(e){$(".extrals").toggle();e.stopPropagation();$('html').click(function(){$('.extrals').hide();})});});</script>
   



<!-- Wookmark integration -->
<script src="js/wookmark.min.js"></script>
<script type="text/javascript">
	(function($) {
		$(function() {
			// Prepare wookmark objects and options.
			var $tiles = $('#suptiles'),
					$handler = $('li', $tiles),
					options = {
						autoResize: true,
						container: $tiles,
						offset: 10,
						outerOffset: 15,
						fillEmptySpace: true,
						itemWidth: 280,
						flexibleWidth: 500
					};

			$tiles.imagesLoaded(function() {
				$handler.wookmark(options);
			});
		});
	})(jQuery);
</script>




<!-- WHEN USING CANVAS BLUR -->
<?php include 'blurbgscript.php'; ?>
<!-- WHEN USING CANVAS BLUR -->


    </body>
    
    

</html>