<script type='text/javascript'>window.onload=function(){
 
 
        //create image object
    var imageObj = new Image();
 
        //when image is finished loading
    imageObj.onload = function() {
                //get size
                var w = imageObj.naturalWidth;
                var h = imageObj.naturalHeight;
 
                var canvas = document.createElement("canvas");
                canvas.width = w;
                canvas.height = h;
                //create virtual canvas
                var ctx = canvas.getContext('2d');
 
                //draw the image on it
                ctx.drawImage(imageObj, 0, 0);
               
                //apply the blur
                stackBoxBlurCanvasRGB(ctx, 0, 0, w, h, 11, 2);
               
                //add grey filter
                ctx.fillStyle='rgba(64,64,64,0.4)';
                ctx.fillRect(0,0,w,h);
               	
                //and display it in 1 second using a fade
                var $canvas = $(canvas) 
                
                $("#bgc").append(canvas);
               	$canvas.show();
                var canvasRatio = $canvas.width()/$canvas.height();
                var windowRatio = $(window).width()/$(window).height();
                if (canvasRatio > windowRatio){
                    $canvas.css({
                        "height": "100%",
                        "width" : "auto"
                    });
                } else {
                    $canvas.css({
                        "width": "100%",
                        "height": "auto"
                    });
                }
               	$canvas.css({
                	"marginLeft" : -$canvas.width()/2+"px",
                	"marginTop" : -$canvas.height()/2+"px"
                });
                window.onresize = function(){
                    var canvasRatio = $canvas.width()/$canvas.height();
                    var windowRatio = $(window).width()/$(window).height();
                    if (canvasRatio > windowRatio){
                        $canvas.css({
                            "height": "100%",
                            "width" : "auto"
                        });
                    } else {
                        $canvas.css({
                            "width": "100%",
                            "height": "auto"
                        });
                    }
                	$canvas.css({
                	 	"marginLeft" : -$canvas.width()/2+"px",
                	 	"marginTop" : -$canvas.height()/2+"px"
                	 });
                }
               
    };
       
        //set the source of the image from the data source
    imageObj.src = $('#bgc').data("src");
 
}
</script>