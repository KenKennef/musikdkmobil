<!DOCTYPE html>
<html lang="da">
    <head>
        <title>Videoer - Musik.dk</title> 
        <meta charset="utf-8"/>
        <?php include 'includesheader.php'; ?>
       <script>$(function(){$("img").unveil(900);});</script> 

 <style>.currentvideo{background:#ED1941}</style>


</head>
    <body>
<nav class="nav bvideo">
<?php include 'navmenu.php'; ?>
</nav>


<!-- BACKGROUND -->
<div class="bg" data-src="demobilleder/betasessionvideothumb.jpg" id="bgc"></div>   



<!-- IF ACCEPT COOKIE POPUP -->
<?php include 'acceptcookie.php'; ?>

<!-- IF SITEWIDE ERROR POPUP -->
<!-- <?php include 'sitewideerrormsg.php'; ?> -->



<section class="cw">
<section class="cm">

		
		
		
				<section class="cfl ml200">





				</section>

		
		
		<section class="cfl ml200">
		
			<header class="h floatleft" style="border-bottom:none"> <span class="font-entypo icon-video hicon redtext" aria-hidden="true"></span>&nbsp;&nbsp;Nyeste Musik.dk produktioner</header>
		<div class="tabs" style="border-top:none">
			<ul  class="videotabs" style="border-top:none">
				<li><a href="#1">Alle</a></li>
				<li><a href="#2">Session</a></li>
				<li><a href="#3">Singlen</a></li>
				<li><a href="#3">Talk</a></li>
			</ul>
			<div class="#1">
		<div class="ip">


<article class="c video pc33">
<div data-mfp-src="#videopopup" class="popup">
<img data-src="demobilleder/betasessionvideothumb.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE"></div>
<!-- ADMIN FEATURE START -->
<div class="a4d34">
<span class="hint--top" data-hint="Ændr stamdata">
<span class="font-entypo icon-cog admincicon" aria-hidden="true" data-mfp-src="#ccvideo2" id="ccvideo"></span></span>
<span class="hint--top" data-hint="Send video til følgere">
<span class="font-entypo icon-mail admincicon floatright" aria-hidden="true" data-mfp-src="#videomaillb2" id="videomaillb"></span></span>
</div>
<!-- ADMIN FEATURE END -->
<div data-mfp-src="#videopopup" class="popup">



<div class="vidol">
				<span>
					AFSPIL
				</span>
		</div>

</a>
<figure class="trianglebrred"><span class="trianglep">1</span></figure>
<div class="ellipsis"><h2 class="videoname">SHOW ME WHAT LOVE IS</h2><span class="metalive"></span><br>
<h2 class="van">Alphabeat</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></div></article>   






<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<div data-mfp-src="#videopopup" class="popup">
<img data-src="demobilleder/lukasvideothumb.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<figure class="trianglebrred"><span class="trianglep">2</span></figure>
<div class="ellipsis"><h2 class="videoname">Better Than Yourself (Beta Singlen)</h2><span class="metatalk"></span><br>
<h2 class="van">Lukas Graham</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </div></article> 

 

<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/KlV-xGTV1P0/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<figure class="trianglebrred"><span class="trianglep">3</span></figure>
<div class="ellipsis"><h2 class="videoname">Hjertestarter</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Nephew</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/_PIyFDIYuzg/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<figure class="trianglebrred"><span class="trianglep">4</span></figure>
<div class="ellipsis"><h2 class="videoname">Din For Evigt</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Burhan G</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    

<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/yd8jh9QYfEs/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<figure class="trianglebrred"><span class="trianglep">5</span></figure>
<div class="ellipsis"><h2 class="videoname">Dont Stop The Music</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Rihanna</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/3F2Jc6qQTCA/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<figure class="trianglebrred"><span class="trianglep">6</span></figure>
<div class="ellipsis"><h2 class="videoname">Hate That I Love You</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Rahbek</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 	   



<button class="btnwhite btnmore" type="button">Vis flere videoer</button>
</div>			</div>
			<div class="#2">
<div class="ip" style="padding-bottom:0">




2

</div>
			</div>
			<div class="#3">
				<div class="ip" style="padding-bottom:0">


3

	</div>
			</div>
		</div>
		
		
		
		<div class="clear"></div>

	
		
		



		<header class="h floatleft">  <span class="font-entypo icon-video hicon redtext" aria-hidden="true"></span>&nbsp;&nbsp;Nye musikvideoer</header>

		<div class="tabs">
			<ul class="videotabs">
				<li><a href="#1">Alle</a></li>
				<li><a href="#2">Danske</a></li>
				<li><a href="#3">Internationale</a></li>
			</ul>
			<div class="#1">
			<section class="ip">


<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/BRFo7d8FXPY/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Better Than Yourself</h2><span class="metatalk"></span><br>
<h2 class="van">Lukas Graham</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/vmlBw-hzfwc/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">SHOW ME WHAT LOVE IS</h2><span class="metalive"></span><br>
<h2 class="van">Alphabeat</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    

<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/KlV-xGTV1P0/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Hjertestarter</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Nephew</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/_PIyFDIYuzg/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Din For Evigt</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Burhan G</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    

<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/yd8jh9QYfEs/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Dont Stop The Music</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Rihanna</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/3F2Jc6qQTCA/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Hate That I Love You</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Rahbek</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 	   



<button class="btnwhite btnmore" type="button">Vis flere videoer</button>
</section>









			</div>
			<div class="#2">
				<section class="ip">
 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/_PIyFDIYuzg/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Din For Evigt</h2><br>
<h2 class="van">Burhan G</h2><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 
<!-- SINGLE VIDEO -->	        
<article class="c video pc33 ">
<img data-src="http://i3.ytimg.com/vi/BRFo7d8FXPY/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Better Than Yourself (Beta Singlen)</h2><br>
<h2 class="van">Lukas Graham</h2><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/vmlBw-hzfwc/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">SHOW ME WHAT LOVE IS (BETA SESSION)</h2><br>
<h2 class="van">Alphabeat</h2><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    



</section>




			</div>
			<div class="#3">
				<section class="ip">

 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/3F2Jc6qQTCA/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Hate That I Love You</h2><br>
<h2 class="van">Rahbek</h2><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 

<!-- SINGLE VIDEO -->	        
<article class="c video pc33 ">
<img data-src="http://i3.ytimg.com/vi/BRFo7d8FXPY/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Better Than Yourself (Beta Singlen)</h2><br>
<h2 class="van">Lukas Graham</h2><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/vmlBw-hzfwc/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">SHOW ME WHAT LOVE IS (BETA SESSION)</h2><br>
<h2 class="van">Alphabeat</h2><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    



				</section>



			</div>
		</div>


<div class="clear"></div>

<div class="biggeradwrap">
<figure class="ti">
<img data-src="demobilleder/madonnaad.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="ARTISTNAME">
<figcaption class="meta" style="border-right:none"><h2><span class="redtext">Nyt fra Madonna!</span> &nbsp;Lyt før alle andre her på siden fra d. 11. august </h2></figcaption>
</figure></div>


<header class="h floatleft"><span class="font-entypo icon-video hicon redtext" aria-hidden="true"></span>&nbsp;&nbsp;Anbefalet til dig</header>

		<div class="tabs">
			<ul class="videotabs">
				<li><a href="#1">Alle</a></li>
				<li><a href="#2">Musikvideoer</a></li>
				<li><a href="#3">Session</a></li>
				<li><a href="#4">Talk</a></li>
				<li><a href="#4">Behind</a></li>
			</ul>
			<div class="#1">
			<section class="ip">




<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/BRFo7d8FXPY/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Better Than Yourself</h2><span class="metatalk"></span><br>
<h2 class="van">Lukas Graham</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/vmlBw-hzfwc/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">SHOW ME WHAT LOVE IS</h2><span class="metalive"></span><br>
<h2 class="van">Alphabeat</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    

<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/KlV-xGTV1P0/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Hjertestarter</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Nephew</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/_PIyFDIYuzg/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Din For Evigt</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Burhan G</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    

<!-- SINGLE VIDEO -->	        
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/yd8jh9QYfEs/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Dont Stop The Music</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Rihanna</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc33">
<img data-src="http://i3.ytimg.com/vi/3F2Jc6qQTCA/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Hate That I Love You</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Rahbek</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 	   



<button class="btnwhite btnmore" type="button">Vis flere videoer</button>
</section>



			</div>
			<div class="#2">
				<section class="ip">

<!-- SINGLE ALBUM -->	        
<article class="c news pc33 ">
<img data-src="http://cdn3.rd.io/album/7/5/9/00000000001d6957/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Shaka Loveless</h2>
<h3 class="van">Shaka Loveless</h3><span class="listicon" data-icon="O" aria-hidden="true"></span> </article> 

<!-- SINGLE ALBUM -->	        
<article class="c news pc33">
<img data-src="http://cdn3.rd.io/album/d/9/6/000000000014869d/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Before The Dinosaurs</h2>
<h3 class="van">Aura Dione</h3><span class="listicon" data-icon="O" aria-hidden="true"></span> </article>
 
 <!-- SINGLE ALBUM -->               
<article class="c news pc33">
<img data-src="http://cdn3.rd.io/album/4/f/4/00000000001534f4/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Continued Silence EP</h2>
<h3 class="van">Imagine Dragons</h3><span class="listicon" data-icon="O" aria-hidden="true"></span></article>    

<!-- SINGLE ALBUM -->	        
<article class="c news pc33">
<img data-src="http://cdn3.rd.io/album/8/a/9/00000000002859a8/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Hjertestarter (Bonus Edition)</h2>
<h3 class="van">Nephew</h3><span class="listicon" data-icon="O" aria-hidden="true"></span> </article>   

<!-- SINGLE ALBUM -->	        
<article class="c news pc33 ">
<img data-src="http://cdn3.rd.io/album/3/6/d/00000000001e2d63/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Oppefra Og Ned</h2>
<h3 class="van">Søren Huss</h3><span class="listicon" data-icon="O" aria-hidden="true"></span> </article> 


 <!-- SINGLE ALBUM -->               
<article class="c news pc33">
<img data-src="http://cdn3.rd.io/album/5/8/d/00000000001ffd85/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Lukas Graham (International Version)</h2>
<h3 class="van">Lukas Graham</h3><span class="listicon" data-icon="O" aria-hidden="true"></span></article>  

<!-- SINGLE ALBUM -->	        
<article class="c news pc33 ">
<img data-src="http://cdn3.rd.io/album/7/5/9/00000000001d6957/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Shaka Loveless</h2>
<h3 class="van">Shaka Loveless</h3><span class="listicon" data-icon="O" aria-hidden="true"></span> </article> 

<!-- SINGLE ALBUM -->	        
<article class="c news pc33">
<img data-src="http://cdn3.rd.io/album/d/9/6/000000000014869d/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Before The Dinosaurs</h2>
<h3 class="van">Aura Dione</h3><span class="listicon" data-icon="O" aria-hidden="true"></span> </article>
 
 <!-- SINGLE ALBUM -->               
<article class="c news pc33">
<img data-src="http://cdn3.rd.io/album/4/f/4/00000000001534f4/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Continued Silence EP</h2>
<h3 class="van">Imagine Dragons</h3><span class="listicon" data-icon="O" aria-hidden="true"></span></article>    

<!-- SINGLE ALBUM -->	        
<article class="c news pc33">
<img data-src="http://cdn3.rd.io/album/8/a/9/00000000002859a8/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Hjertestarter (Bonus Edition)</h2>
<h3 class="van">Nephew</h3><span class="listicon" data-icon="O" aria-hidden="true"></span> </article>   

<!-- SINGLE ALBUM -->	        
<article class="c news pc33 ">
<img data-src="http://cdn3.rd.io/album/3/6/d/00000000001e2d63/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Oppefra Og Ned</h2>
<h3 class="van">Søren Huss</h3><span class="listicon" data-icon="O" aria-hidden="true"></span> </article> 


 <!-- SINGLE ALBUM -->               
<article class="c news pc33">
<img data-src="http://cdn3.rd.io/album/5/8/d/00000000001ffd85/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE" width="85">
<h2 class="videoname">Lukas Graham (International Version)</h2>
<h3 class="van">Lukas Graham</h3><span class="listicon" data-icon="O" aria-hidden="true"></span></article>    

<button class="btnwhite" type="button" style="width:66.1%">Vis flere albums ?</button>
<button class="btnblue" type="button" style="width:32.6%;float:right;margin-right:1px"><span style="margin-left:15px">Gå til udgivelse</span><span class="buttonicon" data-icon="V" aria-hidden="true"></span></button>

				</section>




			</div>
			<div class="#3">
				<section class="ip">

 <!-- SINGLE ALBUM -->               
<article class="c album pc33">
<img data-src="http://cdn3.rd.io/album/4/f/4/00000000001534f4/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Continued Silence EP</h2><br>
<h2 class="van">Imagine Dragons</h2><span class="cicon" data-icon="O" aria-hidden="true"></span></article>    

<!-- SINGLE ALBUM -->	        
<article class="c album pc33 ">
<img data-src="http://cdn3.rd.io/album/7/5/9/00000000001d6957/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Shaka Loveless</h2><br>
<h2 class="van">Shaka Loveless</h2><span class="cicon" data-icon="O" aria-hidden="true"></span> </article> 


<!-- SINGLE ALBUM -->	        
<article class="c album pc33">
<img data-src="http://cdn3.rd.io/album/d/9/6/000000000014869d/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Before The Dinosaurs</h2><br>
<h2 class="van">Aura Dione</h2><span class="cicon" data-icon="O" aria-hidden="true"></span> </article> 

<!-- SINGLE ALBUM -->	        
<article class="c album pc33 ">
<img data-src="http://cdn3.rd.io/album/3/6/d/00000000001e2d63/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Oppefra Og Ned</h2><br>
<h2 class="van">Søren Huss</h2><span class="cicon" data-icon="O" aria-hidden="true"></span> </article> 


<!-- SINGLE ALBUM -->	        
<article class="c album pc33">
<img data-src="http://cdn3.rd.io/album/8/a/9/00000000002859a8/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Hjertestarter (Bonus Edition)</h2><br>
<h2 class="van">Nephew</h2><span class="cicon" data-icon="O" aria-hidden="true"></span> </article>   

 <!-- SINGLE ALBUM -->               
<article class="c album pc33">
<img data-src="http://cdn3.rd.io/album/5/8/d/00000000001ffd85/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Lukas Graham (International Version)</h2><br>
<h2 class="van">Lukas Graham</h2><span class="cicon" data-icon="O" aria-hidden="true"></span></article> 
			
			<button class="btnwhite" type="button">Vis flere albums ?</button>
				</section>

			</div>
			
						<div class="#4">
				<section class="ip">

 <!-- SINGLE ALBUM -->               
<article class="c album pc33">
<img data-src="http://cdn3.rd.io/album/4/f/4/00000000001534f4/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Continued Silence EP</h2><br>
<h2 class="van">Imagine Dragons</h2><span class="cicon" data-icon="O" aria-hidden="true"></span></article> 

<!-- SINGLE ALBUM -->	        
<article class="c album pc33 ">
<img data-src="http://cdn3.rd.io/album/7/5/9/00000000001d6957/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Shaka Loveless</h2><br>
<h2 class="van">Shaka Loveless</h2><span class="cicon" data-icon="O" aria-hidden="true"></span> </article> 
   

<!-- SINGLE ALBUM -->	        
<article class="c album pc33">
<img data-src="http://cdn3.rd.io/album/d/9/6/000000000014869d/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Before The Dinosaurs</h2><br>
<h2 class="van">Aura Dione</h2><span class="cicon" data-icon="O" aria-hidden="true"></span> </article> 

<!-- SINGLE ALBUM -->	        
<article class="c album pc33">
<img data-src="http://cdn3.rd.io/album/8/a/9/00000000002859a8/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Hjertestarter (Bonus Edition)</h2><br>
<h2 class="van">Nephew</h2><span class="cicon" data-icon="O" aria-hidden="true"></span> </article> 

<!-- SINGLE ALBUM -->	        
<article class="c album pc33 ">
<img data-src="http://cdn3.rd.io/album/3/6/d/00000000001e2d63/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Oppefra Og Ned</h2><br>
<h2 class="van">Søren Huss</h2><span class="cicon" data-icon="O" aria-hidden="true"></span> </article> 


 <!-- SINGLE ALBUM -->               
<article class="c album pc33">
<img data-src="http://cdn3.rd.io/album/5/8/d/00000000001ffd85/1/square-600.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<h2 class="videoname">Lukas Graham (International Version)</h2><br>
<h2 class="van">Lukas Graham</h2><span class="cicon" data-icon="O" aria-hidden="true"></span></article>    



<button class="btnwhite" type="button" style="width:66.1%">Vis flere albums ?</button>
<button class="btnblue" type="button" style="width:32.6%;float:right;margin-right:1px"><span style="margin-left:15px">Gå til udgivelse</span><span class="buttonicon" data-icon="V" aria-hidden="true"></span></button>

 


				</section>

			</div>
		</div>



	
	
	
	
	
	
	
	
	
	
	
		</section>
                
    
        
<section class="cl width200px red">




<a href="video.php">
<figure class="mlogosb"></figure> 
</a>        
               
        <nav class="hmenu darkred roundedrighttop-left"><span class="menulisticon2" data-icon="U" aria-hidden="true"></span>&nbsp;&nbsp;Oversigt</nav>
		

<ul class="sbm">
	<li id="selected"><a href="#">Forside</a> </li>
	<li><a href="#">Top 100 </a></li>
	<li><a href="#">On Demand</a> </li>
	<li><a href="#">Musikvideoer </a></li>
	<li><a href="#">Sessions / Live </a></li>
	<li><a href="#">Talks</a> </li>
	<li><a href="#">Behinds </a></li>
	<li><a href="#">De sjove</a> </li>
</ul>


                
                
<nav class="hmenu darkred"><span class="font-entypo icon-numbered-list menulistfavhicon" aria-hidden="true"></span>Alle videoer</nav>
<ul class="sbm2">
	<li><a href="#">Pop/Rock </a></li>
	<li><a href="#">RnB/Soul </a></li>
	<li><a href="#">Alternative </a></li>
	<li><a href="#">Dance/Electro </a></li>
	<li><a href="#">HipHop </a></li>
	<li><a href="#">Jazz & Blues </a></li>
	<li><a href="#">World </a></li>
	<li><a href="#">Klassisk </a></li>
</ul>


                
<nav class="hmenu darkred"><span class="font-entypo icon-sweden menulistfavhicon" aria-hidden="true"></span>Danske</nav>
<ul class="sbm2">
	<li><a href="#">Pop/Rock</a> </li>
	<li><a href="#">RnB/Soul</a> </li>
	<li><a href="#">Alternative </a></li>
	<li><a href="#">Dance/Electro </a></li>
	<li><a href="#">HipHop </a></li>
	<li><a href="#">Jazz & Blues</a> </li>
	<li><a href="#">World</a> </li>
	<li><a href="#">Klassisk </a></li>
</ul>


        	
       
<nav class="hmenu darkred"><span class="font-entypo icon-globe menulistfavhicon" aria-hidden="true"></span>Internationale</nav>       
<ul class="sbm2">
	<li><a href="#">Pop/Rock</a> </li>
	<li><a href="#">RnB/Soul</a> </li>
	<li><a href="#">Alternative</a> </li>
	<li><a href="#">Dance/Electro </a></li>
	<li><a href="#">HipHop </a></li>
	<li><a href="#">Jazz & Blues</a> </li>
	<li><a href="#">World </a></li>
	<li><a href="#">Klassisk </a></li>
</ul> 


        	
       



                      
</section> 


   
</section>
</section>  

        <?php include 'includesfooter.php'; ?>

<!-- VIDEO POPUPBOX -->
<?php include 'videopopup.php'; ?>

   
     <footer>
<?php include 'footer.php'; ?></nav>
     </footer>
    


<!-- WHEN USING TABS JS -->
<script>$('.tabs').liteTabs();</script>	
<!-- WHEN USING TABS JS -->

<!-- WHEN USING CANVAS BLUR -->
<?php include 'blurbgscript.php'; ?>
<!-- WHEN USING CANVAS BLUR -->

<?php include 'changecontentdata-video.php'; ?>


<?php include 'videomaillightbox.php'; ?>

</body>
</html>