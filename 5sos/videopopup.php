
<!-- VIDEO FORM  -->
 
     <div id="videopopup" class="white-popup mfp-hide">

<section class="ppw">

<figure class="video-container">   
	  <iframe style="border:none" height="720" src="http://www.youtube.com/embed/jdejHEjDLZA?autohide=1&autoplay=0&iv_load_policy=3&modestbranding=1&vq=hd720" allowfullscreen></iframe>
</figure>  


<div class="ip">


<!-- SHOW ONLY IF NOT CATEGORIZED -->
<!--
<p class="whred">Denne video er endnu ikke sorteret. Vil du hjælpe os med at ligge den i den rigtige kategori?
<select id="vidslct">
  <option selected="true">Vælg kategori</option>
  <option value="album">Musikvideo</option>
  <option value="single">Session/Live</option>
   <option value="behind">Behind</option>
  <option value="ep">Talk</option>
</select>
</p>
-->




<!-- VIDEODESCRIPTION -->
<!-- 	Contenteditable only possible for admins! -->
<p contenteditable="true">Session fra Lukas Graham</p>

	

</div>







<div class="red" style="height:50px">
	
	<ul class="vswrap">
        
        
		 <div id="sharevid">
					<div id="shareme" data-url="http://musik.dk/" data-text="COMPLETE VIDEO TITLE"></div>
				</div>
				
		<li class="vviews">Visninger: <span>105.134</span></li>
	</ul>

	
</div>











<header class="h"><span class="font-entypo icon-video hicon redtext" aria-hidden="true"></span>&nbsp;&nbsp;Andre med Lukas Graham</header>

<section class="ip">


<!-- SINGLE VIDEO -->	        
<article class="c video pc25">
<img data-src="http://i3.ytimg.com/vi/KlV-xGTV1P0/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Hjertestarter</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Nephew</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc25">
<img data-src="http://i3.ytimg.com/vi/_PIyFDIYuzg/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Din For Evigt</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Burhan G</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article>    

<!-- SINGLE VIDEO -->	        
<article class="c video pc25">
<img data-src="http://i3.ytimg.com/vi/yd8jh9QYfEs/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Dont Stop The Music</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Rihanna</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 


 <!-- SINGLE VIDEO -->               
<article class="c video pc25">
<img data-src="http://i3.ytimg.com/vi/3F2Jc6qQTCA/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="videoname">Hate That I Love You</h2><span class="metamusikvideo"></span><br>
<h2 class="van">Rahbek</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 

<button class="btnwhite btnmore" type="button">Vis flere videoer fra Lukas Graham</button>


</section>



<div class="pr">

<section class="mclvideo mr340">


<header class="h" style="border-top:1px solid #ddd"><span class="font-entypo icon-comment hicon" aria-hidden="true"></span>&nbsp;&nbsp;Kommentarer</header>

<div class="ip">
	
	<!-- START: Livefyre Embed -->
					<div style="height:800px" id="livefyre-comments"></div>
						<script type="text/javascript" src="http://zor.livefyre.com/wjs/v3.0/javascripts/livefyre.js"></script>
						<script type="text/javascript">
						(function () {
						    var articleId = fyre.conv.load.makeArticleId(null);
						    fyre.conv.load({}, [{
						        el: 'livefyre-comments',
						        network: "livefyre.com",
						        siteId: "352308",
						        articleId: articleId,
						        signed: false,
						        collectionMeta: {
						            articleId: articleId,
						            url: fyre.conv.load.makeCollectionUrl(),
						        }
						    }], function() {});
						}());
						</script>
					<!-- END: Livefyre Embed -->

</div>	
	
	
	
	
	
		</section>



  <aside>
  <section class="cr width340px">
  
 <header class="h" style="border-top:1px solid #ddd"><span class="font-entypo icon-star hicon" aria-hidden="true"></span>&nbsp;&nbsp;Besøg artistkanalen</header>
<div class="ip2">

	<article class="carrow pr">
	<img data-src="demobilleder/lukasgrahamsquare.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  width="78" alt="NEWS TITLE - ARTISTNAME">
	<div class="ellipsis-ca">
	<h3 class="ra2">Lukas Graham</h3></div>
	<h2 class="fcn"> 13.457</span></span></h2>
	</article>

</div>
                
                
<header class="hblack"><span class="font-entypo icon-plus hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>Anbefalede videoer</h2></header>



<div class="lightblack">


 <section class="ip2">
                
<article class="adc video">
<img data-src="http://i3.ytimg.com/vi/3F2Jc6qQTCA/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="ra">Hate That I Love You</h2><span class="admetamusikvideo"></span><br>
<h2 class="artistextra">Rahbek</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 

<article class="adc video">
<img data-src="http://i3.ytimg.com/vi/vmlBw-hzfwc/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="ra">SHOW ME WHAT LOVE IS</h2><span class="admetalive"></span><br>
<h2 class="artistextra">Alphabeat</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span></article> 

<article class="adc video ">
<img data-src="http://i3.ytimg.com/vi/BRFo7d8FXPY/mqdefault.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="VIDEOTITLE">
<div class="vidol"><span>AFSPIL</span></div>
<div class="ellipsis"><h2 class="ra">Better Than Yourself</h2><span class="admetalive"></span><br>
<h2 class="artistextra">Lukas Graham</h2></div><span class="font-entypo icon-play cicon" aria-hidden="true"></span> </article> 
 </section>





<header class="hblack"><span class="font-entypo icon-plus hicon" aria-hidden="true"></span>&nbsp;&nbsp;<h2>Anbefalede artister</h2></header>

<div class="lightblack">
<section class="ip">


<article class="adcarrow pr">
<img data-src="demobilleder/nephewnews.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  width="78" alt="NEWS TITLE - ARTISTNAME">
<div class="ellipsis-ca">
<h3 class="ra">Nephew</h3></div>
<h2 class="fcnl"> 12443</span></span></h2>
</article>

<article class="adcarrow pr">
<img data-src="demobilleder/magtenskorridorer.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  width="78" alt="NEWS TITLE - ARTISTNAME">
<div class="ellipsis-ca">
<h3 class="ra">Magtens Korridorer</h3></div>
<h2 class="fcnl"> 6437</span></span></h2>
</article>

<article class="adcarrow pr">
<img data-src="demobilleder/sorenhuss.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  width="78" alt="NEWS TITLE - ARTISTNAME">
<div class="ellipsis-ca">
<h3 class="ra">Søren Huss</h3></div>
<h2 class="fcnl"> 1564</span></span></h2>
</article>

<article class="adcarrow pr">
<img data-src="demobilleder/uligenumre.jpg" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  width="78" alt="NEWS TITLE - ARTISTNAME">
<div class="ellipsis-ca">
<h3 class="ra">Ulige Numre</h3></div>
<h2 class="fcnl"> 2564</span></span></h2>
</article>


</div>
</aside>



</div>





    </div>
       
       

<!-- SOCIAL SHARE -->    
<script src="js/sharre.js"></script>     
<script>
$('#shareme').sharrre({
  share: {
    googlePlus: true,
    facebook: true,
    twitter: true
  },
  enableTracking: true,
  buttons: {
    googlePlus: {size: 'tall', annotation:'bubble'},
    facebook: {layout: 'box_count', lang: 'dk'},
    twitter: {count: 'vertical'}
  },
  hover: function(api, options){
    $(api.element).find('.buttons').show();
  },
  hide: function(api, options){
    $(api.element).find('.buttons').hide();
  }
});
</script>
<!-- SOCIAL SHARE -->  



<!-- VIDEO-POPUP -->
<script type="text/javascript">$('.popup').magnificPopup({type:'inline',});</script>