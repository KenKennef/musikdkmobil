<meta charset="UTF-8">
<meta name="HandheldFriendly" content="True">
<meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale: 1, maximum-scale=1, user-zoomable: false ">


<!-- Mobile IE allows us to activate ClearType technology for smoothing fonts for easy reading -->
<meta http-equiv="cleartype" content="on">



<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="css/style.css">

               
	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>

<script src="js/jquery.dpSocialTimeline.min.js"></script>
<link type="text/css" rel="stylesheet" href="css/dpSocialTimeline.css" />



<script src="js/unveil.js"></script>
<script>$(function(){$("img").unveil(600);});</script> 