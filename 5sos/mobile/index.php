
<!DOCTYPE html>
<html>
<head>
  <title>5 Seconds Of Summer - MusikDK</title>
 <?php include 'includes.php'; ?>


</head>
<body>
<div class="wrapper">


    
    
<figure class="ach"> 
	<img data-src="demobilleder/5sos.jpg" src="demobilleder/placeholders/acheader.png" alt="Kent">
	

<figcaption class="meta">

		<h1>5 Seconds Of Summer</h1>
<!-- 		<h3>1234</h3>		 -->
	</figcaption>
	
</figure>  



<div class="ip">    


		<a href="#modal-slide" title="Vælg service"><button class="btnblue">Lyt</button></a>


</div>

     
     
<header class="h"><span class="font-entypo icon-layout hicon" aria-hidden="true"></span>&nbsp;&nbsp;Seneste update</header>



<div class="ip">
     <div id="socialTimeline" style="width:100%;"></div>
</div>
 	

<script type="text/javascript">
	$(document).ready(function(){
		
		$('#socialTimeline').dpSocialTimeline({
			feeds: 
			{
				'instagram': {data: '5sos'}

			},


			layoutMode: 'one_column',
			skin: 'light',
			showSocialIcons: false,
			showFilter: false,
			share: false,
			oneColumnItemWidth: '100%',
			cache: false,
			cacheTime: 900,	
			addLightbox: false,
			total: 3
		});
		
	});
</script>


 
 

<div class="clear"></div>


<header class="h"><span class="font-entypo icon-video hicon" aria-hidden="true"></span>&nbsp;&nbsp;Nyeste videoer</header>

			<!-- VIDEO-LIST -->
		<ul class="clist">
		
			<li><a href="#video1" title="Se video">
				<img data-src="https://i1.ytimg.com/vi/LzV8Vo3gWwA/mqdefault.jpg" src="demobilleder/placeholders/video.png" alt="ASK:REPLY"></a>
					<span>
					  <h2>ASK:REPLY</h2>
					  <h3>5 Seconds Of Summer</h3>
					</span></a>
			</li>
		
			<li><a href="#video2" title="Se video">
				<img data-src="https://i1.ytimg.com/vi/tZYEK3OyYgE/mqdefault.jpg" src="demobilleder/placeholders/video.png" alt="LIFT Intro"></a>
					<span>
					  <h2>LIFT Intro</h2>
					  <h3>5 Seconds Of Summer</h3>
					</span></a>
			</li>
			
			<li><a href="#video3" title="Se video">
				<img data-src="https://i1.ytimg.com/vi/ym6NDDRFHvc/mqdefault.jpg" src="demobilleder/placeholders/video.png" alt="Don't Stop (Lyric Video)"></a>
					<span>
					  <h2>Don't Stop (Lyric Video)</h2>
					  <h3>5 Seconds Of Summer</h3>
					</span></a>
			</li>
			
		<li><a href="#video4" title="Se video">
				<img data-src="https://i1.ytimg.com/vi/X2BYmmTI04I/mqdefault.jpg" src="demobilleder/placeholders/video.png" alt="She Looks So Perfect"></a>
					<span>
					  <h2>She Looks So Perfect</h2>
					  <h3>5 Seconds Of Summer</h3>
					</span></a>
			</li>
		
			<li><a href="#video5" title="Se video">
				<img data-src="https://i1.ytimg.com/vi/JxAk7a6qNYg/mqdefault.jpg" src="demobilleder/placeholders/video.png" alt="She Looks So Perfect (Teaser 1)"></a>
					<span>
					  <h2>She Looks So Perfect (Teaser 1)</h2>
					  <h3>5 Seconds Of Summer</h3>
					</span></a>
			</li>
			
			<li><a href="#video6" title="Se video">
				<img data-src="https://i1.ytimg.com/vi/FfWg68IyoAg/mqdefault.jpg" src="demobilleder/placeholders/video.png" alt="She Looks So Perfect (Lyric Video)"></a>
					<span>
					  <h2>She Looks So Perfect (Lyric Video)</h2>
					  <h3>5 Seconds Of Summer</h3>
					</span></a>
			</li>
			
			
	</ul>

<header class="h"><span class="font-entypo icon-book hicon" aria-hidden="true"></span>&nbsp;&nbsp;Biografi</header>





<div class="acbio-full">
<p>5 Seconds of Summer er et australsk pop rock band. Bandet består af medlemmerne Luke Hemmings, Michael Clifford, Calum Hood og Ashton Irwin. Bandet blev etableret i Sydney i 2011.</p>

<br>
<p class="infotext fr">Tekst fra Wikipedia</p>
<br>

<!--
<br><h3>Medlemmer</h3>
<ul class="aazlist">
	<li><p>Simon Kvamm</p></li>
	<li><p>Peter Sommer</p></li>

</ul>
-->

</div>









<header class="h"><span class="font-entypo icon-export hicon" aria-hidden="true"></span>&nbsp;&nbsp;Links</header>
<div class="ip">
<table style="width: 100%">
    <tr class="alink">
        <td class="fblink roundedcorners"></a>
		<td class="twlink roundedcorners"><a href="http://www.twitter.com/5SOS"></a>
        <td class="iglink roundedcorners"><a href="http://www.instagram.com/5sos"></a>
        <td class="sclink roundedcorners"><a href="http://www.soundcloud.com/5-seconds-of-summer"></a>

    </tr>	
</table>
</div>















</body>


<!-- LIGHTBOX - START -->
<script src="js/modal.js"></script>
  <?php include 'choosemusicservice.php'; ?>
  
  <?php include 'video1.php'; ?>
  <?php include 'video2.php'; ?>
  <?php include 'video3.php'; ?>
  <?php include 'video4.php'; ?>
  <?php include 'video5.php'; ?>
  <?php include 'video6.php'; ?>
<!--   videos -->

  
<!-- LIGHTBOX - END -->





<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37844616-1', 'musik.dk');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>



</html>