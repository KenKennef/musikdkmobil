<?php
Class category_model extends CI_Model
{
	
	var $table_name    = 'category';
	var $primary_key	= 'id';
	
	
	function get_all_categories()
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		return $this->db->get();
	}
	
	
	function get_category($id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where($this->primary_key,$id);
		return $this->db->get();
	}


	function add_category()
	{
		 $f1= $this->input->post('name');
		 $data_insert['name'] = $f1;
		 $this->db->insert($this->table_name,$data_insert);
	}
	
	function edit_category($id)
	{
		$f1 = $this->input->post('name');
		$data_insert['name'] = $f1;
		$this->db->where($this->primary_key,$id);
		$this->db->update($this->table_name,$data_insert);
		
	}
	
	
	function delete($id)
	{
		$this->db->where($this->primary_key,$id);
		$this->db->delete($this->table_name);
		
		$this->db->where('category_id',$id);
		$this->db->delete('filters');
		
	}
	
}
?>