﻿<?php

    
Class Artist_model extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	var $table_name		= 'music_brainz';
	var $primary_key	= 'id';
	
	function add_musicbrainz($artist_id, $country, $name)
	{
		
		 $data_insert['name'] = $name;
		 
		 $data_insert['artist_id'] = $artist_id;
		 
		 $data_insert['country'] = $country;
		 
		 $this->db->insert($this->table_name,$data_insert);
	}
	
	function add_musicbrainz_url($facebook, $twitter, $wikipedia, $youTube, $vevo)
	{
		
		 $data_insert['facebook'] = $facebook;
		 
		 $data_insert['twitter'] = $twitter;
		 
		 $data_insert['wikipedia'] = $wikipedia;
		 
		 $data_insert['youTube'] = $youTube;
		 
		 $data_insert['vevo'] =  $vevo;
		 
		 $this->db->insert('music_brainz_url',$data_insert);
	}
	
	function add_rovicorp($meta_id, $artist_name, $country, $birth_date, $birth_place, $death_date, $death_place, $discographyUri, $memberOfUri, $influencersUri, $similarsUri, $moodsUri, $musicBioUri, $musicCreditsUri, $songsUri, $musicStylesUri, $associatedWithUri, $themesUri, $artist_id)
	{
		
		 $data_insert['meta_id'] = $meta_id;
		 
		 $data_insert['artist_name'] = $artist_name;
		 
		 $data_insert['country'] = $country;
		 
		 $data_insert['birth_date'] = $birth_date;
		 
		 $data_insert['birth_place'] =  $birth_place;
		 
		 $data_insert['death_date'] =  $death_date;
		 
		 $data_insert['death_place'] =  $death_place;
		 
		 $data_insert['discographyUri'] =  $discographyUri;
		 
		 $data_insert['memberOfUri'] =  $memberOfUri;
		 
		 $data_insert['influencersUri'] =  $influencersUri;
		 
		 $data_insert['similarsUri'] =  $similarsUri;
		 
		 $data_insert['moodsUri'] =  $moodsUri;
		 
		 $data_insert['musicBioUri'] =  $musicBioUri;
		 
		 $data_insert['musicCreditsUri'] =  $musicCreditsUri;
		 
		 $data_insert['songsUri'] =  $songsUri;
		 
		 $data_insert['musicStylesUri'] =  $musicStylesUri;
		 
		 $data_insert['associatedWithUri'] =  $associatedWithUri;
		  
	     $data_insert['themesUri'] =  $themesUri;
		 
		 $data_insert['artist_id'] =  $artist_id;
		 
		 $this->db->insert('rovi_corp',$data_insert);
	}
	
	
	function get_all_artists(){
		
		$this->db->select('*');
		$this->db->from('artists');
		return $this->db->get();
		
	}
	 
}
?>