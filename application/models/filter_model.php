<?php

/**
 * filter_model extends the CI_Model
 */
Class filter_model extends CI_Model {

    var $table_name = 'filters';
    var $primary_key = 'id';
    var $youtube_ids = array();

    /**
     * get_all_filters retrieve all filters 
     */
    function get_all_filters($cat_id) {
        $this->db->select('*');
        $this->db->where('category_id', $cat_id);
        $this->db->from($this->table_name);
        return $this->db->get();
    }

    /**
     * get_filter retrieve the filter info
     */
    function get_filter($id) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where($this->primary_key, $id);
        return $this->db->get();
    }

    /**
     * add_filter, add the filter 
     */
    function add_filter($cat_id) {
        $f1 = $this->input->post('name');
        $data_insert['name'] = $f1;
        $data_insert['category_id'] = $cat_id;
        $this->db->insert($this->table_name, $data_insert);
    }

    /**
     * edit_filter, edit the filter 
     */
    function edit_filter($id) {
        $f1 = $this->input->post('name');
        $data_insert['name'] = $f1;
        $this->db->where($this->primary_key, $id);
        $this->db->update($this->table_name, $data_insert);
    }

    /**
     * delete, delete the filter 
     */
    function delete($id) {
        $this->db->where($this->primary_key, $id);
        $this->db->delete($this->table_name);
    }

    /**
     * get_all_filters_as_array, get all filter
     */
    function get_all_filters_as_array() {
        $this->db->select('*');
        $this->db->from($this->table_name);
        return $this->db->get();
    }

    /**
     * check_video_exits, check video exist or not 
     */
    function check_video_exits($video_id) {
        $this->db->select('*');
        $this->db->from('videos');
        $this->db->where('video_youtube_id', $video_id);
        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            //$query = $this->db->query("SELECT video_id from videos where video_youtube_id = '$video_id'");
            return $result;
        } else {
            return false;
        }
    }

    /**
     * delete_video_by_type, delete the video by type
     */
    function delete_video_by_type($type_id, $type) {/*
      $this->db->where('video_type',$type);
      $this->db->where('type_id',$type_id);
      $this->db->delete('videos'); */

        $this->db->query("DELETE videos,video_category FROM videos INNER JOIN video_category ON videos.video_id =  video_category.video_id WHERE video_category.video_type = '$type' AND video_category.type_id = '$type_id'");
    }

    /**
     * enter_into_db_with_categoty, enter the info into database with category
     */
    function enter_into_db_with_categoty($cat_id, $title, $video_id, $thumbnail, $artist_name, $playlist_item, $type_id) {

        $video_entery_flag = false;
              //echo  $title_use.'_____'.$thumbnail.'_____'.$video_id.'<br>';
                $check_video = $this->check_video_exits($video_id);
                if ($check_video == false) {
                    $data_insert['video_title'] = $title;
                    $data_insert['video_youtube_id'] = $video_id;
                    $data_insert['video_thumbnail'] = $thumbnail;
                    $data_insert['artist_name'] = $artist_name;

                    $this->db->insert('videos', $data_insert);

                    $data_insert1['cat_id'] = $cat_id;
                    $data_insert1['video_type'] = $playlist_item;
                    $data_insert1['type_id'] = $type_id;
                    $data_insert1['video_id'] = $this->db->insert_id();
                    $this->db->insert('video_category', $data_insert1);
                    $this->youtube_ids[] = $video_id;
                }
                /* else{

                  $result = $check_video->result_array();

                  $data_insert1['cat_id'] = $cat_id;
                  $data_insert1['video_type'] = $playlist_item;
                  $data_insert1['type_id']	= $type_id;
                  $data_insert1['video_id'] = $result[0]['video_id'];
                  $query = $this->db->get_where('video_category', array('cat_id' =>$cat_id, 'video_id' =>$data_insert1['video_id']));
                  if($query->num_rows() == 0){
                  $this->db->insert('video_category',$data_insert1);
                  $this->youtube_ids[] = $video_id;
                  }



                  } */
                 return $this->youtube_ids;
    
            }
       // }
        /* else
          {
          $pos      = strripos($title, $filter_word);
          if($pos===false)
          {

          }
          else
          {
          //$cat_n = trim($cat_array[0],$filter_word);
          $title_use = str_ireplace($filter_word,'',$title_use);

          $title_use = trim($title_use);
          $title_use = trim($title_use,'-');
          echo  'else'.$title_use.'_____'.$thumbnail.'_____'.$video_id.'<br>';
          }

          } */
       

    /**
     * get_categories, retrieve categories from database
     */
    function get_categories() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->order_by('name', 'ASC');
        return $this->db->get();
    }

    /**
     * get_videos_by_cat, retrieve video by category
     */
    function get_videos_by_cat($cat_id, $search_term) {
        $this->db->select('*');
        $this->db->from('videos');
        $this->db->where('category_id', $cat_id);
        $this->db->where('artist_name', $search_term);
        return $this->db->get();
    }

    /**
     * get_videos_by_cat, retrieve video by category 
     */
    function get_videos_by_cat_list($cat_id, $search_term, $type_id, $playlist_item) {
        /* $this->db->select('*');
          $this->db->from('videos');
          $this->db->where('category_id',$cat_id);
          $this->db->where('artist_name',$search_term);
          //$this->db->where('video_type',$type_id);
          //$this->db->where('type_id',$playlist_item);
          return $this->db->get(); */
        $query = "select * from videos, video_category where videos.video_id = video_category.video_id and video_category.cat_id ='$cat_id' and videos.artist_name = '$search_term' and video_category.video_type = '$type_id'";
        return $result = $this->db->query($query);
    }

    function save_videos($videos_array, $artist_name, $playlist_item, $type_id) {
        
       
        set_time_limit(0);
        if (isset($videos_array['entry']) != '') {
            foreach ($videos_array['entry'] as $video_item) {
                
                $title = $video_item['title'];
                $id = $video_item['id'];
                $id_explode = explode('/', $id);
                $video_id = $id_explode['6'];
                $thumbnail = "https://i1.ytimg.com/vi/$video_id/default.jpg";

                $video_entery_flag = false;
                
                $title_use = str_ireplace($artist_name, '', $title);
                $title_array = explode('(', $title);
              
                $counter = count($title_array);
                if ($counter > 1) {
                    $cat_name = $title_array[1];
                    $cat_array = explode(')', $cat_name);
 
                    if (isset($cat_array[0])) {

                        $filter_search = trim($cat_array[0]);                  
                        $query = $this->db->get_where('filters', array('name' => $filter_search));
                        if ($query->num_rows() > 0) {
                            $filter = $query->result_array();
                                                      
                            $filter_word = trim($filter[0]['name']);
                            $cat_id = $filter[0]['category_id'];
                          
                            $cat_n = trim($cat_array[0]);
                            $cat_n = str_ireplace($filter_word, '', $cat_n);
                            $title_use = str_ireplace($filter_word, '', $title_use);
                            $cat_n = trim($cat_n);
                        if (empty($cat_n)) {
                            //echo $cat_n.'>>>>';
                            $title_use = str_ireplace(')', '', $title_use);
                            $title_use = str_ireplace('(', '', $title_use);
                        }
                        $title_use = trim($title_use);
                        $title_use = trim($title_use, '-');
                       
                        $video_entery_flag = $this->enter_into_db_with_categoty($cat_id, $title_use, $video_id, $thumbnail, $artist_name, $type_id, $playlist_item);
  
                        }
                    }
                }

                
                
                //echo  '<b>new>>>'.$title.'_____'.$thumbnail.'_____'.$video_id.'>>>>>>>>>>>></b><br>';

               // $filter_words = $this->filter_model->get_all_filters_as_array();

//
//                if ($filter_words->num_rows() > 0) {
//                    foreach ($filter_words->result_array() as $filter) {
//
//                        $filter_word = trim($filter['name']);
//                        $cat_id = $filter['category_id'];
//
//                        $video_entery_flag = $this->enter_into_db_with_categoty($filter_word, $cat_id, $title, $video_id, $thumbnail, $artist_name, $type_id, $playlist_item);
//                    }
//                }

                $check_video = $this->check_video_exits($video_id);
                if ($video_entery_flag == false) {
                    if ($check_video == false) {
                        $title_use = str_ireplace($artist_name, '', $title);
                        $title_use = trim($title_use);
                        $title_use = trim($title_use, '-');

                        $data_insert['video_title'] = $title_use;
                        $data_insert['video_youtube_id'] = $video_id;
                        $data_insert['video_thumbnail'] = $thumbnail;
                        $data_insert['artist_name'] = $artist_name;

                        $this->db->insert('videos', $data_insert);

                        $data_insert2['cat_id'] = 12;
                        $data_insert2['video_type'] = $type_id;
                        $data_insert2['type_id'] = $playlist_item;
                        $data_insert2['video_id'] = $this->db->insert_id();
                        $this->db->insert('video_category', $data_insert2);
                    }

                    /* else
                      {

                      /*$query = $this->db->get_where('videos', array('video_youtube_id' => $video_id));
                      $result = $query->row();
                      $data_insert1['cat_id'] = 12;
                      $data_insert1['video_id'] = $result->video_id;
                      $result = $check_video->result_array();
                      $data_insert1['cat_id'] = 12;
                      $data_insert1['video_type']	= $type_id;
                      $data_insert1['type_id'] = $playlist_item;
                      $data_insert1['video_id'] = $result[0]['video_id'];
                      $query = $this->db->get_where('video_category', array('cat_id' => 12, 'video_id' =>$data_insert1['video_id']));
                      if($query->num_rows() == 0){
                      $this->db->insert('video_category',$data_insert1);
                      }

                      } */
                }
            }

            foreach ($videos_array['link'] as $idx => $url) {
                if (isset($url['@attributes']['rel'])) {
                    if ($url['@attributes']['rel'] == "next") {
                        $next_url = $url['@attributes']['href'];

                        $feedresult = file_get_contents($next_url);
                        $xml = new SimpleXMLElement($feedresult);
                        $json = json_encode($xml);
                        $user_id = json_decode($json, TRUE);
                        $this->save_videos($user_id, $artist_name, $playlist_item, $type_id);
                    }
                }
            }
        }


        /* if(nextelementexists)
          {
          $next_url = '';

          $feedresult = file_get_contents($next_url);
          $xml = new SimpleXMLElement($feedresult);
          $json = json_encode($xml);
          $user_id = json_decode($json, TRUE);
          $this->save_videos($user_id);
          } */
    }

    function save_videos_playlist($play_list_items, $artist_name, $playlist_item, $type_id) {

       
        set_time_limit(0);
      
        if (isset($play_list_items['items']) != '') {

            foreach ($play_list_items['items'] as $video_item) {

                $title = $video_item['snippet']['title'];
                $thumbnail = $video_item['snippet']['thumbnails']['default']['url'];
                $video_id = $video_item['snippet']['resourceId']['videoId'];
                $video_entery_flag = array();
               
                $title_use = str_ireplace($artist_name, '', $title);
                $title_array = explode('(', $title);
              
                $counter = count($title_array);
                if ($counter > 1) {
                    $cat_name = $title_array[1];
                    $cat_array = explode(')', $cat_name);
                 

                    if (isset($cat_array[0])) {


                        $filter_search = trim($cat_array[0]);
                    
                        $query = $this->db->get_where('filters', array('name' => $filter_search));

                        if ($query->num_rows() > 0) {
                            $filter = $query->result_array();
                            
                           
                            $filter_word = trim($filter[0]['name']);
                            $cat_id = $filter[0]['category_id'];
                          
                            $cat_n = trim($cat_array[0]);
                            $cat_n = str_ireplace($filter_word, '', $cat_n);
                            $title_use = str_ireplace($filter_word, '', $title_use);
                            $cat_n = trim($cat_n);
                        if (empty($cat_n)) {
                            //echo $cat_n.'>>>>';
                            $title_use = str_ireplace(')', '', $title_use);
                            $title_use = str_ireplace('(', '', $title_use);
                        }
                        $title_use = trim($title_use);
                        $title_use = trim($title_use, '-');
                       
                        $video_entery_flag = $this->enter_into_db_with_categoty($cat_id, $title_use, $video_id, $thumbnail, $artist_name, $type_id, $playlist_item);
                        
                            
                        } 

           
                    }
                }

//                if ($filter_words->num_rows() > 0) {
//                    foreach ($filter_words->result_array() as $filter) {
//                        $filter_word = trim($filter['name']);
//                        $cat_id = $filter['category_id'];
//
//                        $video_entery_flag = $this->enter_into_db_with_categoty($filter_word, $cat_id, $title, $video_id, $thumbnail, $artist_name, 'playlist', $playlist_item);
//                    }
//                }
 
                
                $check_video = $this->check_video_exits($video_id);
                
               if ($video_entery_flag == false) {
                    if ($check_video == false) {
                        $title_use = str_ireplace($artist_name, '', $title);
                        $title_use = trim($title_use);
                        $title_use = trim($title_use, '-');

                        $data_insert['video_title'] = $title_use;
                        $data_insert['video_youtube_id'] = $video_id;
                        $data_insert['video_thumbnail'] = $thumbnail;
                        $data_insert['artist_name'] = $artist_name;

                        $this->db->insert('videos', $data_insert);

                        $data_insert2['cat_id'] = 12;
                        $data_insert2['video_type'] = 'playlist';
                        $data_insert2['type_id'] = $playlist_item;
                        $data_insert2['video_id'] = $this->db->insert_id();
                        $this->db->insert('video_category', $data_insert2);
                    }
                }
            }

            if (isset($play_list_items['nextPageToken'])) {

                /* Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
                  Google APIs Console <http://code.google.com/apis/console#access>
                  Please ensure that you have enabled the YouTube Data API for your project. */
                $DEVELOPER_KEY = 'AIzaSyBqqPs4u_jrCqWjsoVUbEs1TEumwL428gU';

                $client = new Google_Client();
                $client->setDeveloperKey($DEVELOPER_KEY);

                $youtube = new Google_YoutubeService($client);

                $play_list_items_next = $youtube->playlistItems->listPlaylistItems('id,snippet', array(
                    'playlistId' => $playlist_item,
                    'maxResults' => 50,
                    'pageToken' => $play_list_items['nextPageToken'],
                ));

                $this->save_videos_playlist($play_list_items_next, $artist_name, $playlist_item, $type_id);
            }
        }
    }

}

?>
