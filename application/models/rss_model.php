<?php
class Rss_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
	
	var $table_name		= 'artist_news';
	var $primary_key	= 'news_id';
	var $comparing_key	= 'news_link';
	var $artist_field	= 'artist_id';
	
	function insert_news($insert_data)
	{
		$this->db->select($this->primary_key);
		$this->db->from($this->table_name);
		$this->db->where($this->comparing_key,$insert_data['news_link']);
		$result = $this->db->get();
		if($result->num_rows()<=0)
		{
			$this->db->insert('artist_news', $insert_data); 
		}
	}
	
	
	function get_all_rss_feeds()
	{
		$this->db->select($this->primary_key);
		$this->db->from($this->table_name);
		return $this->db->get();
	}
	
	function get_rss_artist($artist_id)
	{
		$this->db->select($this->primary_key);
		$this->db->from($this->table_name);
		$this->db->where($this->artist_field,$artist_id);
		return $this->db->get();
	}
	
}