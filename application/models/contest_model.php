<?php
Class contest_model extends CI_Model
{
	
	var $table_name		= 'contests';
	var $table_name1		= 'type';
	var $primary_key	= 'contest_id';
	var $table_name2   =  'artists';
	
	
	function get_all_contest()
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		return $this->db->get();
	}
	
	function get_all_user()
	{
		$this->db->select('*');
		$this->db->from('users');
		return $this->db->get();
	}
	
	function get_all_rating(){
		
		$this->db->select('*');
		$this->db->from('pics');
		return $this->db->get();
		
	}
	
	function get_all_video(){
		
		$this->db->select('*');
		$this->db->from('videos');
		return $this->db->get();
		
	}
	
	
	function get_all_artist()
	{
		$this->db->select('*');
		$this->db->from($this->table_name2);
		return $this->db->get();
	}
	function get_all_type()
	{
		$this->db->select('*');
		$this->db->from('type');
		return $this->db->get();
	}
	
	function get_contest($contest_id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where($this->primary_key,$contest_id);
		return $this->db->get();
	}


	function add_contest($final_file_name)
	{
		 $f1= $this->input->post('title');
		 $data_insert['title'] = $f1;
		 
		 $f2= $this->input->post('description');
		 $data_insert['description'] = $f2;
		 
		 $data_insert['image'] = $final_file_name;
		 
		  $f9= $this->input->post('prize');
		 $data_insert['prize'] = $f9;
		 
		$f5=strtotime($this->input->post('start_date')); 
	    $data_insert['start_date'] = $f5;
		
		$f6=strtotime($this->input->post('end_date')); 
	    $data_insert['end_date'] = $f6;
		
		$f10=$this->input->post('type_id'); 
	    $data_insert['type_id'] = $f10;
		 
		 $f7= $this->input->post('status');
		 $data_insert['status'] = $f7;
		 
		 $f8=strtotime($this->input->post('created_date')); 
	     $data_insert['created_date'] = $f8;
		 
		 $this->db->insert($this->table_name,$data_insert);
	}
	
	function edit_contest($contest_id, $final_file_name)
	{
		 $f1= $this->input->post('title');
		 $data_insert['title'] = $f1;
		 
		 $f2= $this->input->post('description');
		 $data_insert['description'] = $f2;
		 
		 $data_insert['image'] = $final_file_name;
		 
		  $f9= $this->input->post('prize');
		 $data_insert['prize'] = $f9;
		 
		$f5=strtotime($this->input->post('start_date')); 
	    $data_insert['start_date'] = $f5;
		
		$f6=strtotime($this->input->post('end_date')); 
	    $data_insert['end_date'] = $f6;
		
		$f10=$this->input->post('type_id'); 
	    $data_insert['type_id'] = $f10;
	   
		 
		 $f7= $this->input->post('status');
		 $data_insert['status'] = $f7;
		 
		 $f8=strtotime($this->input->post('created_date')); 
	     $data_insert['created_date'] = $f8;
		 
		$this->db->where($this->primary_key,$contest_id);
		
		$this->db->update($this->table_name,$data_insert);
		
	}
	
	function delete($contest_id)
	{
		$this->db->where($this->primary_key,$contest_id);
		$this->db->delete($this->table_name);
		
		/*$this->db->where('user_id',$user_id);
		$this->db->delete('user_roles');*/
		
	}
	
	function check_artist($contest_id,$artist_id)
	{
		$this->db->select('*');
		$this->db->from('contest_artist');
		$this->db->where('contest_id',$contest_id);
		$this->db->where('artist_id',$artist_id);
		$result = $this->db->get();
		if($result->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function get_all_types()
	{
		$this->db->select('*');
		$this->db->from($this->table_name1);
		return $this->db->get();
	}
	
	function return_all_types_string($contest_id)
	{
				$str = '';
				$query = 'select * from contest_artist INNER JOIN artists ON artists.artist_id=contest_artist.artist_id where contest_artist.contest_id="'.$contest_id.'"';
				$result = $this->db->query($query);
				//= $this->db->get();
				if($result->num_rows()>0)
				{
					foreach($result->result_array() as $artist)
					{
						$str .= $artist['artist_name'].',';
					}
				}
				if($str!='')
				{
					$str = substr($str,0,-1);
				}
				return $str;
	}
	
	function return_average_rating($id)
	{
				$str = '';
				$avg_rating = '';
				$query = 'select * from pic_rating INNER JOIN pics ON pics.id=pic_rating.pic_id where pic_rating.pic_id="'.$id.'"';
				$result = $this->db->query($query);
				//= $this->db->get();
				if($result->num_rows()>0)
				{
					$count = 0;
					foreach($result->result_array() as $rating)
					{
						$str += $rating['rating'];
						$count = $count+1 ;
					}
					
				 $avg_rating = ($str / $count);
					
				}
				if($str!='')
				{
					$str = substr($str,0,-1);
				}
				
				if($avg_rating!=''){
					return $avg_rating;
				}
				else{
					return "rating not available";
					}
	}
	
	function return_average_video_rating($id)
	{
				$str = '';
				$avg_rating = '';
				$query = 'select * from video_rating INNER JOIN videos ON videos.id=video_rating.video_id where video_rating.video_id="'.$id.'"';
				$result = $this->db->query($query);
				//= $this->db->get();
				if($result->num_rows()>0)
				{
					$count = 0;
					foreach($result->result_array() as $rating)
					{
						$str += $rating['rating'];
						$count = $count+1 ;
					}
					
				 $avg_rating = ($str / $count);
					
				}
				if($str!='')
				{
					$str = substr($str,0,-1);
				}
				
				if($avg_rating!=''){
					return $avg_rating;
				}
				else{
					return "rating not available";
					}
	}
	
	
	
	
	
	function add_type() {
        $f1 = $this->input->post('contest_type');
        $data_insert['name'] = $f1;
        $this->db->insert('type', $data_insert);
    }
	
	 function get_type($type_id) {
        $this->db->select('*');
        $this->db->from('type');
        $this->db->where('type_id', $type_id);
        return $this->db->get();
    }
	 function edit_type($type_id) {
        $f1 = $this->input->post('contest_type');
        $data_insert['name'] = $f1;
        $this->db->where('type_id', $type_id);
        $this->db->update('type', $data_insert);
    }
	
	  function delete_type($type_id) {
        $this->db->where('type_id', $type_id);
        $this->db->delete('type');
    }
	
	 function add_pic($final_file_name, $user_id, $contest_id){
		
		/*  $f1 = $this->input->post('rating');
       	  $data_insert['rating'] = $f1;*/
		 
		 $data_insert['name'] = $final_file_name;
		 
		 $data_insert['user_id'] = $user_id;
		 
	     $data_insert['contest_id'] = $contest_id;
		 
		 $this->db->insert('pics' ,$data_insert);
	}
	
	 function add_video($user_id, $contest_id){
		
		 $f1 = $this->input->post('url');
       	  $data_insert['url'] = $f1;
		 
		 $data_insert['user_id'] = $user_id;
		 
	     $data_insert['contest_id'] = $contest_id;
		 
		 $this->db->insert('videos' ,$data_insert);
	}
		 
		 
		 
		 
		 
}
?>