<?php
Class answer_model extends CI_Model
{
	
	var $table_name		= 'answer';
	var $primary_key	= 'id';
	
	
	function get_all_contest()
	{
		$this->db->select('*');
		$this->db->from('contests');
		return $this->db->get();
	}
	
	function get_all_artist()
	{
		$this->db->select('*');
		$this->db->from('artists');
		return $this->db->get();
	}
	
	
	function get_all_answers($q_id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where('question_id',$q_id);
		return $this->db->get();
	}
	
	function add_answer($question_id , $contest_id)
	{
		 $f2 = $this->input->post('answer');
		 $data_insert['answer'] = $f2;
		 
		 $data_insert['question_id'] = $question_id;
		 
		 $data_insert['contest_id'] = $contest_id;
		 
		  $f3 = $this->input->post('artist_id');
		 $data_insert['artist_id'] = $f3;
		 
		 $this->db->insert($this->table_name,$data_insert);
	}
	
	function get_question($question_id)
	{
		$this->db->select('*');
		$this->db->from('contest_questions');
		$this->db->where('question_id',$question_id);
		return $this->db->get();
	}
	
	function get_answer($id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where('id',$id);
		return $this->db->get();
	}
	
	function edit_answer($id) {
		
        $f1 = $this->input->post('answer');
        $data_insert['answer'] = $f1;
		
		$f2 = $this->input->post('artist_id');
        $data_insert['artist_id'] = $f2;
		
        $this->db->where('id', $id);
        $this->db->update($this->table_name , $data_insert);
    }
	
	 function delete($answer_id) {
        $this->db->where($this->primary_key, $answer_id);
        $this->db->delete($this->table_name);
    }
	
}
?>