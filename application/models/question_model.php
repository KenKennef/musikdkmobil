<?php
Class question_model extends CI_Model
{
	
	var $table_name		= 'contest_questions';
	var $primary_key	= 'question_id';
	
	
	function get_all_contest()
	{
		$this->db->select('*');
		$this->db->from('contests');
		return $this->db->get();
	}
	
	function get_all_question($contest_id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where('contest_id',$contest_id);
		return $this->db->get();
	}
	
	function add_question($contest_id)
	{
		 $f2 = $this->input->post('title');
		 $data_insert['title'] = $f2;
		
		 $f1= $this->input->post('question');
		 $data_insert['question'] = $f1;
		 
		 $data_insert['contest_id'] = $contest_id;
		 
		 $this->db->insert($this->table_name,$data_insert);
	}
	
	function get_questions($question_id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where($this->primary_key,$question_id);
		return $this->db->get();
	}
	function edit_question($question_id) {
		
		 $f2 = $this->input->post('title');
		 $data_insert['title'] = $f2;
		
        $f1 = $this->input->post('question');
        $data_insert['question'] = $f1;
		
        $this->db->where('question_id', $question_id);
        $this->db->update($this->table_name , $data_insert);
    }
	
	 function delete($id) {
        $this->db->where('question_id', $id);
        $this->db->delete($this->table_name);
    }
	
}
?>