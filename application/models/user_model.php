<?php
Class user_model extends CI_Model
{
	
	var $table_name		= 'users';
	var $table_name1		= 'roles';
	var $primary_key	= 'user_id';
	
	
	function get_all_users()
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		return $this->db->get();
	}
	function get_profile($user_id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where($this->primary_key,$user_id);
		return $this->db->get();
	}
	function get_all_roles()
	{
		$this->db->select('*');
		$this->db->from($this->table_name1);
		return $this->db->get();
	}
	
	
	function get_user($user_id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where($this->primary_key,$user_id);
		return $this->db->get();
	}


	function add_user()
	{
		 $f1= $this->input->post('user_first_name');
		 $data_insert['user_first_name'] = $f1;
		 
		 $f2= $this->input->post('user_sur_name');
		 $data_insert['user_sur_name'] = $f2;
		 
		 $f3= $this->input->post('user_email');
		 $data_insert['user_email'] = $f3;
		 
		 $f4= $this->input->post('user_password');
		 $data_insert['user_password'] = $f4;
		 
		$f5=strtotime($this->input->post('user_birthday')); 
	    $data_insert['user_birthday'] = $f5;
	   
		 
		 $f6= $this->input->post('user_address');
		 $data_insert['user_address'] = $f6;
		 
		 $f7= $this->input->post('user_city');
		 $data_insert['user_city'] = $f7;
		 
		 $f8= $this->input->post('user_country');
		 $data_insert['user_country'] = $f8;
		 
		 $this->db->insert($this->table_name,$data_insert);
	}
	
	function edit_user($user_id)
	{
		 $f1= $this->input->post('user_first_name');
		 $data_insert['user_first_name'] = $f1;
		 
		 $f2= $this->input->post('user_sur_name');
		 $data_insert['user_sur_name'] = $f2;
		 
		 $f3= $this->input->post('user_email');
		 $data_insert['user_email'] = $f3;
		 
		 $f4= $this->input->post('user_password');
		 $data_insert['user_password'] = $f4;
		 
	     $f5=strtotime($this->input->post('user_birthday')); 
	    $data_insert['user_birthday'] = $f5;
		 
		 $f6= $this->input->post('user_address');
		 $data_insert['user_address'] = $f6;
		 
		 $f7= $this->input->post('user_city');
		 $data_insert['user_city'] = $f7;
		 
		 $f8= $this->input->post('user_country');
		 $data_insert['user_country'] = $f8;
		 
		$this->db->where($this->primary_key,$user_id);
		$this->db->update($this->table_name,$data_insert);
		
	}
	function edit_profile($user_id)
	{
		 $f1= $this->input->post('user_first_name');
		 $data_insert['user_first_name'] = $f1;
		 
		 $f2= $this->input->post('user_sur_name');
		 $data_insert['user_sur_name'] = $f2;
		 
	     $f5=strtotime($this->input->post('user_birthday')); 
	    $data_insert['user_birthday'] = $f5;
		 
		 $f6= $this->input->post('user_address');
		 $data_insert['user_address'] = $f6;
		 
		 $f7= $this->input->post('user_city');
		 $data_insert['user_city'] = $f7;
		 
		 $f8= $this->input->post('user_country');
		 $data_insert['user_country'] = $f8;
		 
		$this->db->where($this->primary_key,$user_id);
		$this->db->update($this->table_name,$data_insert);
		
	}
	
	
	function delete($user_id)
	{
		$this->db->where($this->primary_key,$user_id);
		$this->db->delete($this->table_name);
		
		$this->db->where('user_id',$user_id);
		$this->db->delete('user_roles');
		
	}
	
	function check_role($user_id,$role_id)
	{
		$this->db->select('*');
		$this->db->from('user_roles');
		$this->db->where('role_id',$role_id);
		$this->db->where('user_id',$user_id);
		$result = $this->db->get();
		if($result->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function return_all_roles_string($user_id)
	{
				$str = '';
				$query = 'select * from user_roles INNER JOIN roles ON roles.role_id=user_roles.role_id where user_roles.user_id="'.$user_id.'"';
				$result = $this->db->query($query);
				//= $this->db->get();
				if($result->num_rows()>0)
				{
					foreach($result->result_array() as $role)
					{
						$str .= $role['role_name'].',';
					}
				}
				if($str!='')
				{
					$str = substr($str,0,-1);
				}
				return $str;
	}
	
	public function get_user_data(){
			  
			  	$this->db->select('*');
				$this->db->from($this->table_name);
				$this->db->where('user_email',$this->session->userdata('email'));
				return $this->db->get();
			 
			 }
			 
	function edit_image($final_file_name, $u_id)
	{
		 $data_insert['img_name'] = $final_file_name;
		 
		$this->db->where($this->primary_key,$u_id);
		$this->db->update($this->table_name,$data_insert);
		
	}
		 
		 
		 
		 
		 
		 
}
?>