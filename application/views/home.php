
<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
<meta name="HandheldFriendly" content="True">
<meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale: 1, maximum-scale=1, user-zoomable: false, minimal-ui">



<!-- Include Font -->
<link href='//fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>


<!-- CHROME 31 and up - Webapp icon accept -->
<meta name="mobile-web-app-capable" content="yes">

<!-- Mobile IE allows us to activate ClearType technology for smoothing fonts for easy reading -->
<meta http-equiv="cleartype" content="on">

<!-- Bing SEO -->
<meta name="geo.placename" content="Denmark" />
<meta name="geo.position" content="55.668291;12.562866" />
<meta name="geo.region" content="DK" />



						<!-- ICONS -->

<!-- 	CHROME -->
<link rel="icon" sizes="196x196" href="favicons/fav196.png">
<link rel="icon" sizes="128x128" href="favicons/fav128.png">

<!-- STANDARD	 -->
<link rel="shortcut icon" href="favicons/favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-touch-icon-152x152.png">
<link rel="icon" type="image/png" href="favicons/favicon-196x196.png" sizes="196x196">
<link rel="icon" type="image/png" href="favicons/favicon-160x160.png" sizes="160x160">
<link rel="icon" type="image/png" href="favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
<meta name="msapplication-TileColor" content="#1c3c50">
<meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">
<meta name="msapplication-config" content="favicons/browserconfig.xml">
<!-- FOR DEVELOPMENT ONLY -->
        <!-- link rel="stylesheet/less" type="text/css" href="<?= base_url() ?>assets/css/style.less" />
        <script src="<?= base_url() ?>assets/js/less.js" type="text/javascript"></script -->
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style_mobile.css" />
<style>
#loginwrap{
/** To edit this background, follow this link:
http://svgeneration.com/generate/Circular-Overlap 2?background=884870&foreground=513882&count=3&height=168&opacity=0.12&
*/
background-color: #513882;background-image:url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHhtbG5zOnhsaW5rPSdodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rJyB3aWR0aD0nMTAwJScgaGVpZ2h0PSczMDAnPgoJPGRlZnM+CgkJPGcgaWQ9J2MnPgoJCQk8ZWxsaXBzZSBjeD0nLTUwJScgcng9JzE1MCUnIHJ5PScxNjglJyBmaWxsLW9wYWNpdHk9JzAuMTInIGZpbGw9JyM1MTM4ODInLz4KCQk8L2c+Cgk8L2RlZnM+Cgk8cmVjdCB3aWR0aD0nMTAwJScgaGVpZ2h0PScxMDAlJyBmaWxsPScjODg0ODcwJy8+Cgk8dXNlIHhsaW5rOmhyZWY9JyNjJyB5PSctMjAwJScvPgoJPHVzZSB4bGluazpocmVmPScjYycgeT0nLTE2Ni42NjY2NjY2NjY2NjY2OSUnLz4KCTx1c2UgeGxpbms6aHJlZj0nI2MnIHk9Jy0xMzMuMzMzMzMzMzMzMzMzMzQlJy8+Cgk8dXNlIHhsaW5rOmhyZWY9JyNjJyB5PSctMTAwJScvPgoJPHVzZSB4bGluazpocmVmPScjYycgeT0nLTY2LjY2NjY2NjY2NjY2NjY3JScvPgoJPHVzZSB4bGluazpocmVmPScjYycgeT0nLTMzLjMzMzMzMzMzMzMzMzMzNiUnLz4KCTx1c2UgeGxpbms6aHJlZj0nI2MnIHk9JzAlJy8+Cgk8dXNlIHhsaW5rOmhyZWY9JyNjJyB5PSczMy4zMzMzMzMzMzMzMzMzMzYlJy8+Cgk8dXNlIHhsaW5rOmhyZWY9JyNjJyB5PSc2Ni42NjY2NjY2NjY2NjY2NyUnLz4KCTx1c2UgeGxpbms6aHJlZj0nI2MnIHk9JzEwMCUnLz4KCTx1c2UgeGxpbms6aHJlZj0nI2MnIHk9JzEzMy4zMzMzMzMzMzMzMzMzNCUnLz4KCTx1c2UgeGxpbms6aHJlZj0nI2MnIHk9JzE2Ni42NjY2NjY2NjY2NjY2OSUnLz4KCTx1c2UgeGxpbms6aHJlZj0nI2MnIHk9JzIwMCUnLz4KCTx1c2UgeGxpbms6aHJlZj0nI2MnIHk9JzIzMy4zMzMzMzMzMzMzMzMzNCUnLz4KCTx1c2UgeGxpbms6aHJlZj0nI2MnIHk9JzI2Ni42NjY2NjY2NjY2NjY3JScvPgoJPHVzZSB4bGluazpocmVmPScjYycgeT0nMzAwJScvPgo8L3N2Zz4=');
overflow:hidden;
}

</style>

<!-- FIX FOR MUSICSERVICE IMAGES IN IE -->
<!--[if IE]>
	<script>$(document).ready(function(){$('.iebgfix').each(function(){var url = $(this).css('background-image');$(this).css('background-image', url.replace('.svgz', '.png' ));});});</script>
<![endif]-->

<!-- END OF FOR DEVELOPMENT ONLY -->

</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5F25ZX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5F25ZX');</script>
<!-- End Google Tag Manager -->
	<div id="mdkMainWrap"></div>
	<script data-main="<?= base_url() ?>js/app/config" src="<?= base_url() ?>js/lib/require-2.1.14.min.js" type="text/javascript"></script>
	
	
	<!-- FOR DEVELOPMENT ONLY MUST BE REMOVED -->
	
<!-- LOGIN -->
<div  id="complogin" class="compModal" style="z-index:-9999;display: none; ">

			<!-- CONTENT -->
			
			<div id="loginwrap">
				<div id="logininnerwrap">
					<div id="login">
					<div style="padding:50px;">
						<a class="modal-close" title="Luk"></a>					
					</div>
						<div class="tc">
						<!-- 		<a href="signup.php"> -->
							<h3>Log ind eller opret en profil ved at forbinde med din fortrukne tjeneste.</h3>
						<!-- 		</a> -->
						</div>
						<br />	
						<button class="btnfacebook btnspacer" onclick="hello('facebook').login( {scope:'email'} );">Log ind / tilmeld med Facebook</button>		
						<button class="btngoogle" onclick="hello('google').login( {scope:'email'} );">Log ind / tilmeld med Google</button>
						<br />
						<br />		
					</div>					
				</div>

	</div>
</div>


<!-- COMPETITION ERROR MODAL -->
<div id="comperror" class="compModal"	style="z-index:-9999;display: none; ">
	<div class="modal-inner red">
		<div class="modal-content red">
			<!-- 	CONTENT START			 -->
			<div style="padding:50px;">
				<a class="modal-close" title="Luk"></a>					
			</div>
			<div class="tc">	
			<span class="font-entypo icon-warning comppopupicon" aria-hidden="true"></span>
				<h2 class="whitetext">Du deltager allerede!</h2>		
			<div class="compbtnwrap"><a href="" class="artistchannelLink"><button class="btnblue"></button></a></div>	
			</div>			
			<!-- 	CONTENT END			 -->
		</div>
	</div>			
</div>


<!-- COMPETITION SUCCESS MODAL -->


<div id="compyes" class="compModal" style="z-index:-9999; display: none;">
	<div class="modal-inner green">
		<div class="modal-content green">
			<!-- 	CONTENT START			 -->
			<div style="padding:50px;">
				<a class="modal-close" title="Luk"></a>					
			</div>
			<div class="tc">	
			<span class="font-entypo icon-thumbs-up comppopupicon" aria-hidden="true"></span>
			<h2 class="whitetext">Du deltager nu</h2>		
			<div class="compbtnwrap"><a href="" class="artistchannelLink"><button class="btnblue"></button></a></div>	
			</div>
			<!-- 	CONTENT END			 -->
		</div>
	</div>
</div>

<!-- COMPETITION MAX  MODAL -->
<div id="compmax" class="compModal" style="z-index:-9999; display: none;">
	<div class="modal-inner green">
		<div class="modal-content green">
			<!-- 	CONTENT START			 -->
			<div style="padding:50px;">
				<a class="modal-close" title="Luk"></a>					
			</div>
			<div class="tc">	
			<div class="modal-inner green">
				<div class="modal-content green">				
				<div class="tc">	
					<span class="font-entypo icon-thumbs-up comppopupicon" aria-hidden="true"></span>					
					<h2 class="whitetext">Tak du har nu stemt på 5 favoritter</h2>		
				</div>
				</div>
			</div>
			</div>
			<!-- 	CONTENT END			 -->
		</div>
	</div>
</div>


<!-- END OF DEVELOPMENT ONLY -->
</body>
</html> 