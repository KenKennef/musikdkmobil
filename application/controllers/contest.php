<?php

      /**
	    * class contest extends the CI_Controller
		*if the user login then he access to that page 

	    */

class contest extends CI_Controller {
	
	/**
	    * construct method load the helpers
		*if session id is null then he redirects to login page

	    */
	
	function __construct()
		{
			parent::__construct();
			$this->load->helper(array('form', 'url', 'html', 'array'));
			$this->load->model('contest_model');
			$admin_id = $this->session->userdata('id');
			if($admin_id=='')
			{
				redirect('login', 'refresh');
			}
		}
		
		/**
	    * Index method listing the contests 
		* Index method loading the view of contest_listing.
		*/
		
		
	public function index()
		{
			$data['contests'] = $this->contest_model->get_all_contest();
			$data['types'] = $this->contest_model->get_all_type();
			$data['contents'] = $this->load->view('contest_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}
		
		/**
	    * add method , add the contest from the user
		* add method loading the view of contest_add.

	    */
		
	public function add()
	{
		
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
	 	//$this->form_validation->set_rules('userfile', 'userfile', 'callback_handle_upload');
		$this->form_validation->set_rules('prize', 'Prize', 'trim|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('type_id', 'Contest Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('created_date', 'Created Date', 'trim|required|xss_clean');
		
	 	if($this->form_validation->run() == FALSE)
   		{
			$data['artists'] = $this->contest_model->get_all_artist();
			$data['types'] = $this->contest_model->get_all_type();
	   		$data['contents']	= 	$this->load->view('contest_add',$data, true);
			$this->load->view('template',$data);
		}
		else
		{
			
			 if($_FILES['userfile']['name']!='')
				{
					$image = $_FILES['userfile']['name'];
					$image = time().'_'.$image;
					
					$name_ext = end(explode(".", basename($_FILES['userfile']['name'])));
					
					$original_path = 'upload/contest_images/';
					
					$path	=	$original_path.$image;
					move_uploaded_file($_FILES['userfile']['tmp_name'],$path)or die('could not upload');
					$data['category']=$this->contest_model->add_contest($image);
					header('Location: '.base_url().'contest/index');
				}
		}
	}
	
	
		/**
	    * handle_upload method , upload the pic

	    */
	
	 function handle_upload() {
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
    }
	
	/**
	    * edit method , edit the contest
		* edit method loading the view of contest_edit.
		*/
	
	
	public function edit($contest_id=0)
	{
		

	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('prize', 'Prize', 'trim|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('type_id', 'Contest Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
		$this->form_validation->set_rules('created_date', 'Created Date', 'trim|required|xss_clean');
	 
		
	 	if($this->form_validation->run() == FALSE)
   		{
			$data['artists'] = $this->contest_model->get_all_artist();
			$data['types'] = $this->contest_model->get_all_type();
			$data['contests'] = $this->contest_model->get_contest($contest_id);
	   		$data['contents']	= 	$this->load->view('contest_edit',$data,true);
			$this->load->view('template',$data);
		}
		else
		{
			
			if($_FILES['userfile']['name']!='')
				{
					$query = $this->db->get_where('contests', array('contest_id' => $contest_id));
                	$row = $query->row();
                	$picture = $row->image;
                	unlink('upload/contest_images/' . $picture);
					
					
					$image = $_FILES['userfile']['name'];
					$image = time().'_'.$image;
					
					$name_ext = end(explode(".", basename($_FILES['userfile']['name'])));
					
					$original_path = 'upload/contest_images/';
					
					$path	=	$original_path.$image;
					move_uploaded_file($_FILES['userfile']['tmp_name'],$path)or die('could not upload');
					$data['contests'] = $this->contest_model->edit_contest($contest_id, $image);
                    header('Location: ' . base_url() . 'index.php/contest/index');
				 } else {

					$final_file_name = $_POST['image_name_old'];
					$data['contests'] = $this->contest_model->edit_contest($contest_id, $final_file_name);
					header('Location: ' . base_url() . 'index.php/contest/index');
                }
	
			
		}
	}
	
	/**
	    * delete method , delete the contest which is given by the user
	    */
	

	public function delete($contest_id=0)
	{
		 $query = $this->db->get_where('contests', array('contest_id' => $contest_id));
		 if ($query->num_rows() > 0) {
			 
            $row = $query->row();
            $picture = $row->image;
            unlink(realpath('upload/contest_images/' . $picture));
            $this->db->delete('contests', array('contest_id' => $contest_id));
            header('Location: ' . base_url() . 'index.php/contest/index');
            return true;
        }
        return false;
	}
	
	   /**
	    * contest_type_listing method listing the type of contests 
		* contest_type_listing method loading the view of contest_type_listing.
		*/
	
	
	public function contest_type_listing($contest_id=0){
		
			$data['types'] = $this->contest_model->get_all_types();
			
			$data['contents'] = $this->load->view('contest_type_listing',$data, true);
			
			$this->load->view('template',$data);
	}
	
	/**
	    * contest_artist method show contest relating to artist 
		* contest_artist method loading the view of artist_listing.
		*/
	
	public function contest_artist($contest_id=0){
		
			$data['artists'] = $this->contest_model->get_all_artist();
			
			$data['contest_id'] = $contest_id;
			
			$data['contents'] = $this->load->view('artist_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}
		
		/**
	    * contest_artist_entry method enter the artist into contest
		*/
	
	
	function contest_artist_entry($contest_id=0)
	{
		$artists = $this->input->post('artist');
		$this->db->where('contest_id',$contest_id);
		$this->db->delete('contest_artist');
		
		foreach($artists as $artist)
		{
			$artist_update['artist_id'] = $artist;
			$artist_update['contest_id'] = $contest_id;
			$this->db->insert('contest_artist',$artist_update);
		}
		
		header("Location: ".base_url()."contest");
	}
	
	
	   /**
	    * contest_type_add method enter the contest type .
		* contest_artist method loading the view of contest_type_add.
		*/
	
	public function contest_type_add(){
		
		$this->load->library('form_validation');
        $this->form_validation->set_rules('contest_type', 'Contest Type', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $data['contents'] = $this->load->view('contest_type_add', '', true);
            $this->load->view('template', $data);
        } else {
            $data['drugs'] = $this->contest_model->add_type();
            header('Location: ' . base_url() . 'index.php/contest/contest_type_listing');
        }
		
		}
		
	
	
	   /**
	    * edit_type_edit method edit the contest type .
		* edit_type_edit method loading the view of contest_type_edit.
		*/
		
	 public function edit_type_edit($type_id=0) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('contest_type', 'Contest Type', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $data['types'] = $this->contest_model->get_type($type_id);
            $data['contents'] = $this->load->view('contest_type_edit', $data, true);
            $this->load->view('template', $data);
        } else {
            $data['drugs'] = $this->contest_model->edit_type($type_id);
            header('Location: ' . base_url() . 'index.php/contest/contest_type_listing');
        }
    }
	
	/**
	    * delete_type method delete the contest type .
		*/
	 public function delete_type($type_id=0) {
        $this->contest_model->delete_type($type_id);
        header('Location: ' . base_url() . 'index.php/contest/contest_type_listing');
    }
	
	   /**
	    * add_pic method add the pic .
		*/
	public function add_pic($contest_id = 0)
	{
	
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('userfile', 'userfile', 'trim');
		/*$this->form_validation->set_rules('rating', 'rating', 'trim|required|xss_clean');*/
		
	 	if($this->form_validation->run() == FALSE)
   		{
			$data['contest_id'] = $contest_id ;
			
	   		$data['contents']	= 	$this->load->view('pic_add', $data , true);
			$this->load->view('template',$data);
		}
		else
		{
			$contest_id = $_POST['contest_id'];

			$user_id = $this->session->userdata('id');
					
			$image = $_FILES['userfile']['name'];
			$image = time().'_'.$image;
					
			$name_ext = end(explode(".", basename($_FILES['userfile']['name'])));
					
			$original_path = 'upload/pic_contest/';
					
			$path	=	$original_path.$image;
			move_uploaded_file($_FILES['userfile']['tmp_name'],$path)or die('could not upload');

			$data['category']=$this->contest_model->add_pic($image, $user_id, $contest_id);
			header('Location: '.base_url()."contest/rating_listing/$contest_id");
		}
	}
	
	 /**
	    * rating_listing method rate the pic contest .
		*/
	
	public function rating_listing($contest_id = 0)
		{
			$data['contest_id'] = $contest_id ;
			
			$data['contests'] = $this->contest_model->get_all_contest();
			$data['users'] = $this->contest_model->get_all_user();
			
			$data['ratings'] = $this->contest_model->get_all_rating();
			
			/*print_r($data['ratings']);
			exit;*/
			
			$data['contents'] = $this->load->view('rating_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}
		
		 /**
	    * delete_rating method delete the rating .
		*delete the pic to the uploaded directory
		*/
		public function delete_rating($rating_id=0 , $contest_id = 0){
			
			$id = $rating_id ;
			
			$c_id = $contest_id ;
			
			
		 $query = $this->db->get_where('pics', array('id' => $id));
		 
		 if ($query->num_rows() > 0) {
			 
            $row = $query->row();

            $picture = $row->name;
			
            unlink(realpath('upload/pic_contest/' . $picture));
            $this->db->delete('pics', array('id' => $id));
            header('Location: ' . base_url() . "index.php/contest/rating_listing/$c_id");
            return true;
        }
        return false;
	}
	
	
	
	/* function valid_url_format($str){
        $pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
        if (!preg_match($pattern, $str)){
            echo 'The URL you entered is not correctly formatted.';
            return FALSE;
        }
 
        return TRUE;
    }     */

	
	/**
	    * add_video method add the video .
		* add_video method loading the view of video_add
		*/
	
	public function add_video($contest_id = 0){
		
			//$this->load->helper('video');
			/* $vimeo_url = 'http://vimeo.com/34792993';
			 echo vimeo_embed($vimeo_url);*/
			 
			 
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('url', 'url','trim|required|xss_clean');
			 
			 
			if($this->form_validation->run() == FALSE)
			{
				$data['contest_id'] = $contest_id ;
				
				$data['contents']	= 	$this->load->view('video_add', $data , true);
				$this->load->view('template',$data);
			}
			else
			{
				$contest_id = $_POST['contest_id'];	
				
				$user_id = $this->session->userdata('id');
	
				$data['contents']=$this->contest_model->add_video($user_id, $contest_id);
				header('Location: '.base_url()."contest/video_listing/$contest_id");
			}
			 
		}
		
		/**
	    * video_listing method listing the videos.
		* video_listing method loading the view of video_listing
		*/
		
		public function video_listing($contest_id = 0){
			
			$data['contest_id'] = $contest_id ;
			
			$data['contests'] = $this->contest_model->get_all_contest();
			$data['users'] = $this->contest_model->get_all_user();
			$data['videos'] = $this->contest_model->get_all_video();
			
			$data['contents'] = $this->load->view('video_listing',$data, true);
			
			$this->load->view('template',$data);
			
			
			}
			
			/**
	    * delete_video method delete the videos.
		*/
	
			
		public function delete_video($id= 0 ,$contest_id = 0){
			 $video_id = $id ;
			 $c_id = $contest_id ;
		/*	 print_r($c_id );
			 exit;
			 */
			 $this->db->where('id', $id);
        	 $this->db->delete('videos');
			 header('Location: '.base_url()."contest/video_listing/$c_id");
		}
		
	/*	
		public function video(){
			$this->load->helper('video');
			 $vimeo_url = 'http://vimeo.com/6213767';
			 echo vimeo_embed($vimeo_url);
			
			}*/
}

/* End of file contest.php */
/* Location: ./application/controllers/contest.php */

?>