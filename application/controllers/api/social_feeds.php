<?php

require(APPPATH.'libraries/REST_Controller.php');

class social_feeds extends REST_Controller 
{
	var $artist_id;
	private $consumerKey = "hGG4sqbn9LbhbX1VwHcOQ";
    private $consumerSec = "CVeNID6RjmmXCP4nWCDY4YATgswWm3jSSYF4Tjtc4";
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('instagram_api');
		
		$this->load->helper('new_helper');
		$this->load->database();
		
		$this->session->set_userdata('instagram-token', '1194241575.ba48c14.60c32e4d0e0c47c58d639f0f725968e0');
		$this->session->set_userdata('instagram-username', 'alifayyazz');
		$this->session->set_userdata('instagram-profile-picture', 'http://images.ak.instagram.com/profiles/profile_1194241575_75sq_1395271215.jpg');
		$this->session->set_userdata('instagram-user-id', '1194241575');
		$this->session->set_userdata('instagram-full-name', 'Ali Fayyaz');
				
		$this->instagram_api->access_token = $this->session->userdata('instagram-token');
	}
	
	function instagram_tags($tag)
	{
	$tags_recent_data = $this->instagram_api->tagsRecent($tag);
		
	if(!isset($tags_recent_data->meta->error_message))
		{
			if(is_array($tags_recent_data->data))
			{
				$counter = 0;
				foreach($tags_recent_data->data as $feed_data)
				{
					//print_r($feed_data);
					
					/*$image = array('src' => $feed_data->images->standard_resolution->url);
					if(count($feed_data->tags) > 0)
					{
						foreach($feed_data->tags as $tag)
						{
							echo $tag;
						} 
					}*/
					
					
					$array[$counter]['title'] 			= $feed_data->images->standard_resolution->url;
					$array[$counter]['description'] 	= utf8_encode($feed_data->caption->text);
					$array[$counter]['pubDate']			= $feed_data->caption->created_time;
					$array[$counter]['link']			= $feed_data->user->username;
					$array[$counter]['type']			= 'Instagram';
					++$counter;
					if($counter==5)
					{
					//	break;
					}
				
					//print_r($array);
					
				}
				$result = json_encode($array);
				echo $result;
				exit;
			}
		}	
	}
	
	function instagram_get($artist_id)
	{
		$user_name = '';
		$sql = "SELECT urls FROM social_urls WHERE type = 'instagram' and artist_id = '".$artist_id."' ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			foreach($query->result_array() as $row)
			{
				$url = $row['urls'];
				$user_name = end(explode('/',$url));
			}
		}
		if($user_name=='')
		{
			exit;//redirect('');
		}
		
		$user_search_data = $this->instagram_api->userSearch($user_name);
		
		if(isset($user_search_data->data)) 
		{
			if(count($user_search_data->data) != 0) 
			{
				$array_users = $user_search_data->data;
				foreach($array_users as $users)
				{
					if($users->username== $user_name )
					{
						$user_recent_data = $this->instagram_api->getUserRecent($users->id);
						//print_r($user_recent_data);
						if(!isset($user_recent_data->meta->error_message))
						{
							if(is_array($user_recent_data->data))
							{
								$counter = 0;
								foreach($user_recent_data->data as $feed_data)
								{
									//print_r($feed_data);
									
									/*$image = array('src' => $feed_data->images->standard_resolution->url);
									if(count($feed_data->tags) > 0)
									{
										foreach($feed_data->tags as $tag)
										{
											echo $tag;
										} 
									}*/
									
									$time_string = $this->time_elapsed_string($feed_data->caption->created_time);
									$array[$counter]['title'] 			= $feed_data->images->standard_resolution->url;
									$array[$counter]['description'] 	= $feed_data->caption->text;
									$array[$counter]['pubDate']			= $time_string;
									$array[$counter]['link']			= $feed_data->link;
									$array[$counter]['type']			= 'Instagram';
									++$counter;
									if($counter==5)
									{
									//	break;
									}
								
									//print_r($array);
									
								}
								$result = json_encode($array);
								echo $result;
								exit;
							}
						}
					}
					else
					{
						continue;
					}
				}
			}
		}
		
	}
	
	
	function fetchNewTweet_get($artist_id) 
	{
		$username = '';
		$sql = "SELECT urls FROM social_urls WHERE type = 'twitter' and artist_id = '".$artist_id."' ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			foreach($query->result_array() as $row)
			{
				$url = $row['urls'];
				$username = end(explode('/',$url));
				
				
				date_default_timezone_set('America/Los_Angeles');
				 $consumerEncoded = base64_encode($this->consumerKey.':'.$this->consumerSec);
				 $bearerToken = $this->request("https://api.twitter.com/oauth2/token",'Basic',$consumerEncoded,true);
				 if($bearerToken == false) {
					return false;
				 }
				 $tweet = $this->request("https://api.twitter.com/1.1/statuses/user_timeline.json?count=20&screen_name=".$username,'Bearer',$bearerToken);
				 $tweet = json_decode($tweet);
				 //print_r($tweet);
				 
				 $counter = 0;
				 if(is_array($tweet))
				 {
					foreach($tweet as $item)
					{
						$tweet_text = $item->text;
						
						$tweet_text = preg_replace("/@(\w+)/", "<a href=\"http://twitter.com/$1\" target=\"_blank\">@$1</a>", $tweet_text);
						$tweet_text = preg_replace("/#([a-z_0-9]+)/i", "<a href=\"http://twitter.com/search/$1\"  target=\"_blank\">$0</a>", $tweet_text);
						$time_string = $this->time_elapsed_string(strtotime($item->created_at));
						$array[$counter]['title'] 			= make_clickable($tweet_text);
						$array[$counter]['description'] 	= $item->text;
						$array[$counter]['pubDate']			= $time_string;
						$array[$counter]['link']			= '';
						$array[$counter]['type']			= 'Twitter';
						++$counter;
						if($counter==5)
						{
							break;
						}
					}
				 }
			}
		}
		if($username==''){ exit; }
		
			//print_r($array);
			$result = json_encode($array);
			echo $result;
			exit;
		 
         
	}
	
	function time_elapsed_string($ptime)
	{
		$etime = time() - $ptime;
	
		if ($etime < 1)
		{
			return '0 seconds';
		}
	
		$a = array( 12 * 30 * 24 * 60 * 60  =>  'år',
					30 * 24 * 60 * 60       =>  'måned',
					24 * 60 * 60            =>  'dag',
					60 * 60                 =>  'time',
					60                      =>  'minutter',
					1                       =>  'sekund'
					);
	
		foreach ($a as $secs => $str)
		{
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return $r . ' ' . $str . ($r > 1 ? 'e' : '') . ' siden';
			}
		}
	}

	function facebook_get($artist_id)
	{
		$username = '';
		$sql = "SELECT urls FROM social_urls WHERE type = 'facebook' and artist_id = '".$artist_id."' ";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			foreach($query->result_array() as $row)
			{
				$url = $row['urls'];
				$username = end(explode('/',$url));
			}
		}
		if($username==''){ exit; }
		date_default_timezone_set('America/Los_Angeles');
		require_once('CurlSetup.php');
		
		
		echo $url					=	"http://graph.facebook.com/".$username;
		$output					=	@file_get_contents($url);
		
		$out					=	json_decode($output);
		if(!isset($out->id))
		{
			$array = array();
			$result = json_encode($array);
			echo $result;
			exit;
		}
		else
		{
			$facebook_id			=	$out->id;
			//exit;
			//echo 'http://www.facebook.com/feeds/page.php?format=rss20&id='.$facebook_id;
		
			$url = 'http://www.facebook.com/feeds/page.php?format=rss20&id='.$facebook_id;
			$optArr = array();
			$obj = new CurlSetup($url, $optArr);
			$input = $obj->curlOutput();
	
			//echo $input;
			
			$this->load->library('Xml2array',NULL,'myXml2array');
			
			$facebook_array = $this->myXml2array->createarray($input);
			
			//print_r($facebook_array['rss']['channel']['item']);
			$counter = 0;
			foreach($facebook_array['rss']['channel']['item'] as $item)
			{
				$time_string = $this->time_elapsed_string(strtotime($item['pubDate']));
				
				$array[$counter]['facebook_title'] 	= $item['title']['@cdata'];
				$array[$counter]['description'] 	= $item['description']['@cdata'];
				$array[$counter]['pubDate']			= $time_string;
				$array[$counter]['link']			= $item['link'];
				$array[$counter]['type']			= 'Facebook';
				++$counter;
				if($counter==5)
				{
					break;
				}
			}
			$result = json_encode($array);
			echo $result;
			exit;
		}
	}
	  
	  
	function request($url,$authType,$authValue,$bearer=false) {
         $ch = curl_init();
         if($bearer == true) {
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,"grant_type=client_credentials");
         }
         curl_setopt($ch,CURLOPT_URL,$url);
         curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
         curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 5);
         curl_setopt($ch,CURLOPT_USERAGENT, "Tweet Fetcher PHP 0.0.1");
         curl_setopt($ch,CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
         curl_setopt($ch,CURLOPT_HTTPHEADER,array('Authorization: '.$authType.' '.$authValue,'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'));
         $result = curl_exec($ch);
         curl_close($ch);

         if($bearer == true) {
            $json = json_decode($result);
            if(isset($json->{'access_token'})) {
               return $json->{'access_token'};
            }
            return false;
         }
         return $result;
      }
	  
	

}
