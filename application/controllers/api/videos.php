<?php

	require(APPPATH.'libraries/REST_Controller.php');

    class videos extends REST_Controller{
  
	////// API FOR ALL ARTISTS WITH BEGINNING LETTERS
	public function artist_videos_get($artist_id)
	{
		$this->load->database();
		$this->db->select('*');
        $this->db->from('category');
        $this->db->order_by('name', 'ASC');
		$cats = $this->db->get();
		
		if($cats->num_rows()>0)
		{
			foreach($cats->result_array() as $cat)
			{
				$cat_name 	= 	$cat['name'];
				$cat_id 	=	$cat['id'];
				
				
				$video_query = "select videos.artist_id,videos.artist_name,videos.video_title,videos.video_youtube_id,videos.video_thumbnail from videos INNER JOIN video_category ON videos.video_id = video_category.video_id and video_category.cat_id ='".$cat_id."' where videos.artist_id = '".$artist_id."'  group by videos.video_id";
				$videos = $this->db->query($video_query);
				if($videos->num_rows()>0)
				{
					$result_array[$cat_name] = array();
					foreach($videos->result_array() as $video)
					{
						$result_array[$cat_name][] = $video;
					}
				}
			}
		}
		
		  if($result_array) {
		   $this->response($result_array, 200);
		  } else {
		   $this->response(array('error' => 'Couldn\'t find any videos from that artist!'), 404);
		  }
	}
	
	
	public function videos_all_get()
	{
		
		$this->load->database();
		$this->db->select('*');
        $this->db->from('category');
        $this->db->order_by('name', 'ASC');
		$cats = $this->db->get();
		
		if($cats->num_rows()>0)
		{
			foreach($cats->result_array() as $cat)
			{
				$cat_name 	= 	$cat['name'];
				$cat_id 	=	$cat['id'];
				
				
				$video_query = "select videos.artist_name,videos.video_title,videos.video_youtube_id,videos.video_thumbnail from videos INNER JOIN video_category ON videos.video_id = video_category.video_id and video_category.cat_id ='".$cat_id."' group by videos.video_id";
				$videos = $this->db->query($video_query);
				if($videos->num_rows()>0)
				{
					$result_array[$cat_name] = array();
					foreach($videos->result_array() as $video)
					{
						$result_array[$cat_name][] = $video;
					}
				}
			}
		}
		
		  if($result_array) {
		   $this->response($result_array, 200);
		  } else {
		   $this->response(array('error' => 'Couldn\'t find any videos from that artist!'), 404);
		  }
	
	}
	  	 
}