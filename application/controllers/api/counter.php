<?php

	require(APPPATH.'libraries/REST_Controller.php');

    class counter extends REST_Controller{
	
		public function artists_get()  
		{ 
			$this->load->database();
			$sql = "SELECT REPLACE(FORMAT(COUNT( DISTINCT artist_id ), 0), ',' , '.') AS artists FROM artists ";

			$query = $this->db->query($sql);
			$data = $query->result();
			
			if($data) {
				$this->response($data, 200); 
			} else {
				$this->response(array('error' => 'Couldn\'t count amount of artists!'), 404);
			}
		}
		
		public function albums_get()  
		{ 
			$this->load->database();
			$sql = "SELECT REPLACE( FORMAT( COUNT( DISTINCT album_id ) , 0 ) ,  ',',  '.' ) AS total_album FROM products
						INNER JOIN albums ON products.pro_id = albums.product_id";
			$query = $this->db->query($sql);
			$data = $query->result();
			
			if($data) {
				$this->response($data, 200); 
			} else {
				$this->response(array('error' => 'Couldn\'t count amount of artists!'), 404);
			}
		}

		public function videos_get()
		{
			$this->load->database();
			$sql = "SELECT REPLACE(FORMAT(COUNT( DISTINCT video_id ), 0), ',' , '.') AS total_videos FROM videos ";
			$query = $this->db->query($sql);
			$data = $query->result();
			
			if($data) {
				$this->response($data, 200); 
			} else {
				$this->response(array('error' => 'Couldn\'t count amount of artists!'), 404);
			}		
		}
  
}

?>