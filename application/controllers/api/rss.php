<?php

	require(APPPATH.'libraries/REST_Controller.php');

    class rss extends REST_Controller{
  
	// ALL ARTISTS
	public function all_rss_feeds_get()  
	{ 
		$this->load->database();
		$this->load->model('rss_model');
		
		$data = $this->rss_model->get_all_rss_feeds();
		
		if($data) {
			$this->response($data, 200); 
		} else {
			$this->response(array('error' => 'Couldn\'t find any artists!'), 404);
		}
	}
	
	public function rss_artist_get($artist_id)  
	{ 
		$this->load->database();
		$this->load->model('rss_model');
		
		$data = $this->rss_model->get_rss_artist($artist_id);
		
		if($data) {
			$this->response($data, 200); 
		} else {
			$this->response(array('error' => 'Couldn\'t find any artists!'), 404);
		}
	}
	
	public function get_rss() {

		$rss->load('http://wordpress.org/news/feed/');
		
		$feed = array();
		foreach ($rss->getElementsByTagName('item') as $node) {
			$item = array ( 
				'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
				'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
				'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
				'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
				);
			array_push($feed, $item);
			
		}

		if($feed) {
			$this->response($feed, 200); 
		} else {
			$this->response(array('error' => 'Couldn\'t find any artists!'), 404);
		}		
	}

    
	
 }
?>