<?php

/*
 *	Class : CurlSetup
 *	initialize curl session and grab data from the given URL
 *	http://curl.haxx.se/libcurl/c/curl_easy_setopt.html
 */

class CurlSetup {

	private $Handle;
	private $Output;

	//URL to fetch data
	private $strURL;

	private $bReturnTransfer = true;
	private $strUserAgent;
	private $iFreshConnect = 1;
	private $iFollowLocation = 1;

	/*
	 *	Curl Proxy related properties
	 */
	//	Available options for this are CURLPROXY_HTTP, CURLPROXY_SOCKS4, CURLPROXY_SOCKS5, CURLPROXY_SOCKS4A
	private $strProxyType = 'HTTP'; //default type

	private $bProxyTunnel = false;
	private $strProxyIP;
	private $strProxyPort;
	private $strProxyUserPass;

	//Curl Error
	public $curlError;
	
	/*
	 *	Options can be passed as $optArr to the constructor
	 *
	 *  Options Array
	 * array( 'return_transfer' => true, 'fresh_connect' => 1, 'follow_location' => 1, 'proxy_tunnel' =>true, 'proxy_type' => 'HTTP', 'proxy_ip' => '', 'proxy_port'	=> '', 'proxy_user_pass' => '' );
	 *
	 */
	function __construct($url, $optArr) {

		$this->strURL = $url;
		
		$this->strUserAgent = $_SERVER['HTTP_USER_AGENT'];

		if(!empty($optArr['return_transfer']))
			$this->bReturnTransfer = $optArr['return_transfer'];

		if(!empty($optArr['fresh_connect']))
			$this->iFreshConnect = $optArr['fresh_connect'];

		if(!empty($optArr['follow_location']))
			$this->iFollowLocation = $optArr['follow_location'];
		
		if(!empty($optArr['proxy_type']))
			$this->strProxyType = $optArr['proxy_type'];

		if(!empty($optArr['proxy_tunnel']))
			$this->bProxyTunnel = $optArr['proxy_tunnel'];
		
		if(!empty($optArr['proxy_ip']))
			$this->strProxyIP = $optArr['proxy_ip'];

		if(!empty($optArr['proxy_port']))
			$this->strProxyPort = $optArr['proxy_port'];

		if(!empty($optArr['proxy_user_pass']))
			$this->strProxyUserPass = $optArr['proxy_user_pass'];
	}
	
	/*
	 *	initialize curl session and other appropriate options
	 */
	function curlInitSession() {
		
		$this->Handle = curl_init();

		curl_setopt($this->Handle, CURLOPT_URL, $this->strURL);
		curl_setopt($this->Handle, CURLOPT_RETURNTRANSFER, $this->bReturnTransfer);
		curl_setopt($this->Handle, CURLOPT_USERAGENT, $this->strUserAgent); 
		curl_setopt($this->Handle, CURLOPT_FRESH_CONNECT, $this->iFreshConnect);
		curl_setopt($this->Handle, CURLOPT_FOLLOWLOCATION, $this->iFollowLocation);

		if($this->strProxyIP) {

			curl_setopt($this->Handle, CURLOPT_HTTPPROXYTUNNEL, $this->bProxyTunnel);
			
			$this->setCurlProxy();
			
			curl_setopt($this->Handle, CURLOPT_PROXY, $this->strProxyIP);
			
			if($this->strProxyPort) curl_setopt($this->Handle, CURLOPT_PROXYPORT, $this->strProxyPort);
			
			if($this->strProxyUserPass) curl_setopt($this->Handle, CURLOPT_PROXYUSERPWD, $this->strProxyUserPass); //username:password
		}
	}
	
	/*
	 *	grab URL and pass it to the Output
	 */
	function curlOutput() {		
		
		$this->curlInitSession();
		
		$this->Output = curl_exec($this->Handle);

		if (curl_errno($this->Handle)) { 
			$this->curlError = curl_error($this->Handle); 
		}

		return $this->Output;		
	}

	/*
	 *	set CURLOPT_PROXYTYPE option
	 */
	function setCurlProxy(){
		switch($this->strProxyType) {
			case 'HTTP' :
				curl_setopt($this->Handle, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
				break;
			case 'SOCKS4' :
				curl_setopt($this->Handle, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS4);
				break;
			case 'SOCKS5' :
				curl_setopt($this->Handle, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
				break;
			case 'SOCKS4A' :
				curl_setopt($this->Handle, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS4A);
				break;
		}
	}

	function __destruct() {
	   curl_close($this->Handle);
	}

}//CurlConnection

?>