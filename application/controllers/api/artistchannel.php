<?php

  require(APPPATH.'libraries/REST_Controller.php');

  class artistchannel extends REST_Controller{
  
	///// ARTIST NAME
	public function artistname_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT formated_name FROM artists WHERE artist_id = '".$artist_id."'";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		//$data['formated_name'] = strtolower(preg_replace("/\s+/", '', $data['formated_name']));
		
		if($data) {
			$this->response($data, 200);
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist with that name!'), 404);
		}
	}
	
	///// ARTIST NAME
	public function artist_image_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT artist_image FROM artists WHERE artist_id = '".$artist_id."'";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist image!'), 404);
		}
	}
	
	public function header_artist_image_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT folder_path FROM artists WHERE artist_id = '".$artist_id."' AND image_proccessed_flag = 1";
		$query = $this->db->query($sql);
		$data = $query->row();
		
		if($data) {
			$sql_image = "SELECT * FROM image_example_table WHERE artist_id = '".$artist_id."' ";
			$query = $this->db->query($sql_image);
			$imgs = $query->row();
			$image_name = $imgs->image_example_name;
			$image_path = base_url().$data->folder_path.'/1300x540_'.$image_name;
			$data->artist_image = $image_path;
			//print_r($data);
			
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist image!'), 404);
		}
	}

	///// ARTIST ALBUMS
	public function artist_albums_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT formated_name, products.pro_id, title, artists.artist_id, coverarttoenailurl, physicalReleaseDate, digitalReleaseDate FROM artists
     INNER JOIN artist_product ON artist_product.artist_id = artists.artist_id 
     INNER JOIN products ON products.pro_id = artist_product.product_id
     INNER JOIN albums ON albums.product_id = products.pro_id
WHERE (artists.artist_id =  '".$artist_id."' OR artists.artist_parent_id = '".$artist_id."')";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist albums!'), 404);
		}
	}
	
	///// ARTIST TRACKS
	public function artist_tracks_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT products.title AS album_title, products.genre AS album_genre, products.ccws_pro_id AS product_upc, track_title, SUBSTR(track_duration, 4) AS track_duration FROM products
					INNER JOIN track_albumn_information ON products.ccws_pro_upc = track_albumn_information.product_upc 
					AND track_albumn_information.artist_id =  '".$artist_id."'  LIMIT 0 , 30";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); 
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist albums!'), 404);
		}
	}
	
	///// ARTIST ALBUM+TRACKS
	public function artist_releases_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT products.pro_id,products.title AS album_title, products.genre AS album_genre, products.coverarttoenailurl AS cover_image, products.ccws_pro_id 
					AS product_upc, track_title, track_duration, album_id, physicalReleaseDate , digitalReleaseDate FROM products INNER JOIN track_albumn_information 
					ON products.ccws_pro_upc = track_albumn_information.product_upc AND track_albumn_information.artist_id ='".$artist_id."' ORDER BY physicalReleaseDate, digitalReleaseDate DESC";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist releases!'), 404);
		}
	}
	
	///// ARTIST VIDEOS
	public function artist_videos_get()  
	{ 
		$this->load->database();
		$sql = "";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist videos!'), 404);
		}
	}

	///// RELATED ARTIST
	public function related_artists_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT artist_image, formated_name , artists.artist_id , count(user_id) AS num_followers FROM related_artists 
					LEFT JOIN artist_followers ON related_artists.related_artist_id = artist_followers.artist_id 
					INNER JOIN artists ON related_artists.related_artist_id = artists.artist_id WHERE related_artists.artist_id= '".$artist_id."'
					GROUP BY related_artists.related_artist_id";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any related artist!'), 404);
		}
	}
	
	///// NEW ARTIST FOLLOWERS
	public function new_artist_followers_get($start_date, $end_date)  
	{ 
		$this->load->database();
		$sql = "SELECT users.img_name FROM artist_followers INNER JOIN artists ON artists.artist_id = artist_followers.artist_id INNER JOIN users ON users.user_id = artist_followers.user_id
					WHERE artist_followers.artist_id = artists.artist_id AND date_time BETWEEN '$start_date' AND '$end_date' LIMIT 20";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist followers!'), 404);
		}
	}

	///// ARTIST BIOGRAPHY
	public function artist_bio_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT artist_bio.bio FROM artist_bio INNER JOIN artists ON artists.artist_id = artist_bio.artist_id WHERE artist_bio.artist_id = '".$artist_id."'";
		$query = $this->db->query($sql);
		$data = $query->result();

		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any artist biography!'), 404);
		}
	}
	
	// VIDEOS MOST POPULAR
	public function most_popular_videos_get($user_id)  
	{ 
		$this->load->database();
		$this->load->helper('url');
		$sql = "SELECT * FROM track_albumn_information INNER JOIN user_songs ON track_albumn_information.track_alb_id = user_songs.track_alb_id
					WHERE user_songs.user_id = '$user_id' ORDER BY rate DESC LIMIT 6'";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any videos!'), 404);
		}
	}
	
	// VIDEOS NEWEST
	public function newest_videos_get($user_id)  
	{ 
		$this->load->database();
		$sql = "SELECT * FROM track_albumn_information INNER JOIN user_songs ON track_albumn_information.track_alb_id = user_songs.track_alb_id 
					WHERE user_songs.user_id = '$user_id' ORDER BY rate DESC LIMIT 6";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any videos!'), 404);
		}
	}

	// FOLLOWERS
	public function followers_get($artist_id)  
	{ 
		$this->load->database();
		$sql = "SELECT * FROM monthly_followers INNER JOIN artists ON artists.artist_id = monthly_followers.artist_id WHERE monthly_followers.artist_id = '".$artist_id."'";
		$query = $this->db->query($sql);
		$data = $query->result();
		
		if($data) {
			$this->response($data, 200); // 200 being the HTTP response code
		} else {
			$this->response(array('error' => 'Couldn\'t find any followers!'), 404);
		}
	}		

	
   	
  }
  
?>