<?php
class category extends CI_Controller {
	
	function __construct()
		{
			parent::__construct();
			$this->load->model('category_model');
			$admin_id = $this->session->userdata('id');
			if($admin_id=='')
			{
				redirect('login', 'refresh');
			}
		}
	public function index()
		{
			$data['categories'] = $this->category_model->get_all_categories();
			
			$data['contents'] = $this->load->view('cat_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}
		
	public function add()
	{
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('name', 'Category Name', 'trim|required|xss_clean');
	 
	 	if($this->form_validation->run() == FALSE)
   		{
	   		$data['contents']	= 	$this->load->view('cat_add','', true);
			$this->load->view('template',$data);
		}
		else
		{
			$data['category']=$this->category_model->add_category();
			header('Location: '.base_url().'category/index');
		}
	}
	
	public function edit($id=0)
	{
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('name', 'Category Name', 'trim|required|xss_clean');
	 
		
	 	if($this->form_validation->run() == FALSE)
   		{
			$data['categories'] = $this->category_model->get_category($id);
	   		$data['contents']	= 	$this->load->view('cat_edit',$data,true);
			$this->load->view('template',$data);
		}
		else
		{
			$data['categories']=$this->category_model->edit_category($id);
			header('Location: '.base_url().'category/index');
		}
	}
	
	public function chg_status($cat_id,$status)
	{
		$new_status= 1;
		if($status==1)
		{
			$new_status=2;
		}
		
		$update_array['status_id'] = $new_status;
		$this->db->where('id',$cat_id);
		$this->db->update('category',$update_array);
		header('Location: '.base_url().'category/index');
		
	}
	public function delete($id=0)
	{
		$this->category_model->delete($id);
		header('Location: '.base_url().'category/index');
	}
	
}
