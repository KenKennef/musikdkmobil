<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

      /**
	    * class artist_info extends the CI_Controller
		*if the user login then he access to that page 

	    */
class artist_info extends CI_Controller {

	   /**
	    * construct method load the helpers
		*if session id is null then he redirects to login page

	    */
		 public static $artist_id;
		
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url', 'html', 'array'));
		$admin_id = $this->session->userdata('id');
		if($admin_id=='')
		{
			redirect('login', 'refresh');
		}
	}
	
	   /**
	    * Index method listing the artist info
		* Index method loading the view of artist.
		*/
		
		
	public static function getTimeStamp()
	  {
		return time();
	  }
	
	  public static function getSharedSecret()
	  {
		return "tmGafaAzDW";
	  }
	
	  public static function getAPIKey()
	  {
		return "wg9tsghsr9xp2tppfxunh256";
	  }
	
	  public static function createMD5Hash()
	  {
		$string = self::getAPIKey() . self::getSharedSecret() . self::getTimeStamp();
		
		$md = md5($string);

		return $md;
	  }
	  function get_content($url)
	{
		$ch = curl_init();
	
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_HEADER, 0);
	
		ob_start();
	
		curl_exec ($ch);
		curl_close ($ch);
		$string = ob_get_contents();
	
		ob_end_clean();
	   
		return $string;    
	}
	  
	  public static function getResult()
	  {
		  
			 $search = urlencode($_POST['name']);
		
			$url = "http://api.rovicorp.com/search/v2.1/music/search?apikey=".self::getAPIKey()."&sig=".self::createMD5Hash()."&query=$search&entitytype=artist";
			
			$contents = self::get_content($url);  
			return $contents;
	  }
	  
	  public function musicbrainz($arr)
	  {
		  $html_string = '';
			$html_string .= "<h1> Music Brainz Data : </h1>";
			
			$html_string .= "<table border=1;>";
			foreach($arr as $inner_array)
				{
					if(is_array($inner_array))
					{
						foreach($inner_array as $idx=>$val)
						{
							$html_string .= "<tr>";
							$html_string .= "<td>" . $idx. "</td>". "<td>".$val."</td>"; 
							$html_string .= "</tr>";
							
							if(is_array($val))
							{
								foreach($val as $idx=>$val)
								{
									
									$html_string .= "<tr>";
									$html_string .= "<td> $idx". "</td>". "<td>". $val."</td>";
									$html_string .= "</tr>";
									
									if(is_array($val))
									{
										foreach($val as $idx=>$val)
										{
											
											$html_string .= "<tr>";
											$html_string .= "<td> $idx". "</td>". "<td>". $val."</td>";
											$html_string .= "</tr>";
											
										}
									}
									
								}
							}
							
						}
					}
						//$this->db->insert('music_brainz_url', $data_insert);
					
				}
				
			$html_string .= "<table>";
				
				
				
				
				/*$count = $arr['artist-list']['@attributes']['count'];
				
				if($count  == 1) {
					$artistArr = $arr['artist-list']['artist']; 
					
					$html_string .= "<tr>";
					foreach($artistArr as $key => $val) {
						
						if($key=='name') {
							$html_string .= "<td> $key ==>" . $val . "</td>";
							//$name = $val;	
						}
						if($key=='country') {
							$html_string .= "<td> $key ==>" . $val . "</td>";
							//$country = $val;	
					    }
						if(is_array($val) && $key == '@attributes') {
							$html_string .= "<td> $key ==>" . $val . "</td>";
							//$artist_id = $val['id'];	
						}
						
					}
					$html_string .= "</tr>";
					
				} else {
					
					//$html_string .= "<h1> Music Brainz Data : </h1>";
					
					//$artistArr = $arr['artist-list']['artist'][0]; 
					$html_string .= "<tr>";
					foreach($artistArr as $key => $val) {
						
						if($key=='name') {
							//$name = $val;	
							$html_string .= "<td> $key ==>" . $val. "</td>";
						}
						if($key=='country') {
							//$country = $val;	
							$html_string .= "<td> $key ==>" . $val. "</td>";
						}
						/*if(is_array($val) && $key == '@attributes') {
							//$artist_id = $val['id'];	
							$html_string .= "<td> $key ==>" . $val. "</td>";
						
					   }*/
					   
					/*  if(is_array($val)){
								foreach ($val as $key => $val) {
									$html_string .= "<td> $key ==>" . $val. "</td>";
								}
					  }
													 
					   
					   
					   
					   
					$html_string .= "</tr>";
					
					}*/
					  
		       
				
				// $this->artist_model->add_musicbrainz($artist_id, $country, $name);
				
				return $html_string;
	
	}
	
	
	 public function musicbrainz_url($array)
	  {
		
		
			$html_string = '';		
		    $html_string .= "<table border= 1;>";
			/*$html_string .= "<thead><th>Thumb</th><th>Artist</th><th>Main Release</th><th>Title</th><th>Role</th><th>Year</th><th>Resource Url</th>";
			$html_string .= "</thead>" ;*/
			$html_string .= "<tbody>";
		
		
		//$data_insert['artist_id'] = $artist_id;
		
		
		foreach($array as $inner_array)
				{
					$html_string .= "<tr>";
					if(is_array($inner_array))
					{
						foreach($inner_array as $idx=>$val)
						{
						
							if($idx=='name'){
								
										$html_string .= "<td> $val" . "</td>";
										
								       //$data_insert['service'] = $val;
									
						}
							
							if($idx=='url')
							{
								
								$html_string .= "<td> $val". "</td>";
								//$data_insert['service_url'] = $val;
								
							}
							
						}
					}
					
					
					$html_string .= "</tr>";
						//$this->db->insert('music_brainz_url', $data_insert);
					
				}
				
				
		$html_string .= "</tbody>";
		$html_string .= "</table>";
			     
		return $html_string;

	}
	 public function rovicorp($array)
	  {
			  $html_string = '';
			       // print_r($array);
					$html_string .= "<h1> Rovi Data : </h1>";
						
					//$data_insert['artist_id'] = $artist_id;
					$html_string .= "<table border =1;>";
					 foreach ($array as $index => $value) {
				
					       foreach ($value as $index => $value) {
							   $html_string .= "<tr>";
							   
							  $html_string .= "<td>"."$index </td>"." <td>$value" . "</td>";
							  
							  $html_string .= "</tr>" ;
								 if(is_array($value)){
											 foreach ($value as $index => $value) {
													 
														 if(is_array($value)){
														 
																 foreach ($value as $index => $value) {
														 				 if(is_array($value)){
																 
																			 foreach ($value as $index => $value) {
																				 
																		   	$html_string .= "<tr>";
														 
																			 $html_string .= "<td>"."$index</td>"."<td>$value". "</td>";
																			 
																			 $html_string .= "</tr>" ;
																			 
																	 
																					 if(is_array($value)){
																						 
																								 foreach ($value as $index => $value) {
																									 
																									 if($value!=''){
																										$html_string .= "<tr>" ;
																										 
																										$html_string .= "<td>"."$index</td>"."<td>$value"."</td>";
																										$html_string .= "</tr>";
																									
																									 }
																								 }
																					  }
																	 			
																			   }

													 
																   }
														 
														          }
													 
												           }
											 
							                  }


								 }
								
									
									/*if($index == 'name'){
										
										$data_insert['service_name'] = $value;
									
									}
									
									if(is_array($value)){
										if(!empty($value)){
											
											$data_insert['counter_type'] = $index;
										
									   	    $data_insert['counter'] = max($value);
											
											$data_insert['artist_id'] = $artist_id;
										
										   $this->db->insert('services', $data_insert);
											  
										}
										
									}*/

					        }
		
				    }
					
					$html_string .="</table>";
					
					return $html_string;
		}
					

					
					
					
					
					
					
					
			      /*  $meta_id = $array['searchResponse']['meta:id']."\n";
					$artist_name = $array['searchResponse']['results'][0]['name']['name'];
					$country = $array['searchResponse']['results'][0]['name']['country'];
					$birth_date = $array['searchResponse']['results'][0]['name']['birth']['date'];
					$birth_place = $array['searchResponse']['results'][0]['name']['birth']['place'];
					$death_date = $array['searchResponse']['results'][0]['name']['death']['date'];
					$death_place = $array['searchResponse']['results'][0]['name']['death']['place'];
					$discographyUri = $array['searchResponse']['results'][0]['name']['discographyUri'];
					$memberOfUri = $array['searchResponse']['results'][0]['name']['memberOfUri'];
					$imagesUri = $array['searchResponse']['results'][0]['name']['imagesUri'];
					$influencersUri = $array['searchResponse']['results'][0]['name']['influencersUri'];
					$similarsUri = $array['searchResponse']['results'][0]['name']['similarsUri'];
					$moodsUri = $array['searchResponse']['results'][0]['name']['moodsUri'];
					$musicBioUri = $array['searchResponse']['results'][0]['name']['musicBioUri'];
					$musicCreditsUri = $array['searchResponse']['results'][0]['name']['musicCreditsUri'];
					$songsUri = $array['searchResponse']['results'][0]['name']['songsUri'];
					$musicStylesUri = $array['searchResponse']['results'][0]['name']['musicStylesUri'];
					$associatedWithUri = $array['searchResponse']['results'][0]['name']['associatedWithUri'];
					$themesUri = $array['searchResponse']['results'][0]['name']['themesUri'];
					
					
		
					$this->artist_model->add_rovicorp($meta_id, $artist_name, $country, $birth_date, $birth_place, $death_date,$death_place, $discographyUri, $memberOfUri, $influencersUri, $similarsUri, $moodsUri, $musicBioUri, $musicCreditsUri, $songsUri, $musicStylesUri, $associatedWithUri, $themesUri, $artist_id );
	
			     


	   }
	  */
	  
	  
	  
	
	function index()
	{
		$html_string='';
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('name', 'Artist Name', 'trim|required');
	 
	 	if($this->form_validation->run() == FALSE)
   		{
	   		$data['contents']	= 	$this->load->view('artist','', true);
			$this->load->view('template',$data);
		}
		else
		{		
			
				$search = urlencode($_POST['name']);
				$html_string .= "<h1> Freebase Data : </h1>";

	"<br>".$profile = file_get_contents("https://www.googleapis.com/freebase/v1/search?query=$search");
	
	$array = json_decode($profile, true);
	
	//print_r($array);
	
	$html_string .="<table border= 1;>";
	foreach($array as $inner_array)
				{
					if(is_array($inner_array))
					{
						foreach($inner_array as $idx=>$val)
						{
							$html_string .="<tr>";
							$html_string .="<td>" . $idx. "</td>". "<td>".$val."</td>"; 
							$html_string .="</tr>";
							
							if(is_array($val))
							{
								foreach($val as $idx=>$val)
								{
									
									$html_string .="<tr>";
									$html_string .="<td> $idx". "</td>". "<td>". $val."</td>";
									$html_string .="</tr>";
									
									if(is_array($val))
									{
										foreach($val as $idx=>$val)
										{
											
											$html_string .="<tr>";
											$html_string .="<td> $idx". "</td>". "<td>". $val."</td>";
											$html_string .="</tr>";
											
										}
									}
									
								}
							}
							
						}
					}
					
					
				}
	
	
	
		$html_string .="</table>";

				
				
				"<br>".$Search = @file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/artists/search.xml?q=$search");
				if($Search!=''){
				$xml = simplexml_load_string($Search);
				$json = json_encode($xml);
				$array = json_decode($json, true);
				
				
				$profile_id = urlencode($array['data']['artists']['artist']['@attributes']['id']);
				
				$context = array(
					'http'=>array('max_redirects' => 99)
				);
				
				$context = stream_context_create($context);
				
				$data = file_get_contents("http://www.musicbrainz.org/ws/2/artist?query=$search" , false, $context); 
				
				$xml = new SimpleXMLElement($data);
				
				$json = json_encode($xml);
				$array = json_decode($json, true);
				
				$id = urlencode($array['artist-list']['artist']['0']['@attributes']['id']);
				$name = urlencode($array['artist-list']['artist']['0']['name']);
				
				"<br>".$Search = file_get_contents("http://www.musicbrainz.org/ws/2/artist/?query=artist:\"$name\"");
				$xml = simplexml_load_string($Search);
				
				$json = json_encode($xml);
				$array = json_decode($json, true);
				
				$html_string .= self::musicbrainz($array);  
				
				}
				
				//print_r($array);
				//exit;
				/*$html_string .="<br> Artist Id:   ".$array['artist-list']['artist']['@attributes']['id'];
				$html_string .="<br> Name :   ".$array['artist-list']['artist']['name'];
				$html_string .="<br> Gender :   ".$array['artist-list']['artist']['gender'];
				$html_string .="<br> Country :   ".$array['artist-list']['artist']['country'];
				$html_string .="<br> Artist Area :   ".$array['artist-list']['artist']['area']['name'];
				$html_string .="<br>";*/
			

				/*$context = array(
					'http'=>array('max_redirects' => 99)
				);
				$context = stream_context_create($context);
				
				"<br>".$Search = file_get_contents("http://www.musicbrainz.org/ws/2/release-group/?query=release:\"$name\"", false, $context); 
				$xml = simplexml_load_string($Search);
				$json = json_encode($xml);
				$array = json_decode($json, true);
				//print_r($array);
				//exit;

		
			for($i=0 ; $i < $array['release-group-list']['@attributes']['count'] ; $i++){
					$html_string .="<br> release group id   :     ".$array['release-group-list']['release-group']['@attributes']['id'] ;
					$html_string .="<br> release group type :     ".$array['release-group-list']['release-group']['@attributes']['type'] ;
					$html_string .="<br> title :     ".$array['release-group-list']['release-group']['title'] ;
					$html_string .="<br> primary type :     ".$array['release-group-list']['release-group']['primary-type'] ;
					//$html_string .="<br> artist id :     ".$array['release-group-list']['release-group']['artist-credit']['name-credit']['artist']['@attributes']['id'] ;
					$html_string .="<br> artist name :     ".$array['release-group-list']['release-group']['artist-credit']['name-credit']['artist']['name'] ;
					$html_string .="<br> sort name :     ".$array['release-group-list']['release-group']['artist-credit']['name-credit']['artist']['sort-name'] ;
					
					$count2 = 0;
					
					//$html_string .=$array['release-group-list']['release-group']['release-list']['@attributes']['count'];
					
					for($j = 0 ; $j < $array['release-group-list']['release-group']['release-list']['@attributes']['count']; $j++ ){
						
						$html_string .="<br> release id :     ".$array['release-group-list']['release-group']['release-list']['release'][$count2]['@attributes']['id'] ;
						$html_string .="<br> release title :     ".$array['release-group-list']['release-group']['release-list']['release'][$count2]['title'] ;
						$html_string .="<br> release title :     ".$array['release-group-list']['release-group']['release-list']['release'][$count2]['status'] ;
						$count2 ++ ;
						
						
						}
				
				}*/
				
				
				
				//$artist_id = $this->db->insert_id();
					
				"<br>".$profile = @file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/profiles/artist/$profile_id.json");
				$array = json_decode($profile, true);
				$html_string .= self::musicbrainz_url($array);
				//exit;
				
				
				/*foreach($array as $inner_array)
				{
					if(is_array($inner_array))
					{
						foreach($inner_array as $idx=>$val)
						{
						
							
							if($idx=='name')
							{
								$html_string .= "<br>".'<b>'.$val.':</b> ';
								
							}
							if($idx=='url')
							{
								$url[] =  $val;
								$html_string .="<br>";
							}
						}
					}
				}
				
					print_r($url);
					
					$facebook = $url[0];
					$twitter = $url[1];
					$wikipedia = $url[2];
					$youTube = $url[3];
					$vevo = $url[4];
					print_r($facebook);
					exit;*/
					
					
			
					
					

			"<br>".$profile = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/metrics/profile/$profile_id.xml");

			$xml = simplexml_load_string($profile);
			$json = json_encode($xml);
			$array = json_decode($json, true);
			
			$profile_id = urlencode($array['data']['criteria']['profile_id']);
			
			
			//$file = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/artists/view/$profile_id.json");
			
			//$file1 = json_decode($file, true);
			
			/*$html_string .="<br>"."Name :".$file1['name'];
			
			$html_string .="<br>"."Music_Brainz_id :".$file1['music_brainz_id'];
			
			$html_string .="<br>"."is_verified :".$file1['is_verified'];
			*/
			
			//$html_string .="<br>".$artist = file_get_contents('http://copenhagenbeta.api3.nextbigsound.com/metrics/artist/356.xml');
			
			// "<br>".$Genres = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/genres/artist/$profile_id.xml");
			//$xml = simplexml_load_string($Genres);
			//$json = json_encode($xml);
			//$array = json_decode($json, true);
			//var_dump($array);
			/*$html_string .="<br>"."Status :".$array['status'];
			$html_string .="<br>"."Message :".$array['message'];
			$html_string .="<br>"."Name :".$array['data']['artist']['name'];*/
			
			 "<br>".$Genres = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/metrics/artist/$profile_id.json");
			  
			  $array = json_decode($Genres, true);
			 // print_r($array);

			 /*print_r($artist_id);
			 exit;*/
				
			$html_string .="<h1> Next Bigsound Data : </h1>";
			
		    foreach ($array as $index => $value) {
				$html_string .="<table border=1;>";
					 foreach ($value as $index => $value) {
							
							 foreach ($value as $index => $value) {
								 	$html_string .="<tr>";
									if($index == 'name'){
										
										//$data_insert['service_name'] = $value;
										
										$html_string .=$value . " : ";
									
									}
									
									if($index == 'url'){
										
										//$data_insert['service_name'] = $value;
										
										echo "<br>";
										
										$html_string .="    {   ".$value."    }   ";
									
									}
									
									if(is_array($value)){
										if(!empty($value)){
											
											$html_string .="<td> $index". "</td>" . "<td>".max($value)."</td>";
											//$data_insert['counter_type'] = $index;
										
									   	   // $data_insert['counter'] = max($value);
											
											//$data_insert['artist_id'] = $artist_id;
										
										   //$this->db->insert('services', $data_insert);
											  
										}
										
									}
									
									$html_string .="</tr>";

						}
		
				}
				$html_string .="</table>" ;
		}
		
			
							
					$info =  artist_info::getResult();	
					$array = json_decode($info, true);
					$id = $array['searchResponse']['results'][0];
					$id = $array['searchResponse']['results'][0]['name']['musicGenres'][0]['id'];

					$url = "http://api.rovicorp.com/search/v2.1/music/search?apikey=".self::getAPIKey()."&sig=".self::createMD5Hash()."&query=$search&entitytype=artist&filter=genreid:$id&size=1";
		
					$info = self::get_content($url);  
					$array = json_decode($info, true);
					$html_string .= self::rovicorp($array);
					
					//print_r($array);
					
					/*$html_string .="\n"."<br><b>"."Meta:id </b> ==>".$array['searchResponse']['meta:id']."\n";
					$html_string .="<br><b>"."Artist Name </b> ==>".$array['searchResponse']['results'][0]['name']['name']."\n";
					$html_string .="<br><b>"."Country </b> ==>".$array['searchResponse']['results'][0]['name']['country']."\n";
					$html_string .="<br><b>"."Birth Date </b> ==>".$array['searchResponse']['results'][0]['name']['birth']['date']."\n";
					$html_string .="<br><b>"."Birth Place </b> ==>".$array['searchResponse']['results'][0]['name']['birth']['place']."\n";
					$html_string .="<br><b>"."Death Date </b> ==>".$array['searchResponse']['results'][0]['name']['death']['date']."\n";
					$html_string .="<br><b>"."Death Place </b> ==>".$array['searchResponse']['results'][0]['name']['death']['place']."\n";
					$html_string .="<br><b>"."Discography Uri </b> ==>".$array['searchResponse']['results'][0]['name']['discographyUri']."\n";
					$html_string .="<br><b>"."Member Of Uri </b> ==>".$array['searchResponse']['results'][0]['name']['memberOfUri']."\n";
					$html_string .="<br><b>"."Images Uri </b> ==>".$array['searchResponse']['results'][0]['name']['imagesUri']."\n";
					$html_string .="<br><b>"."Influencers Uri </b> ==>".$array['searchResponse']['results'][0]['name']['influencersUri']."\n";
					$html_string .="<br><b>"."Similars Uri </b> ==>".$array['searchResponse']['results'][0]['name']['similarsUri']."\n";
					$html_string .="<br><b>"."Moods Uri </b> ==>".$array['searchResponse']['results'][0]['name']['moodsUri']."\n";
					$html_string .="<br><b>"."MusicBio Uri </b> ==>".$array['searchResponse']['results'][0]['name']['musicBioUri']."\n";
					$html_string .="<br><b>"."Music Credits Uri </b> ==>".$array['searchResponse']['results'][0]['name']['musicCreditsUri']."\n";
					$html_string .="<br><b>"."Songs Uri </b> ==>".$array['searchResponse']['results'][0]['name']['songsUri']."\n";
					$html_string .="<br><b>"."Music Styles Uri </b> ==>".$array['searchResponse']['results'][0]['name']['musicStylesUri']."\n";
					$html_string .="<br><b>"."Associated With Uri </b> ==>".$array['searchResponse']['results'][0]['name']['associatedWithUri']."\n";
					$html_string .="<br><b>"."Themes Uri </b> ==>".$array['searchResponse']['results'][0]['name']['themesUri']."\n";*/
					
					$html_string .="<h1> Discogs Data : </h1>";

			"<br>".$profile = @file_get_contents("http://api.discogs.com/artist/$search");
			if($profile!='')
			{
			$array = json_decode($profile, true);
				
			$id = $array['resp']['artist']['id'];
		
			"<br>".$profile = file_get_contents("http://api.discogs.com/artists/$id/releases");
			$array = json_decode($profile, true);
			
			//print_r($array);
		
			$html_string .="<table border= 1;>";
			$html_string .="<thead><th>Thumb</th><th>Artist</th><th>Main Release</th><th>Title</th><th>Role</th><th>Year</th><th>Resource Url</th>";
			$html_string .="</thead>" ;
			$html_string .="<tbody>";
		
				if(!empty($array)){
					if(is_array($array)){
							 foreach ($array as $index => $value) {
								 if(is_array($value)){
											 foreach ($value as $index => $value) {
												  $html_string .="<tr>";
												 if(is_array($value)){
														
														 foreach ($value as $index => $value) {
															 
															if($index == 'thumb'){
																 $html_string .="<td>";
																 $html_string .="<img src=\"$value\">";
																 $html_string .="</td>";
																
															} 
																
															
															if($index=='artist'){
																$html_string .="<td>";
																$html_string .="$value";
																$html_string .="</td>";
															}
															
															if($index=='main_release'){
																$html_string .="<td>";
																$html_string .="$value";
																$html_string .="</td>";
															}
															if($index=='title'){
																$html_string .="<td>";
																$html_string .="$value";
																$html_string .="</td>";
															}
															 
															if($index=='role'){
																$html_string .="<td>";
																$html_string .="$value";
																$html_string .="</td>";
															}
															
															if($index=='year'){
																$html_string .="<td>";
																$html_string .="$value";
																$html_string .="</td>";
															}
															if($index=='resource_url'){
																$html_string .="<td>";
																$html_string .="$value";
																$html_string .="</td>";
															}
							
														}
												
												 }
								
												 $html_string .="</tr>";
										}
										
								 }
							}
				
					}
			}
			else{
				
				$html_string .="no data available" ;
				}
				
		
		
		   $html_string .="</tbody>";
		   $html_string .="</table>";
		
		
			}
			else
			{
				$html_string .="no data available" ;
			}
							
					
					
					
	
					
					
					
		}
		
			$data_inner['html_string'] = $html_string;
		    $data_inner['contents'] = $this->load->view('artist_detail', $data_inner, true);
			$this->load->view('template', $data_inner);
			
	
	}
}

/* End of file artist_info.php */
/* Location: ./application/controllers/artist_info.php */





