<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 		/**
	    * class youtube extends the CI_Controller
		*if the user login then he access to that page 

	    */
class Youtube extends CI_Controller {

	   /**
	    * construct method load the helpers
		*if session id is null then he redirects to login page

	    */
	
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('filter_model');
		$admin_id = $this->session->userdata('id');
		if($admin_id=='')
		{
			redirect('login', 'refresh');
		}
	}
	
	   /**
		* Index method loading the view of answer_listing.
		*/
	
	public function index()
	{
		$data['contents'] = $this->load->view('youtube_view','', true);
			
		$this->load->view('template',$data);
	}
	
	   /**
		* search method loading the view of youtube_view_search.
		*/
	
	function search()
	{
		$data['contents'] = $this->load->view('youtube_view_search','', true);
			
		$this->load->view('template',$data);
	}
	
	    /**
		* import method loading the view of youtube_view_import.
		*/
	function import()
	{
		$data['contents'] = $this->load->view('youtube_view_import','', true);
			
		$this->load->view('template',$data);
	}
	
	    /**
		* grab_videos_byid method loading the view of import_by_id.
		*/
	
	function grab_videos_byid()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('listid', 'ID', 'trim|required');
		$this->form_validation->set_rules('artist_name', 'Artist Name', 'trim|required');
		 
		if($this->form_validation->run() == FALSE)
		{
			$data['contents']	= $this->load->view('import_by_id','', true);
			$this->load->view('template',$data);
		}
		else
		{
			//$data['playlist_id'] = $this->input->post('listid');
			$data['artist_name'] = $this->input->post('artist_name');
			$data['type']	= $this->input->post('type_id');
			if($data['type'] == 'user')
			{
				$url = $this->input->post('listid');
				$array = explode('user/',$url);
				if(count($array)>=2)
				{
					$list_part = $array[1];
					$and_pure = explode('&',$list_part);
					$slash_pure = explode('/',$and_pure[0]);
					$playlist_id = $slash_pure[0];
					$data['playlist_id'] = $playlist_id;
				}
				else
				{
					redirect('youtube/grab_videos_byid');
				}
				
				$data['contents']	= $this->load->view('import_list_by_user',$data, true);
				$this->load->view('template',$data);
				}
			else
			{
					
				$url = $this->input->post('listid');
				$array = explode('list=',$url);
				if(count($array)>=2)
				{
					$list_part = $array[1];
					$and_pure = explode('&',$list_part);
					$slash_pure = explode('/',$and_pure[0]);
					$playlist_id = $slash_pure[0];
					$data['playlist_id'] = $playlist_id;
				}
				else
				{
					redirect('youtube/grab_videos_byid');
				}
				
				$data['contents']	= $this->load->view('import_by_list_id_te',$data, true);
				$this->load->view('template',$data);
			}
			}
	 }

}