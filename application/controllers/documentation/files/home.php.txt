<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	 /**
	    * class home extends the CI_Controller
	    */

class home extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   
 }


       /**
	    * if user is authenticate then the index method load the view of template
	    */

 function index()
 {
   if($this->session->userdata('id'))
   {
	   			//$this->load->model('pro_model');
				//$data['query']= $this->pro_model->fetch_record();
     			
				$data['contents'] = '';//$session_data;
     			$this->load->view('template',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
  
 }
}


/* End of file home.php */
/* Location: ./application/controllers/home.php */
		

?>

