<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

       /**
	    * class artist_info extends the CI_Controller
		*if the user login then he access to that page 

	    */
class artist_info extends CI_Controller {

	   /**
	    * construct method load the helpers
		*if session id is null then he redirects to login page

	    */
	
	function __construct()
	{
		parent::__construct();
		$admin_id = $this->session->userdata('id');
		if($admin_id=='')
		{
			redirect('login', 'refresh');
		}
	}
	
	   /**
	    * Index method listing the artist info
		* Index method loading the view of artist.
		*/
	
	function index()
	{
		
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('name', 'Artist Name', 'trim|required');
	 
	 	if($this->form_validation->run() == FALSE)
   		{
	   		$data['contents']	= 	$this->load->view('artist','', true);
			$this->load->view('template',$data);
		}
		else
		{
			
				$search = urlencode($_POST['name']);
				"<br>".$Search = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/artists/search.xml?q=$search");
				$xml = simplexml_load_string($Search);
				$json = json_encode($xml);
				//echo "$json";
				$array = json_decode($json, true);
				//print_r ($array);
				$profile_id = $array['data']['artists']['artist']['@attributes']['id'];
				
				//exit;
				
				echo "<br> Status:   ".$array['status'];
				echo "<br> Message:   ".$array['message'];
				//$data['music_brainz_id'] =  $array['data']['artists']['artist']['0']['music_brainz_id'];
				
				$context = array(
					'http'=>array('max_redirects' => 99)
				);
				
				$context = stream_context_create($context);
				
				$data = file_get_contents("http://www.musicbrainz.org/ws/2/artist?query=$search" , false, $context); 
				
				$xml = new SimpleXMLElement($data);
				
				$json = json_encode($xml);
				$array = json_decode($json, true);
				
				
				$id = urlencode($array['artist-list']['artist']['0']['@attributes']['id']); // getting id and then send again for further information
				$name = urlencode($array['artist-list']['artist']['0']['name']);
				
				//print_r($name);
	
				
				"<br>".$Search = file_get_contents("http://www.musicbrainz.org/ws/2/artist/?query=artist:\"$name\"");
				$xml = simplexml_load_string($Search);
				
				$json = json_encode($xml);
				$array = json_decode($json, true);
				//print_r($array);
				//exit;
				echo "<br> Artist Id:   ".$array['artist-list']['artist']['@attributes']['id'];
				echo "<br> Name :   ".$array['artist-list']['artist']['name'];
				echo "<br> Gender :   ".$array['artist-list']['artist']['gender'];
				echo "<br> Country :   ".$array['artist-list']['artist']['country'];
				echo "<br> Artist Area :   ".$array['artist-list']['artist']['area']['name'];
				echo "<br>";
			

				$context = array(
					'http'=>array('max_redirects' => 99)
				);
				$context = stream_context_create($context);
				
				"<br>".$Search = file_get_contents("http://www.musicbrainz.org/ws/2/release-group/?query=release:\"$name\"", false, $context); 
				$xml = simplexml_load_string($Search);
				$json = json_encode($xml);
				$array = json_decode($json, true);
//print_r($array);

		
			for($i=0 ; $i < $array['release-group-list']['@attributes']['count'] ; $i++){
					echo "<br> release group id   :     ".$array['release-group-list']['release-group']['@attributes']['id'] ;
					echo "<br> release group type :     ".$array['release-group-list']['release-group']['@attributes']['type'] ;
					echo "<br> title :     ".$array['release-group-list']['release-group']['title'] ;
					echo "<br> primary type :     ".$array['release-group-list']['release-group']['primary-type'] ;
					//echo "<br> artist id :     ".$array['release-group-list']['release-group']['artist-credit']['name-credit']['artist']['@attributes']['id'] ;
					echo "<br> artist name :     ".$array['release-group-list']['release-group']['artist-credit']['name-credit']['artist']['name'] ;
					echo "<br> sort name :     ".$array['release-group-list']['release-group']['artist-credit']['name-credit']['artist']['sort-name'] ;
					
					$count2 = 0;
					
					//echo $array['release-group-list']['release-group']['release-list']['@attributes']['count'];
					
					for($j = 0 ; $j < $array['release-group-list']['release-group']['release-list']['@attributes']['count']; $j++ ){
						
						echo "<br> release id :     ".$array['release-group-list']['release-group']['release-list']['release'][$count2]['@attributes']['id'] ;
						echo "<br> release title :     ".$array['release-group-list']['release-group']['release-list']['release'][$count2]['title'] ;
						echo "<br> release title :     ".$array['release-group-list']['release-group']['release-list']['release'][$count2]['status'] ;
						$count2 ++ ;
						
						
						}
				
				}
				
					
				"<br>".$profile = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/profiles/artist/$profile_id.json");
				$array = json_decode($profile, true);
				
				
				foreach($array as $inner_array)
				{
					if(is_array($inner_array))
					{
						foreach($inner_array as $idx=>$val)
						{
							if($idx=='name')
							{
								echo  "<br>".'<b>'.$val.':</b> ';
								
							}
							if($idx=='url')
							{
								echo  $val;
								echo "<br>";
							}
						}
					}
				}
				


			"<br>".$profile = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/metrics/profile/$profile_id.xml");
			
			//var_dump($profile);
			
			$xml = simplexml_load_string($profile);
			$json = json_encode($xml);
			$array = json_decode($json, true);
			//var_dump($array);
			
			$profile_id = $array['data']['criteria']['profile_id'];
			
			
			//$file = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/artists/view/$profile_id.json");
			
			//$file1 = json_decode($file, true);
			
			/*echo "<br>"."Name :".$file1['name'];
			
			echo "<br>"."Music_Brainz_id :".$file1['music_brainz_id'];
			
			echo "<br>"."is_verified :".$file1['is_verified'];
			*/
			
			//echo "<br>".$artist = file_get_contents('http://copenhagenbeta.api3.nextbigsound.com/metrics/artist/356.xml');
			
			 "<br>".$Genres = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/genres/artist/$profile_id.xml");
			$xml = simplexml_load_string($Genres);
			$json = json_encode($xml);
			$array = json_decode($json, true);
			//var_dump($array);
			/*echo "<br>"."Status :".$array['status'];
			echo "<br>"."Message :".$array['message'];
			echo "<br>"."Name :".$array['data']['artist']['name'];*/
			
			 "<br>".$Genres = file_get_contents("http://copenhagenbeta.api3.nextbigsound.com/metrics/artist/$profile_id.json");
			/*$xml = simplexml_load_string($Genres);
			$json = json_encode($xml);*/
			$array = json_decode($Genres, true);
			//print_r($array);
				
			if(is_array($array))   {
		    foreach ($array as $index => $value) {
				
				if(is_array($value)){
					 foreach ($value as $index => $value) {
						
						if(is_array($value)){
							
							 foreach ($value as $index => $value) {
									echo "<br>"."$index ==> $value";
									
									if(is_array($value)){
										 foreach ($value as $index=>$value) {
											echo "<br>"."$index ==> $value";
										 }
										
									}
							 
							 }
						
						}
					 }
					
				}
		}
	}


			
			
			
			
			
			
							
							

					$data['contents']	= 	$this->load->view('artist_detail');
					//$this->load->view('template',$data);
		}
	
	}
}

/* End of file artist_info.php */
/* Location: ./application/controllers/artist_info.php */
?>


