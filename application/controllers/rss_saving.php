<?php

class Rss_saving extends CI_Controller{
 
	public function save_RSS_urls()  
	{ 
		$this->load->database();
		
		$this->load->model('rss_model');
			
		$rss_url_array = array();
		
		$rss_url_array['soundvenue'] 		= 'http://soundvenue.com/category/musik/feed';
		$rss_url_array['ekstrabladet'] 		= 'http://ekstrabladet.dk/rss2/?mode=normal&submode=musik';
		$rss_url_array['bt'] 				= 'http://www.bt.dk/musik/seneste/rss';
		$rss_url_array['gaffa'] 			= 'http://gaffa.dk/feeds/nyheder';
		$rss_url_array['bdk'] 				= 'http://www.b.dk/feeds/rss/Musik';
		$rss_url_array['mh']					='http://feeds.feedburner.com/metalhammer/main';
		
		foreach($rss_url_array as $url_ind=>$url)
		{
			$contents = file_get_contents($url);

			$this->load->library('Xml2array',NULL,'myXml2array');
			$array = $this->myXml2array->createarray($contents);
			$news_items = $array['rss']['channel']['item'];
			
			
			foreach($news_items as $ind=>$news)
			{
				$insert_array['news_title']					= $news['title'];
				$pubDate 												= $news['pubDate'];
				$insert_array['news_pubdate'] 			= strtotime($pubDate);
				if($url_ind!='soundvenue')
				{
					$insert_array['news_description'] 		= strip_tags($news['description']);
				}
				else
				{
					$insert_array['news_description'] 		= strip_tags($news['description']['@cdata']) . strip_tags($news['content:encoded']['@cdata']);
				}
					$insert_array['news_link']	 				= trim($news['link']);
				
				$this->rss_model->insert_news($insert_array);
			}
			
		}
		
		
	}
 }