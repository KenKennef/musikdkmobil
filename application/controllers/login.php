<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('login_model');
   }

 function index()
 {
	   $this->load->library('form_validation');
	   $this->form_validation->set_rules('user_email', 'user_email', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('user_password', 'user_password', 'trim|required|xss_clean|callback_check_database');
	
	   if($this->form_validation->run() == FALSE)
	   {
		 //Field validation failed.  User redirected to login page
		 $this->load->view('login');
	   }
	   else
	   {
		 //Go to private area
		 redirect('home', 'refresh');
	   }
 }
 
 function check_database($user_password)
 {
	   //Field validation succeeded.  Validate against database
	   $user_email = $this->input->post('user_email');
	
	   //query the database
	   $result = $this->login_model->login($user_email, $user_password);
	   
	  /* print_r($result);
	   exit;
	*/
	
	   if($result->num_rows()>0)
	   {
		 foreach($result->result_object() as $row)
		 { 
		   
		   $this->session->set_userdata('id' , $row->user_id);
		   $this->session->set_userdata('email' , $row->user_email);
		   
		 }
		 return TRUE;
	   }
	   else
	   {
		 $this->form_validation->set_message('check_database', 'Invalid username or password');
		 return FALSE;
	   }
 }
 
		  function logout()
		 {
		   $this->session->unset_userdata('id');
		   $this->session->unset_userdata('email');
		   
		   redirect('login', 'refresh');
		 }
		 
		 public function forget()
		{
				if (isset($_GET['info'])) {
					$data['info'] = $_GET['info'];
				}
				if (isset($_GET['error'])) {
					$data['error'] = $_GET['error'];
				}
						 
				$data['contents']=$this->load->view('login-forget');
				//$this->load->view('template',$data);
				
		} 
		public function doforget()
		{
				$this->load->helper('url');
				$email= $_POST['email'];
				$q = $this->db->query("select * from users where user_email='" . $email . "'");
				if ($q->num_rows > 0) {
					$r = $q->result();
					$user=$r[0];
					$this->resetpassword($user);
					$info= "Password has been reset and has been sent to email id: ". $email;
					//redirect('/index.php/login/forget?info=' . $info, 'refresh');
					//header('Location: '.base_url().'index.php/login/forget?info='. $info, 'refresh' );
					
				}
				$error= "The email id you entered not found on our database ";
				//redirect('/index.php/login/forget?error=' . $error, 'refresh');
				//header('Location: '.base_url().'index.php/login/forget?error='. $error, 'refresh');
     
		} 

	    private function resetpassword($user)
		{ 
		
			/*print_r($user);
			exit;*/
			
			date_default_timezone_set('GMT');
			$this->load->helper('string');
			$password= random_string('alnum', 16);
			$this->db->where('user_id', $user->user_id);
			$this->db->update('users',array('user_password'=>MD5($password)));
			$this->load->library('email');
			$this->email->from('cantreply@youdomain.com', 'Your name');
			$this->email->to($user->user_email);   
			$this->email->subject('Password reset');
			$this->email->message('You have requested the new password, Here is you new password:'. $password);  
			$this->email->send();
			echo $this->email->print_debugger();
		} 
}

?>