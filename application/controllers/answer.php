<?php
class answer extends CI_Controller {
	
	function __construct()
		{
			parent::__construct();
			$this->load->helper(array('form', 'url', 'html', 'array'));
			$this->load->model('answer_model');
			$admin_id = $this->session->userdata('id');
			if($admin_id=='')
			{
				redirect('login', 'refresh');
			}
		}
	public function index($q_id = 0, $c_id = 0)
		{
			$data['question_id'] = $q_id;
			$data['contest_id'] = $c_id;
			$data['artists'] = $this->answer_model->get_all_artist();
			$data['questions'] = $this->answer_model->get_question($q_id);
			$data['answers'] =  $this->answer_model->get_all_answers($q_id);
			$data['contents'] = $this->load->view('answer_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}
		
	public function add($q_id = 0,$c_id = 0){
		
		$question_id = $q_id ;
		$contest_id = $c_id ;
		
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('answer', 'answer', 'trim|required|xss_clean');
		$this->form_validation->set_rules('artist_id', 'Artist Id', 'trim|required|xss_clean');
		
			if($this->form_validation->run() == FALSE)
			{
				$data['contest_id'] = $contest_id ;
				$data['artists'] = $this->answer_model->get_all_artist();
				/*print_r($data);
				exit;
				*/
				$data['contents'] = $this->load->view('answer_add',$data, true);
				
				$this->load->view('template',$data);
			}
			else
			{
				$data['contents']=$this->answer_model->add_answer($question_id , $contest_id);
				header('Location: '.base_url()."answer/index/$question_id/$contest_id");
			}
		}
	public function edit($id = 0, $q_id = 0, $c_id = 0 ){
			$answer_id = $id;
			$question_id = $q_id ;
			$contest_id = $c_id ;
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('answer', 'answer', 'trim|required|xss_clean');
			$this->form_validation->set_rules('artist_id', 'Artist', 'trim|required|xss_clean');

			if ($this->form_validation->run() == FALSE) {
				$data['artists'] = $this->answer_model->get_all_artist();
				$data['answers'] = $this->answer_model->get_answer($id);
				$data['contents'] = $this->load->view('answer_edit', $data, true);
				$this->load->view('template', $data);
			} else {
				
				$data['contents'] = $this->answer_model->edit_answer($id);
				header('Location: ' . base_url() . "index.php/answer/index/$question_id/$contest_id");
			}
		
		}
		
		 public function delete($id=0 , $q_id = 0, $c_id = 0 ) {
			$answer_id = $id;
			$question_id = $q_id ;
			$contest_id = $c_id ;
			$this->answer_model->delete($answer_id);
			header('Location: ' . base_url() . "index.php/answer/index/$question_id/$contest_id");
    	}
		
}
		
		
?>