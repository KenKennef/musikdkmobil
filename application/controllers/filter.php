<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class filter extends CI_Controller {
	function __construct()
		{
			parent::__construct();
			$this->load->model('filter_model');
			$admin_id = $this->session->userdata('id');
			if($admin_id=='')
			{
				redirect('login', 'refresh');
			}
		}
	public function index($cat_id=0)
	{
		$data['cat_id'] 	= $cat_id;
		$data['filters'] 	= $this->filter_model->get_all_filters($cat_id);
		$data['contents']	= $this->load->view('filter_listing',$data,true);
		$this->load->view('template',$data);
	}
		
	public function add($cat_id)
	{
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('name', 'Filter Name', 'trim|required|xss_clean');
	 
	 	if($this->form_validation->run() == FALSE)
   		{
			$data['cat_id'] 	= $cat_id;
	   		$data['contents']	= $this->load->view('filter_add','',true);
			$this->load->view('template',$data);
		}
		else
		{
			
			$data['filter']=$this->filter_model->add_filter($cat_id);
			header('Location: '.base_url().'index.php/filter/index/'.$cat_id);
		}
	}
	
	public function edit($id,$cat_id)
	{
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('name', 'Filter Name', 'trim|required|xss_clean');
	 
	 	if($this->form_validation->run() == FALSE)
   		{
			$data['filters'] = $this->filter_model->get_filter($id);
	   		$data['contents']	= $this->load->view('filter_edit',$data,true);
			$this->load->view('template',$data);
		}
		else
		{
			$data['filters']=$this->filter_model->edit_filter($id);
			header('Location: '.base_url().'filter/index/'.$cat_id);
		}
	}
	public function delete($id,$cat_id)
	{
		$this->filter_model->delete($id);
		header('Location: '.base_url().'filter/index/'.$cat_id);
	}
	
}