<?php
class question extends CI_Controller {
	
	function __construct()
		{
			parent::__construct();
			$this->load->helper(array('form', 'url', 'html', 'array'));
			$this->load->model('question_model');
			$admin_id = $this->session->userdata('id');
			if($admin_id=='')
			{
				redirect('login', 'refresh');
			}
		}
	public function index($contest_id = 0)
		{
			$data['contest_id'] = $contest_id;
			/*print_r($data['contest_id']);
			exit;*/
			$data['contests'] = $this->question_model->get_all_contest();
			$data['questions'] = $this->question_model->get_all_question($contest_id);
			$data['contents'] = $this->load->view('question_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}
		
	public function add($contest_id = 0){
		
	 	$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
	 	$this->form_validation->set_rules('question', 'Question', 'trim|required|xss_clean');
		
			if($this->form_validation->run() == FALSE)
			{
				$data['contest_id'] = $contest_id ;
				$data['contents'] = $this->load->view('question_add','$data', true);
				
				$this->load->view('template',$data);
			}
			else
			{
				$data['contents']=$this->question_model->add_question($contest_id);
				header('Location: '.base_url()."question/index/$contest_id");
			}
		}
	public function edit($id = 0 , $contest_id = 0){
		
			$question_id = $id;
			$contest_id = $contest_id;
		
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
			$this->form_validation->set_rules('question', 'Question', 'trim|required|xss_clean');

			if ($this->form_validation->run() == FALSE) {
				$data['questions'] = $this->question_model->get_questions($question_id);
				$data['contents'] = $this->load->view('question_edit', $data, true);
				$this->load->view('template', $data);
			} else {
				$data['questions'] = $this->question_model->edit_question($question_id);
				header('Location: ' . base_url() . "index.php/question/index/$contest_id");
			}
		
		}
		
		 public function delete($id=0 , $contest_id = 0) {
			$question_id = $id;
			$contest_id = $contest_id;
			$this->question_model->delete($id);
			header('Location: ' . base_url() . "index.php/question/index/$contest_id");
    	}
		
}
		
		
?>