<?php
class signup extends CI_Controller {
	
	function __construct()
		{
			parent::__construct();
			$this->load->model('user_model');
		}
		
	public function index(){
		
		$this->load->library('form_validation');
	 	$this->form_validation->set_rules('user_first_name', 'User Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_sur_name', 'Sur Name', 'trim|required|xss_clean');
	 	$this->form_validation->set_rules('user_email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_password', 'Password', 'trim|required|xss_clean');
		
	 	if($this->form_validation->run() == FALSE)
   		{
	   		$data['contents']	= 	$this->load->view('signup');
			//$this->load->view('template',$data);
		}
		else
		{
			$data['category']=$this->user_model->add_user();
			header('Location: '.base_url().'login/index');
		}
		
		
		
		}
	
}
?>