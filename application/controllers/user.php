<?php
class user extends CI_Controller {
	
	function __construct()
		{
			parent::__construct();
			$this->load->model('user_model');
			$this->load->helper(array('form', 'url', 'html', 'array'));
			$admin_id = $this->session->userdata('id');
			if($admin_id=='')
			{
				redirect('login', 'refresh');
			}
		}
	public function index()
		{
			$data['users'] = $this->user_model->get_all_users();
			
			$data['contents'] = $this->load->view('user_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}
		
	public function add()
	{
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('user_first_name', 'User Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_sur_name', 'Sur Name', 'trim|required|xss_clean');
	 	$this->form_validation->set_rules('user_email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_birthday', 'Birthday', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_address', 'Address', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_city', 'City', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_country', 'Country', 'trim|required|xss_clean');
		
	 	if($this->form_validation->run() == FALSE)
   		{
	   		$data['contents']	= 	$this->load->view('user_add','', true);
			$this->load->view('template',$data);
		}
		else
		{
			$data['category']=$this->user_model->add_user();
			header('Location: '.base_url().'user/index');
		}
	}
	
	public function edit($user_id=0)
	{
	 	$this->load->library('form_validation');
	 	$this->form_validation->set_rules('user_first_name', 'User Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_sur_name', 'Sur Name', 'trim|required|xss_clean');
	 	$this->form_validation->set_rules('user_email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_birthday', 'Birthday', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_address', 'Address', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_city', 'City', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_country', 'Country', 'trim|required|xss_clean');
	 
		
	 	if($this->form_validation->run() == FALSE)
   		{
			$data['users'] = $this->user_model->get_user($user_id);
	   		$data['contents']	= 	$this->load->view('user_edit',$data,true);
			$this->load->view('template',$data);
		}
		else
		{
			$data['users']=$this->user_model->edit_user($user_id);
			header('Location: '.base_url().'user/index');
		}
	}
	
	public function chg_status($cat_id,$status)
	{
		$new_status= 1;
		if($status==1)
		{
			$new_status=2;
		}
		
		$update_array['status_id'] = $new_status;
		$this->db->where('id',$cat_id);
		$this->db->update('category',$update_array);
		header('Location: '.base_url().'category/index');
		
	}
	public function user_role($user_id=0){
		
			$data['roles'] = $this->user_model->get_all_roles();
			
			$data['user_id'] = $user_id;
			
			$data['contents'] = $this->load->view('role_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}
	public function delete($user_id=0)
	{
		$this->user_model->delete($user_id);
		header('Location: '.base_url().'user/index');
	}
	
	function user_roles_entry($user_id=0)
	{
		$roles = $this->input->post('role');
		$this->db->where('user_id',$user_id);
		$this->db->delete('user_roles');
		
		foreach($roles as $role)
		{
			$user_update['user_id'] = $user_id;
			$user_update['role_id'] = $role;
			$this->db->insert('user_roles',$user_update);
		}
		
		header("Location: ".base_url()."user");
	}

	/*public function profile_listing($user_id = ''){
		
			$data['users'] = $this->user_model->get_profile($this->session->userdata('id'));
			
			$data['contents'] = $this->load->view('profile_listing',$data, true);
			
			$this->load->view('template',$data);
		
		}*/
		
	public function edit_profile(){
		
		$this->load->library('form_validation');
	 	$this->form_validation->set_rules('user_first_name', 'User Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_sur_name', 'Sur Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_birthday', 'Birthday', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_address', 'Address', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_city', 'City', 'trim|required|xss_clean');
		$this->form_validation->set_rules('user_country', 'Country', 'trim|required|xss_clean');
	 
		
			if($this->form_validation->run() == FALSE)
			{
				$data['users'] = $this->user_model->get_user($this->session->userdata('id'));
				$data['contents']	= 	$this->load->view('profile_edit',$data,true);
				$this->load->view('template',$data);
			}
			else
			{
				$data['users']=$this->user_model->edit_profile($this->session->userdata('id'));
				header('Location: '.base_url().'user/edit_profile');
			}
		
		}
		public function changepwd(){
			
				$this->load->library('form_validation');
				$this->form_validation->set_rules('opassword','Old Password','required|trim|xss_clean|callback_change');
				$this->form_validation->set_rules('npassword','New Password','required|trim');
				$this->form_validation->set_rules('cpassword','Confirm Password','required|trim|matches[npassword]');
				
					if($this->form_validation->run()!= true)
					{
						
						$data['contents'] = $this->load->view('change_password','',true);
						$this->load->view('template',$data);
					
					}
					else{
						
						$data['contents'] = $this->load->view('change_password','',true);
						$this->load->view('template',$data);
						header('Location: '.base_url().'home/index');
						
						}
				}
				
		public function change()
		{
					
					$sql = $this->user_model->get_user_data();
					/*print_r($sql);
					exit;*/
					
					foreach ($sql->result() as $my_info) {
						
					$db_password = $my_info->user_password;
					$db_id = $my_info->user_id;
					
					}
					
					if($this->input->post('opassword') == $db_password){
					
					$fixed_pw = $this->input->post('npassword');
					/*print_r($fixed_pw);
					exit;*/
					$update = $this->db->query("Update users SET user_password = $fixed_pw WHERE user_id = $db_id")or die(mysql_error());
				
				}
			}
			
			public function add_image($u_id)
			{
				//$image_name = $img_name ;
				
			/*	print_r($u_id);
				exit;*/
				
				$config['upload_path'] = 'upload/';
				$config['allowed_types'] = '*';
				$this->load->library('upload', $config);
		

				//$this->load->library('form_validation');
				//$this->form_validation->set_rules('userfile', 'userfile', 'required');
					$data['users'] = $this->user_model->get_user($this->session->userdata('id'));
					$data['contents']	= 	$this->load->view('user_profile_image',$data,true);
					$this->load->view('template',$data);
					
					 if ($this->upload->do_upload('userfile')) {
		
						$query = $this->db->get_where('users', array('user_id' => $u_id));
						$row = $query->row();
						$picture = $row->img_name;
						unlink(realpath('upload/' . $picture));
		
						$upload_data = $this->upload->data();
						$file_path = $upload_data['file_path'];
						$file = $upload_data['full_path'];
						$file_ext = $upload_data['file_ext'];
		
						$final_file_name = md5(time()) . $file_ext;
		
						rename($file, $file_path . $final_file_name); // Name of image
		
						$data['contests'] = $this->user_model->edit_image($final_file_name, $u_id);
						header('Location: ' . base_url() . 'index.php/user/edit_profile');
					} 
					
					
				
			
			
			}
			
		
		
		
}
?>