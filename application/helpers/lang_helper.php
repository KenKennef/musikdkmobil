<?php


	function set_language($lang_code,$lang_id=0)
    {
		$CI =& get_instance();
		if($lang_code == '') $lang_code = $CI->config->item('defaultLanguage');
		if($lang_id == '') $lang_id = $CI->config->item('defaultLanguageId');
		
		//echo $lang_code;
		$CI->lang->load('general', $lang_code);
		$CI->lang->load('calendar', $lang_code);
		$CI->lang->load('date', $lang_code);
		$CI->lang->load('db', $lang_code);
		$CI->lang->load('email', $lang_code);
		$CI->lang->load('form_validation', $lang_code);
		$CI->lang->load('ftp', $lang_code);
		$CI->lang->load('imglib', $lang_code);
		$CI->lang->load('migration', $lang_code);
		$CI->lang->load('number', $lang_code);
		$CI->lang->load('profiler', $lang_code);
		$CI->lang->load('unit_test', $lang_code);
		$CI->lang->load('upload', $lang_code);
		//$CI->lang->load('validation', $lang_code);
		$CI->session->set_userdata('lang_id',$lang_id);
		$CI->session->set_userdata('lang_code',$lang_code);
     
	 }
	 
	 

?>